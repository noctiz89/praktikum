BLASTP 2.6.0+


Reference: Stephen F. Altschul, Thomas L. Madden, Alejandro A.
Schaffer, Jinghui Zhang, Zheng Zhang, Webb Miller, and David J.
Lipman (1997), "Gapped BLAST and PSI-BLAST: a new generation of
protein database search programs", Nucleic Acids Res. 25:3389-3402.


Reference for composition-based statistics: Alejandro A. Schaffer,
L. Aravind, Thomas L. Madden, Sergei Shavirin, John L. Spouge, Yuri
I. Wolf, Eugene V. Koonin, and Stephen F. Altschul (2001),
"Improving the accuracy of PSI-BLAST protein database searches with
composition-based statistics and other refinements", Nucleic Acids
Res. 29:2994-3005.



Database: Choanoflagellata/15_Hartaetosiga_balthica.proteins.fasta
           18,816 sequences; 6,636,578 total letters



Query= AAH73964.1 PHC1 protein [Homo sapiens]

Length=957
                                                                      Score     E
Sequences producing significant alignments:                          (Bits)  Value

  m.39135 g.39135 ORF g.39135 m.39135 type:3prime_partial len:999...  42.0    0.001
  m.62581 g.62581 ORF g.62581 m.62581 type:3prime_partial len:202...  35.4    0.044
  m.54289 g.54289 ORF g.54289 m.54289 type:complete len:1085 (-) ...  32.3    0.91 
  m.97220 g.97220 ORF g.97220 m.97220 type:5prime_partial len:449...  32.0    1.0  
  m.20865 g.20865 ORF g.20865 m.20865 type:5prime_partial len:588...  30.4    3.5  
  m.15337 g.15337 ORF g.15337 m.15337 type:complete len:152 (+) c...  28.9    4.8  
  m.60856 g.60856 ORF g.60856 m.60856 type:5prime_partial len:214...  29.3    5.8  
  m.21895 g.21895 ORF g.21895 m.21895 type:5prime_partial len:530...  28.9    8.9  


> m.39135 g.39135  ORF g.39135 m.39135 type:3prime_partial len:999 
(-) comp6842_c0_seq2:2-2998(-)
Length=999

 Score = 42.0 bits (97),  Expect = 0.001, Method: Compositional matrix adjust.
 Identities = 28/93 (30%), Positives = 45/93 (48%), Gaps = 2/93 (2%)

Query  861  HGERDLGNPNTAPPTPELHGINPVFLSS-NPSRWSVEEVYEFIASLQGCQEIAEEFRSQE  919
            H    +GN   AP TP  H +   F ++     WSV+EV  +++ L         F+  E
Sbjct  326  HSSSFIGNHVPAPHTPLEHAVRSQFSTTVEAQHWSVDEVAAWMSVLPDVDMYTSLFKENE  385

Query  920  IDGQALLLLKEEHLMSAMNIK-LGPALKICAKI  951
            I+G+ LL L ++ L + ++IK  G  L +   I
Sbjct  386  INGRVLLHLTDDMLENGLSIKPFGHRLLLLNHI  418


> m.62581 g.62581  ORF g.62581 m.62581 type:3prime_partial len:202 
(-) comp8037_c2_seq1:3-608(-)
Length=202

 Score = 35.4 bits (80),  Expect = 0.044, Method: Composition-based stats.
 Identities = 20/72 (28%), Positives = 35/72 (49%), Gaps = 0/72 (0%)

Query  886  LSSNPSRWSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPAL  945
            +  NP  W+  EVYE+I    G  E+A       + G  LL++ +  L++  N+ +  A 
Sbjct  1    MKGNPLEWTPPEVYEWINVCLGKPEVAASLLHNNVCGVDLLMMTQPILINVHNLNVMDAK  60

Query  946  KICAKINVLKET  957
            +    +N L E+
Sbjct  61   EFVGSVNELFES  72


> m.54289 g.54289  ORF g.54289 m.54289 type:complete len:1085 (-) 
comp7714_c0_seq2:46-3300(-)
Length=1085

 Score = 32.3 bits (72),  Expect = 0.91, Method: Compositional matrix adjust.
 Identities = 15/45 (33%), Positives = 25/45 (56%), Gaps = 2/45 (4%)

Query  885   FLSSNPSRWSVEEVYEFIASLQ--GCQEIAEEFRSQEIDGQALLL  927
             F + N S WS  +V  ++ S+Q     + A  F+ Q +DG+ LL+
Sbjct  1010  FNAKNVSEWSTNDVVAWLKSIQDGDFAKYAPVFKQQGVDGKTLLV  1054


> m.97220 g.97220  ORF g.97220 m.97220 type:5prime_partial len:449 
(-) comp8983_c1_seq1:222-1568(-)
Length=449

 Score = 32.0 bits (71),  Expect = 1.0, Method: Compositional matrix adjust.
 Identities = 22/84 (26%), Positives = 33/84 (39%), Gaps = 4/84 (5%)

Query  429  MLQSSPLSLPPDAAPKPPIPIQSKPPVAPIKPPQLGAAKMSAAQQPPPHIPVQVVGTRQP  488
            +L  +PL+L  +        +  +  V P K  Q+     S A    P + +QV    +P
Sbjct  209  LLDVNPLTLGIETVGGVMTKLIERNSVVPTKKSQI----FSTAADNQPTVTIQVFEGERP  264

Query  489  GTAQAQALGLAQLAAAVPTSRGMP  512
             T     LG   L    P  RG+P
Sbjct  265  MTKDNHLLGKFDLNDIPPAPRGVP  288


> m.20865 g.20865  ORF g.20865 m.20865 type:5prime_partial len:588 
(-) comp5293_c0_seq1:69-1832(-)
Length=588

 Score = 30.4 bits (67),  Expect = 3.5, Method: Compositional matrix adjust.
 Identities = 26/88 (30%), Positives = 42/88 (48%), Gaps = 17/88 (19%)

Query  324  ATYTQIQPHSL-IQQQQQIHLQQK------------QVVIQQQIAIHHQQQFQHRQSQLL  370
            A  T++  +SL   Q   IHL++K               +   +A  H + F  R +Q L
Sbjct  304  AIDTRVDVYSLQYYQDSIIHLRKKMHEGVSGALRPPHCRLSSNVAQQHAR-FMERMAQWL  362

Query  371  HTATHLQLAQQQQ--QQQQQQQQQPQAT  396
            HT   L++ +++Q  Q Q Q+Q +PQ T
Sbjct  363  HTCL-LRIKKERQSLQTQSQEQTRPQDT  389


> m.15337 g.15337  ORF g.15337 m.15337 type:complete len:152 (+) 
comp4458_c0_seq1:146-601(+)
Length=152

 Score = 28.9 bits (63),  Expect = 4.8, Method: Composition-based stats.
 Identities = 19/53 (36%), Positives = 28/53 (53%), Gaps = 1/53 (2%)

Query  598  SEEERDDVSTLGSMLPAKASPVAESPKVMDEKSSLGEKAESVANVNANTPSSE  650
            S+EE D V+ LG+    KA  VA+   V D +   G+K+  V ++ AN    E
Sbjct  96   SQEEADAVAGLGTYKAPKAVEVADVC-VTDLREGDGDKSSKVEDIVANDEEME  147


> m.60856 g.60856  ORF g.60856 m.60856 type:5prime_partial len:214 
(-) comp7965_c2_seq1:2053-2694(-)
Length=214

 Score = 29.3 bits (64),  Expect = 5.8, Method: Composition-based stats.
 Identities = 18/63 (29%), Positives = 32/63 (51%), Gaps = 3/63 (5%)

Query  876  PELHGINPVFLSSNPSRWSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMS  935
            P +   + +FL S+P+   V +V   I +++    I EE   Q +DG   L++   HL+ 
Sbjct  85   PVMVSCSSIFLQSSPTHLDVAQVKRTILAIEHIHGIHEEHFWQLVDG---LVIGTLHLIV  141

Query  936  AMN  938
            + N
Sbjct  142  SKN  144


> m.21895 g.21895  ORF g.21895 m.21895 type:5prime_partial len:530 
(-) comp5400_c0_seq2:1382-2971(-)
Length=530

 Score = 28.9 bits (63),  Expect = 8.9, Method: Compositional matrix adjust.
 Identities = 20/52 (38%), Positives = 27/52 (52%), Gaps = 7/52 (13%)

Query  609  GSMLPAK-----ASPVAESPKVMDEKS--SLGEKAESVANVNANTPSSELVA  653
            G++LP+K       P AE PK    K+  S G  A++  + NANTP   L A
Sbjct  339  GTILPSKNIPSSVKPFAEIPKRSRRKNRDSYGSDADADVHANANTPKDILEA  390



Lambda      K        H        a         alpha
   0.308    0.122    0.340    0.792     4.96 

Gapped
Lambda      K        H        a         alpha    sigma
   0.267   0.0410    0.140     1.90     42.6     43.6 

Effective search space used: 3991796042


  Database: Choanoflagellata/15_Hartaetosiga_balthica.proteins.fasta
    Posted date:  Jun 24, 2020  11:20 AM
  Number of letters in database: 6,636,578
  Number of sequences in database:  18,816



Matrix: BLOSUM62
Gap Penalties: Existence: 11, Extension: 1
Neighboring words threshold: 11
Window for multiple hits: 40
