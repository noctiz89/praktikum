BLASTP 2.6.0+


Reference: Stephen F. Altschul, Thomas L. Madden, Alejandro A.
Schaffer, Jinghui Zhang, Zheng Zhang, Webb Miller, and David J.
Lipman (1997), "Gapped BLAST and PSI-BLAST: a new generation of
protein database search programs", Nucleic Acids Res. 25:3389-3402.


Reference for composition-based statistics: Alejandro A. Schaffer,
L. Aravind, Thomas L. Madden, Sergei Shavirin, John L. Spouge, Yuri
I. Wolf, Eugene V. Koonin, and Stephen F. Altschul (2001),
"Improving the accuracy of PSI-BLAST protein database searches with
composition-based statistics and other refinements", Nucleic Acids
Res. 29:2994-3005.



Database: Choanoflagellata/15_Hartaetosiga_balthica.proteins.fasta
           18,816 sequences; 6,636,578 total letters



Query= AAI44122.1 PHC2 protein [Homo sapiens]

Length=830
                                                                      Score     E
Sequences producing significant alignments:                          (Bits)  Value

  m.39135 g.39135 ORF g.39135 m.39135 type:3prime_partial len:999...  37.4    0.024
  m.71504 g.71504 ORF g.71504 m.71504 type:5prime_partial len:125...  34.3    0.22 
  m.18532 g.18532 ORF g.18532 m.18532 type:complete len:811 (-) c...  31.2    1.7  
  m.62581 g.62581 ORF g.62581 m.62581 type:3prime_partial len:202...  28.5    6.7  


> m.39135 g.39135  ORF g.39135 m.39135 type:3prime_partial len:999 
(-) comp6842_c0_seq2:2-2998(-)
Length=999

 Score = 37.4 bits (85),  Expect = 0.024, Method: Compositional matrix adjust.
 Identities = 21/70 (30%), Positives = 36/70 (51%), Gaps = 1/70 (1%)

Query  762  EPTKWNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKEDHLMSAMNIK-LGPALKI  820
            E   W+V++V  ++  LP        F+  EI+G+ LL L +D L + ++IK  G  L +
Sbjct  355  EAQHWSVDEVAAWMSVLPDVDMYTSLFKENEINGRVLLHLTDDMLENGLSIKPFGHRLLL  414

Query  821  YARISMLKDS  830
               I  ++ S
Sbjct  415  LNHIRNIQRS  424


> m.71504 g.71504  ORF g.71504 m.71504 type:5prime_partial len:1254 
(-) comp8352_c0_seq2:596-4357(-)
Length=1254

 Score = 34.3 bits (77),  Expect = 0.22, Method: Compositional matrix adjust.
 Identities = 21/56 (38%), Positives = 27/56 (48%), Gaps = 2/56 (4%)

Query  174  TSSPALTASQAQMYLRAQMVQNLTLRTQQTPAAAASGPTPTQPVLPSLALKPTPGG  229
            T+   L  +  Q Y  AQ +  L  RT + PAA +  P+PT  V  SL   P P G
Sbjct  554  TNEEELFDNNDQHYYSAQDI--LLTRTTENPAALSFPPSPTSTVFTSLVNTPVPSG  607


> m.18532 g.18532  ORF g.18532 m.18532 type:complete len:811 (-) 
comp4975_c1_seq1:916-3348(-)
Length=811

 Score = 31.2 bits (69),  Expect = 1.7, Method: Compositional matrix adjust.
 Identities = 14/55 (25%), Positives = 27/55 (49%), Gaps = 1/55 (2%)

Query  758  FLPSEPTKWNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKEDHLMSAMNI  812
            + P E  +W  +DV   ++ L    E+ + F   ++ G   L L +D ++  MN+
Sbjct  6    YTPREVEQWGSDDVLALLKILDISDELIDFFHENDVTGSEFLKLDDDAIVE-MNL  59


> m.62581 g.62581  ORF g.62581 m.62581 type:3prime_partial len:202 
(-) comp8037_c2_seq1:3-608(-)
Length=202

 Score = 28.5 bits (62),  Expect = 6.7, Method: Composition-based stats.
 Identities = 17/72 (24%), Positives = 34/72 (47%), Gaps = 0/72 (0%)

Query  759  LPSEPTKWNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKEDHLMSAMNIKLGPAL  818
            +   P +W   +VYE+I    G  E+A       + G  LL++ +  L++  N+ +  A 
Sbjct  1    MKGNPLEWTPPEVYEWINVCLGKPEVAASLLHNNVCGVDLLMMTQPILINVHNLNVMDAK  60

Query  819  KIYARISMLKDS  830
            +    ++ L +S
Sbjct  61   EFVGSVNELFES  72



Lambda      K        H        a         alpha
   0.310    0.125    0.359    0.792     4.96 

Gapped
Lambda      K        H        a         alpha    sigma
   0.267   0.0410    0.140     1.90     42.6     43.6 

Effective search space used: 3415831310


  Database: Choanoflagellata/15_Hartaetosiga_balthica.proteins.fasta
    Posted date:  Jun 24, 2020  11:20 AM
  Number of letters in database: 6,636,578
  Number of sequences in database:  18,816



Matrix: BLOSUM62
Gap Penalties: Existence: 11, Extension: 1
Neighboring words threshold: 11
Window for multiple hits: 40
