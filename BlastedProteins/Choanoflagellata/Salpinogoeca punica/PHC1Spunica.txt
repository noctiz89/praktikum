BLASTP 2.6.0+


Reference: Stephen F. Altschul, Thomas L. Madden, Alejandro A.
Schaffer, Jinghui Zhang, Zheng Zhang, Webb Miller, and David J.
Lipman (1997), "Gapped BLAST and PSI-BLAST: a new generation of
protein database search programs", Nucleic Acids Res. 25:3389-3402.


Reference for composition-based statistics: Alejandro A. Schaffer,
L. Aravind, Thomas L. Madden, Sergei Shavirin, John L. Spouge, Yuri
I. Wolf, Eugene V. Koonin, and Stephen F. Altschul (2001),
"Improving the accuracy of PSI-BLAST protein database searches with
composition-based statistics and other refinements", Nucleic Acids
Res. 29:2994-3005.



Database: Choanoflagellata/03_Salpingoeca_punica.proteins.fasta
           28,067 sequences; 7,722,045 total letters



Query= AAH73964.1 PHC1 protein [Homo sapiens]

Length=957
                                                                      Score     E
Sequences producing significant alignments:                          (Bits)  Value

  m.79339 g.79339 ORF g.79339 m.79339 type:5prime_partial len:427...  43.1    5e-04
  m.91343 g.91343 ORF g.91343 m.91343 type:internal len:614 (+) c...  42.7    7e-04
  m.148864 g.148864 ORF g.148864 m.148864 type:complete len:684 (...  30.8    3.0  


> m.79339 g.79339  ORF g.79339 m.79339 type:5prime_partial len:427 
(-) comp14788_c0_seq1:521-1801(-)
Length=427

 Score = 43.1 bits (100),  Expect = 5e-04, Method: Compositional matrix adjust.
 Identities = 23/79 (29%), Positives = 42/79 (53%), Gaps = 4/79 (5%)

Query  877  ELHGINPVFLSSNPSRWSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSA  936
            E + I+P    ++P  WS + V E++ S+ G +  A  F+  +I+G+ LL L ++ L   
Sbjct  345  ESYAIDP---DTDPREWSTQAVAEWLVSI-GFEAYAATFKENDIEGKHLLELTKDDLKEL  400

Query  937  MNIKLGPALKICAKINVLK  955
               +LG  + I  +I  L+
Sbjct  401  DISRLGHRMTISKEIERLR  419


> m.91343 g.91343  ORF g.91343 m.91343 type:internal len:614 (+) 
comp15298_c1_seq1:2-1846(+)
Length=614

 Score = 42.7 bits (99),  Expect = 7e-04, Method: Compositional matrix adjust.
 Identities = 23/63 (37%), Positives = 35/63 (56%), Gaps = 1/63 (2%)

Query  893  WSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKICAKIN  952
            W+ +EV ++++ + G  E    FR  +IDG ALL LK + L       +G  LKI   I+
Sbjct  28   WTTDEVCDWLSKI-GLSEHRVSFRKHKIDGAALLSLKIDDLRELGVDAVGDRLKIMNAID  86

Query  953  VLK  955
            +LK
Sbjct  87   LLK  89


> m.148864 g.148864  ORF g.148864 m.148864 type:complete len:684 
(-) comp16842_c2_seq1:443-2494(-)
Length=684

 Score = 30.8 bits (68),  Expect = 3.0, Method: Compositional matrix adjust.
 Identities = 14/27 (52%), Positives = 18/27 (67%), Gaps = 0/27 (0%)

Query  3   TESEQNSNSTNGSSSSGGSSRPQIAQM  29
           +ES     S NGS+SSGGS+RP +  M
Sbjct  70  SESNSADESRNGSTSSGGSTRPSVRMM  96



Lambda      K        H        a         alpha
   0.308    0.122    0.340    0.792     4.96 

Gapped
Lambda      K        H        a         alpha    sigma
   0.267   0.0410    0.140     1.90     42.6     43.6 

Effective search space used: 4097024681


  Database: Choanoflagellata/03_Salpingoeca_punica.proteins.fasta
    Posted date:  Jun 24, 2020  11:20 AM
  Number of letters in database: 7,722,045
  Number of sequences in database:  28,067



Matrix: BLOSUM62
Gap Penalties: Existence: 11, Extension: 1
Neighboring words threshold: 11
Window for multiple hits: 40
