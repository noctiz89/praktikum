BLASTP 2.6.0+


Reference: Stephen F. Altschul, Thomas L. Madden, Alejandro A.
Schaffer, Jinghui Zhang, Zheng Zhang, Webb Miller, and David J.
Lipman (1997), "Gapped BLAST and PSI-BLAST: a new generation of
protein database search programs", Nucleic Acids Res. 25:3389-3402.


Reference for composition-based statistics: Alejandro A. Schaffer,
L. Aravind, Thomas L. Madden, Sergei Shavirin, John L. Spouge, Yuri
I. Wolf, Eugene V. Koonin, and Stephen F. Altschul (2001),
"Improving the accuracy of PSI-BLAST protein database searches with
composition-based statistics and other refinements", Nucleic Acids
Res. 29:2994-3005.



Database: Fungi/GCF_000182925.2_NC12_protein.faa
           10,812 sequences; 5,632,514 total letters



Query= AAI44122.1 PHC2 protein [Homo sapiens]

Length=830
                                                                      Score     E
Sequences producing significant alignments:                          (Bits)  Value

  XP_962994.2 MAPKK kinase [Neurospora crassa OR74A]                  41.6    0.001
  XP_960881.3 polarized growth protein Boi2 [Neurospora crassa OR...  37.4    0.017
  XP_959165.2 HIT finger domain-containing protein [Neurospora cr...  33.1    0.30 
  XP_011393780.1 rho GTPase activator, variant 1 [Neurospora cras...  32.0    0.85 
  XP_011393779.1 rho GTPase activator [Neurospora crassa OR74A]       31.6    1.1  
  XP_961068.1 hypothetical protein NCU04278 [Neurospora crassa OR...  30.4    2.5  
  XP_957673.3 hypothetical protein NCU04058 [Neurospora crassa OR...  29.3    4.5  
  XP_963002.3 ATRX [Neurospora crassa OR74A]                          28.9    7.2  
  XP_963650.2 hormone-sensitive lipase [Neurospora crassa OR74A]      28.9    7.7  
  XP_964442.3 cell morphogenesis protein [Neurospora crassa OR74A]    28.9    8.6  
  XP_963033.2 histone-lysine N-methyltransferase set-9 [Neurospor...  28.5    9.9  


> XP_962994.2 MAPKK kinase [Neurospora crassa OR74A]
Length=914

 Score = 41.6 bits (96),  Expect = 0.001, Method: Compositional matrix adjust.
 Identities = 20/69 (29%), Positives = 36/69 (52%), Gaps = 1/69 (1%)

Query  760  PSEPTKWNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKEDHLMSAMNIKLGPALK  819
            P     WN + V E++RS+  C +    FR   I+G+ALL + ++ L      K+G  ++
Sbjct  61   PESVKNWNEDRVCEYLRSVK-CGDYERIFRKNNINGEALLEIDKEVLKEMGIEKVGDRVR  119

Query  820  IYARISMLK  828
            ++  I  L+
Sbjct  120  LFLSIKKLR  128


> XP_960881.3 polarized growth protein Boi2 [Neurospora crassa 
OR74A]
Length=945

 Score = 37.4 bits (85),  Expect = 0.017, Method: Compositional matrix adjust.
 Identities = 21/71 (30%), Positives = 39/71 (55%), Gaps = 2/71 (3%)

Query  761  SEPTKWNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKEDHL-MSAMNI-KLGPAL  818
            SE   W+ + V E++ ++   +   E FR QEI G+ +L + +  L + A ++  +G  L
Sbjct  217  SEVESWSADQVAEYLFTVGVEKHHCEVFRDQEITGEVILGMDQTSLFIKAFDLGSVGRRL  276

Query  819  KIYARISMLKD  829
            K + +I  L+D
Sbjct  277  KTWQKIKNLQD  287


> XP_959165.2 HIT finger domain-containing protein [Neurospora 
crassa OR74A]
Length=373

 Score = 33.1 bits (74),  Expect = 0.30, Method: Compositional matrix adjust.
 Identities = 15/50 (30%), Positives = 22/50 (44%), Gaps = 0/50 (0%)

Query  602  ESKEEGAPLKLKCELCGRVDFAYKFKRSKRFCSMACAKRYNVGCTKRVGL  651
            E +E   P +  CE+CG        K   R C++ C + +   C  R GL
Sbjct  324  EEEEGRYPARRFCEVCGYWGRVKCIKCGARVCALECLEAHREECLVRYGL  373


> XP_011393780.1 rho GTPase activator, variant 1 [Neurospora crassa 
OR74A]
Length=918

 Score = 32.0 bits (71),  Expect = 0.85, Method: Compositional matrix adjust.
 Identities = 24/52 (46%), Positives = 31/52 (60%), Gaps = 1/52 (2%)

Query  285  VPAVAAHPLIAPAYAQLQPHQLLPQPSSKHLQPQFVIQQQPQPQ-QQQPPPQ  335
            V +V   P+   + AQ+QP+    QPSS+H QPQ     Q QPQ QQ+P PQ
Sbjct  846  VQSVEMQPVAQESPAQVQPNLQPEQPSSQHPQPQQASAHQSQPQLQQEPLPQ  897


> XP_011393779.1 rho GTPase activator [Neurospora crassa OR74A]
Length=985

 Score = 31.6 bits (70),  Expect = 1.1, Method: Compositional matrix adjust.
 Identities = 24/52 (46%), Positives = 31/52 (60%), Gaps = 1/52 (2%)

Query  285  VPAVAAHPLIAPAYAQLQPHQLLPQPSSKHLQPQFVIQQQPQPQ-QQQPPPQ  335
            V +V   P+   + AQ+QP+    QPSS+H QPQ     Q QPQ QQ+P PQ
Sbjct  913  VQSVEMQPVAQESPAQVQPNLQPEQPSSQHPQPQQASAHQSQPQLQQEPLPQ  964


> XP_961068.1 hypothetical protein NCU04278 [Neurospora crassa 
OR74A]
Length=761

 Score = 30.4 bits (67),  Expect = 2.5, Method: Compositional matrix adjust.
 Identities = 28/109 (26%), Positives = 46/109 (42%), Gaps = 14/109 (13%)

Query  499  ALTDLSSPGMTSGNGNSASSIAGTAPQNGENKPPQAIVKPQILT-----------HVIEG  547
            A T L S G  S    S+ ++     Q G  KP Q   + ++             + +E 
Sbjct  61   AATSLKSSGHPSSLSQSSITLHSVDGQGGAGKPAQTNSEAEVADVARQSAQGCKPNSLET  120

Query  548  FVIQEGAEPFPVGRSSLLVGNLKKKYAQG---FLPEKLPQQDHTTTTDS  593
              ++  + PFP  RS LL GN+ ++   G   +L  + P+   +T  DS
Sbjct  121  GPVKNRSSPFPTTRSKLLPGNIVRRKTAGKDPYLNRRTPKPGPSTHKDS  169


> XP_957673.3 hypothetical protein NCU04058 [Neurospora crassa 
OR74A]
Length=306

 Score = 29.3 bits (64),  Expect = 4.5, Method: Compositional matrix adjust.
 Identities = 14/29 (48%), Positives = 19/29 (66%), Gaps = 0/29 (0%)

Query  702  LTHSQEDSSRCSDNSSYEEPLSPISASSS  730
            L H  + S+ CSD+ SYE P +P+S  SS
Sbjct  270  LQHPYDHSASCSDSGSYEYPRTPLSIPSS  298


> XP_963002.3 ATRX [Neurospora crassa OR74A]
Length=1869

 Score = 28.9 bits (63),  Expect = 7.2, Method: Compositional matrix adjust.
 Identities = 22/90 (24%), Positives = 39/90 (43%), Gaps = 23/90 (26%)

Query  761  SEPTKWNVEDVYEFI------------RSLPGCQEIAEEFRAQEIDGQALLLLKEDH---  805
            +EP  W+VE V + +              LP  + +A + R  +IDG+ LL+  +++   
Sbjct  7    NEPFHWDVERVVKELCTPERTWDAPAPHKLPDPERLAFKLRELDIDGETLLMYPDEYGWP  66

Query  806  -LMSAMNIK-------LGPALKIYARISML  827
                 + IK       +G A+K +   S L
Sbjct  67   TFWEMLGIKKLAHHLSIGKAIKQFRETSTL  96


> XP_963650.2 hormone-sensitive lipase [Neurospora crassa OR74A]
Length=841

 Score = 28.9 bits (63),  Expect = 7.7, Method: Compositional matrix adjust.
 Identities = 25/93 (27%), Positives = 38/93 (41%), Gaps = 19/93 (20%)

Query  672  ASKASLPPLTKDTKKQPTGTVPLSVTAALQLTHSQ-EDSSRCSDNSSYEEP---------  721
            A+K  LP L+ D KK P    P ++     +  +  +   RC   S  E P         
Sbjct  240  AAKTGLPILSLDYKKAPEFPYPYALNECYDVYSTIIKTKGRCIGLSGREVPKIVVTGDSA  299

Query  722  ---------LSPISASSSTSRRRQGQRDLELPD  745
                     L  + + SS  RR QG+ +L++PD
Sbjct  300  GGTLATSMTLMIVESGSSPVRRFQGEVNLKVPD  332


> XP_964442.3 cell morphogenesis protein [Neurospora crassa OR74A]
Length=2721

 Score = 28.9 bits (63),  Expect = 8.6, Method: Compositional matrix adjust.
 Identities = 28/106 (26%), Positives = 46/106 (43%), Gaps = 2/106 (2%)

Query  101   AQLQSLAAVQQASLVSNRQGSTSGSNVSAQAPAQSSSINLAASPAAAQLLNRAQSVNSAA  160
             AQ+    A  + S++ N QG+    NVSA    +  ++  AA  A A L      V + +
Sbjct  1379  AQISQREATMRQSVI-NEQGAGERGNVSAAMEIEKRNLRTAALSAMASLCGGPIRVLTES  1437

Query  161   ASGIAQQAVLLGNTSSPALTASQAQMYLRAQM-VQNLTLRTQQTPA  205
              S +   A  + N       A   ++ L  +  ++NL +  QQ PA
Sbjct  1438  GSFLYFDARRMLNWIDAIFNAESNRINLIGRRALKNLIVHNQQYPA  1483


> XP_963033.2 histone-lysine N-methyltransferase set-9 [Neurospora 
crassa OR74A]
Length=777

 Score = 28.5 bits (62),  Expect = 9.9, Method: Compositional matrix adjust.
 Identities = 36/115 (31%), Positives = 52/115 (45%), Gaps = 21/115 (18%)

Query  624  YKFKRSKRFCSMA--CAKRYNVGCTKRVGLFHSDRSKL-------------QKAGAATHN  668
            Y F+R +R+ S A   ++  +V    R  +    +S++             Q AGA   +
Sbjct  280  YSFRRKRRYGSTALQASRTPSVTPDMRPRVLRKSQSQMMLGERTSTTDSAAQGAGADGQS  339

Query  669  RRRASKASLPPLTKDTKKQPTGTVPLSVTAALQLTHSQEDSSRCSDNSSYEEPLS  723
            R+RA +   PP T  TKKQ T   P+ V  AL    S   S   SDN + + PLS
Sbjct  340  RKRALEMGTPPFTP-TKKQKTTQYPV-VPIAL----STAPSRGSSDNETSKSPLS  388



Lambda      K        H        a         alpha
   0.310    0.125    0.359    0.792     4.96 

Gapped
Lambda      K        H        a         alpha    sigma
   0.267   0.0410    0.140     1.90     42.6     43.6 

Effective search space used: 3285224306


  Database: Fungi/GCF_000182925.2_NC12_protein.faa
    Posted date:  Jun 24, 2020  11:21 AM
  Number of letters in database: 5,632,514
  Number of sequences in database:  10,812



Matrix: BLOSUM62
Gap Penalties: Existence: 11, Extension: 1
Neighboring words threshold: 11
Window for multiple hits: 40
