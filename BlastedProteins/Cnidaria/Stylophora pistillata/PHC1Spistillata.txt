BLASTP 2.6.0+


Reference: Stephen F. Altschul, Thomas L. Madden, Alejandro A.
Schaffer, Jinghui Zhang, Zheng Zhang, Webb Miller, and David J.
Lipman (1997), "Gapped BLAST and PSI-BLAST: a new generation of
protein database search programs", Nucleic Acids Res. 25:3389-3402.


Reference for composition-based statistics: Alejandro A. Schaffer,
L. Aravind, Thomas L. Madden, Sergei Shavirin, John L. Spouge, Yuri
I. Wolf, Eugene V. Koonin, and Stephen F. Altschul (2001),
"Improving the accuracy of PSI-BLAST protein database searches with
composition-based statistics and other refinements", Nucleic Acids
Res. 29:2994-3005.



Database: Cnidaria/GCF_002571385.1_Stylophora_pistillata_v1_protein.faa
           33,252 sequences; 18,275,238 total letters



Query= AAH73964.1 PHC1 protein [Homo sapiens]

Length=957
                                                                      Score     E
Sequences producing significant alignments:                          (Bits)  Value

  XP_022788400.1 polyhomeotic-like protein 1 isoform X1 [Stylopho...  137     1e-35
  XP_022788401.1 polyhomeotic-like protein 1 isoform X2 [Stylopho...  90.1    1e-19
  XP_022800979.1 uncharacterized protein LOC111338729 isoform X1 ...  91.7    1e-18
  XP_022800980.1 uncharacterized protein LOC111338729 isoform X2 ...  91.7    1e-18
  XP_022780254.1 uncharacterized protein LOC111321587 isoform X2 ...  74.3    5e-13
  XP_022780253.1 uncharacterized protein LOC111321587 isoform X1 ...  73.9    5e-13
  XP_022780252.1 uncharacterized protein LOC111321587 isoform X1 ...  73.9    5e-13
  XP_022780251.1 uncharacterized protein LOC111321587 isoform X1 ...  73.9    5e-13
  XP_022788866.1 sex comb on midleg-like protein 2 isoform X1 [St...  68.9    2e-11
  XP_022788860.1 sex comb on midleg-like protein 2 isoform X1 [St...  68.9    2e-11
  XP_022788872.1 sex comb on midleg-like protein 2 isoform X2 [St...  68.6    2e-11
  XP_022797047.1 histone acetyltransferase KAT6A-like [Stylophora...  62.4    1e-09
  XP_022777959.1 polyhomeotic-proximal chromatin protein-like iso...  61.6    2e-09
  XP_022777953.1 uncharacterized protein LOC111319432 isoform X1 ...  61.6    3e-09
  XP_022777946.1 uncharacterized protein LOC111319432 isoform X1 ...  61.6    3e-09
  XP_022796962.1 scm-like with four MBT domains protein 1 [Stylop...  60.1    9e-09
  XP_022781212.1 lethal(3)malignant brain tumor-like protein 2 is...  59.7    1e-08
  XP_022781210.1 MBT domain-containing protein 1-like isoform X1 ...  59.7    1e-08
  XP_022781211.1 MBT domain-containing protein 1-like isoform X2 ...  59.7    1e-08
  XP_022779947.1 polycomb protein Scm-like [Stylophora pistillata]    57.4    4e-08
  XP_022804678.1 lethal(3)malignant brain tumor-like protein 3 is...  55.1    3e-07
  XP_022788944.1 lethal(3)malignant brain tumor-like protein 3 [S...  54.3    3e-07
  XP_022778323.1 lethal(3)malignant brain tumor-like protein 3 [S...  42.0    0.002
  XP_022796115.1 uncharacterized protein LOC111334594 [Stylophora...  37.4    0.045
  XP_022804679.1 lethal(3)malignant brain tumor-like protein 4 is...  37.4    0.065
  XP_022800592.1 sterile alpha motif domain-containing protein 14...  32.7    1.5  
  XP_022798456.1 uncharacterized protein LOC111336591 [Stylophora...  32.7    1.9  
  XP_022789856.1 lipoma-preferred partner-like [Stylophora pistil...  32.0    2.4  
  XP_022804166.1 WD repeat, SAM and U-box domain-containing prote...  32.0    2.6  
  XP_022804165.1 WD repeat, SAM and U-box domain-containing prote...  32.0    2.8  
  XP_022791263.1 nesprin-1-like [Stylophora pistillata]               32.0    4.0  
  XP_022800614.1 LOW QUALITY PROTEIN: titin-like [Stylophora pist...  31.6    5.3  
  XP_022784542.1 sterile alpha motif domain-containing protein 3-...  30.4    7.0  
  XP_022803195.1 doublesex- and mab-3-related transcription facto...  30.0    8.0  
  XP_022803202.1 doublesex- and mab-3-related transcription facto...  30.0    8.0  


> XP_022788400.1 polyhomeotic-like protein 1 isoform X1 [Stylophora 
pistillata]
Length=280

 Score = 137 bits (344),  Expect = 1e-35, Method: Composition-based stats.
 Identities = 100/284 (35%), Positives = 143/284 (50%), Gaps = 46/284 (16%)

Query  684  PQILTHIIEGFVIQEGAEPFPVGC-SQLLKESEKPLQTGLPTGLTENQSGGSLGVDSPSA  742
            P+ILTH +EGFVIQE   PF  G  ++ + +++         G  + ++G     +SPS+
Sbjct  23   PRILTHFVEGFVIQESNYPFTSGNDTEEINQNQLNGNQNYENGDIDMEAG--FEAESPSS  80

Query  743  ELD-------KKANLLKCEYCGKYAPAEQFRGSKRFCSMTCAKRYNVSCSHQFRLKRKKM  795
              +       + +N+  CE CG        RG KRFCS +CA+RY+VSCS       +KM
Sbjct  81   NDEAFNDVFVEDSNIGHCEQCGSLTEVNHHRGPKRFCSTSCARRYSVSCS-------RKM  133

Query  796  KEFQEANYARVRRRGPRRSSSDIARAKIQGKCHRGQEDSSRGSDNSSYDEALSPTSPGPL  855
              F        R +G R SS+     K+        E  +                 GP 
Sbjct  134  VAFH------ARSQGGRHSSAKSDHKKLSYSLPMYYEQQA-----------------GPY  170

Query  856  SVRAG--HGERDLGNPNTAPPTPELHGINPVFLSSN--PSRWSVEEVYEFIASLQGCQEI  911
             V A     E D  +P     T     + P++L  +   ++W+VEEV  F+ SL GC E 
Sbjct  171  MVDASIQVNEFDTVHPLNFDWTRS--DVEPLWLYEHVEAAKWNVEEVAAFVKSLNGCSEY  228

Query  912  AEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKICAKINVLK  955
            A+ F SQEIDGQAL+LL+EEH++ AM++KLGPALKI A +N +K
Sbjct  229  ADSFVSQEIDGQALMLLREEHMVVAMHMKLGPALKIVANVNAMK  272


> XP_022788401.1 polyhomeotic-like protein 1 isoform X2 [Stylophora 
pistillata]
Length=245

 Score = 90.1 bits (222),  Expect = 1e-19, Method: Composition-based stats.
 Identities = 42/77 (55%), Positives = 59/77 (77%), Gaps = 2/77 (3%)

Query  881  INPVFLSSN--PSRWSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMN  938
            + P++L  +   ++W+VEEV  F+ SL GC E A+ F SQEIDGQAL+LL+EEH++ AM+
Sbjct  161  VEPLWLYEHVEAAKWNVEEVAAFVKSLNGCSEYADSFVSQEIDGQALMLLREEHMVVAMH  220

Query  939  IKLGPALKICAKINVLK  955
            +KLGPALKI A +N +K
Sbjct  221  MKLGPALKIVANVNAMK  237


 Score = 62.0 bits (149),  Expect = 3e-10, Method: Composition-based stats.
 Identities = 36/106 (34%), Positives = 56/106 (53%), Gaps = 10/106 (9%)

Query  684  PQILTHIIEGFVIQEGAEPFPVGC-SQLLKESEKPLQTGLPTGLTENQSGGSLGVDSPSA  742
            P+ILTH +EGFVIQE   PF  G  ++ + +++         G  + ++G     +SPS+
Sbjct  23   PRILTHFVEGFVIQESNYPFTSGNDTEEINQNQLNGNQNYENGDIDMEAG--FEAESPSS  80

Query  743  ELD-------KKANLLKCEYCGKYAPAEQFRGSKRFCSMTCAKRYN  781
              +       + +N+  CE CG        RG KRFCS +CA+RY+
Sbjct  81   NDEAFNDVFVEDSNIGHCEQCGSLTEVNHHRGPKRFCSTSCARRYS  126


> XP_022800979.1 uncharacterized protein LOC111338729 isoform X1 
[Stylophora pistillata]
Length=749

 Score = 91.7 bits (226),  Expect = 1e-18, Method: Compositional matrix adjust.
 Identities = 40/82 (49%), Positives = 59/82 (72%), Gaps = 3/82 (4%)

Query  879  HGINPVF---LSSNPSRWSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMS  935
            H + P F   L+ N   W+V++VY+F+ASL G  EIA  FR++ IDG +L+L++E+HL++
Sbjct  659  HKLVPRFRPDLNENVLEWTVDDVYQFVASLTGSTEIAHAFRTEHIDGHSLVLMQEDHLLN  718

Query  936  AMNIKLGPALKICAKINVLKET  957
             M+I+LGPALKI A+I  L E 
Sbjct  719  RMSIRLGPALKIIAQIKKLTEN  740


> XP_022800980.1 uncharacterized protein LOC111338729 isoform X2 
[Stylophora pistillata]
Length=748

 Score = 91.7 bits (226),  Expect = 1e-18, Method: Compositional matrix adjust.
 Identities = 40/82 (49%), Positives = 59/82 (72%), Gaps = 3/82 (4%)

Query  879  HGINPVF---LSSNPSRWSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMS  935
            H + P F   L+ N   W+V++VY+F+ASL G  EIA  FR++ IDG +L+L++E+HL++
Sbjct  658  HKLVPRFRPDLNENVLEWTVDDVYQFVASLTGSTEIAHAFRTEHIDGHSLVLMQEDHLLN  717

Query  936  AMNIKLGPALKICAKINVLKET  957
             M+I+LGPALKI A+I  L E 
Sbjct  718  RMSIRLGPALKIIAQIKKLTEN  739


> XP_022780254.1 uncharacterized protein LOC111321587 isoform X2 
[Stylophora pistillata]
Length=1564

 Score = 74.3 bits (181),  Expect = 5e-13, Method: Compositional matrix adjust.
 Identities = 49/121 (40%), Positives = 64/121 (53%), Gaps = 10/121 (8%)

Query  841   SSYDEALSPTSPGPLS------VRAGHGERDLGNPNTAPPTPELHGINPVFLSSNPSRWS  894
             S Y    SPTS  P S      +R      D+G   + P    ++G +P  L +NPS WS
Sbjct  1444  SRYSPYNSPTSNSPFSPLGNRPIRPKPFCVDVGTNTSLPGC--VNGRSPD-LPANPSTWS  1500

Query  895   VEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKICAKINVL  954
             VE+V  ++ S   C   A  F  QEIDGQAL+LL  + LM    +KLGP LK+C+ I  L
Sbjct  1501  VEQVVRYVRSTD-CLHYANIFLDQEIDGQALMLLSRDSLMQFTRMKLGPTLKMCSYIAQL  1559

Query  955   K  955
             K
Sbjct  1560  K  1560


> XP_022780253.1 uncharacterized protein LOC111321587 isoform X1 
[Stylophora pistillata]
Length=1565

 Score = 73.9 bits (180),  Expect = 5e-13, Method: Compositional matrix adjust.
 Identities = 49/121 (40%), Positives = 64/121 (53%), Gaps = 10/121 (8%)

Query  841   SSYDEALSPTSPGPLS------VRAGHGERDLGNPNTAPPTPELHGINPVFLSSNPSRWS  894
             S Y    SPTS  P S      +R      D+G   + P    ++G +P  L +NPS WS
Sbjct  1445  SRYSPYNSPTSNSPFSPLGNRPIRPKPFCVDVGTNTSLPGC--VNGRSPD-LPANPSTWS  1501

Query  895   VEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKICAKINVL  954
             VE+V  ++ S   C   A  F  QEIDGQAL+LL  + LM    +KLGP LK+C+ I  L
Sbjct  1502  VEQVVRYVRSTD-CLHYANIFLDQEIDGQALMLLSRDSLMQFTRMKLGPTLKMCSYIAQL  1560

Query  955   K  955
             K
Sbjct  1561  K  1561


> XP_022780252.1 uncharacterized protein LOC111321587 isoform X1 
[Stylophora pistillata]
Length=1565

 Score = 73.9 bits (180),  Expect = 5e-13, Method: Compositional matrix adjust.
 Identities = 49/121 (40%), Positives = 64/121 (53%), Gaps = 10/121 (8%)

Query  841   SSYDEALSPTSPGPLS------VRAGHGERDLGNPNTAPPTPELHGINPVFLSSNPSRWS  894
             S Y    SPTS  P S      +R      D+G   + P    ++G +P  L +NPS WS
Sbjct  1445  SRYSPYNSPTSNSPFSPLGNRPIRPKPFCVDVGTNTSLPGC--VNGRSPD-LPANPSTWS  1501

Query  895   VEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKICAKINVL  954
             VE+V  ++ S   C   A  F  QEIDGQAL+LL  + LM    +KLGP LK+C+ I  L
Sbjct  1502  VEQVVRYVRSTD-CLHYANIFLDQEIDGQALMLLSRDSLMQFTRMKLGPTLKMCSYIAQL  1560

Query  955   K  955
             K
Sbjct  1561  K  1561


> XP_022780251.1 uncharacterized protein LOC111321587 isoform X1 
[Stylophora pistillata]
Length=1565

 Score = 73.9 bits (180),  Expect = 5e-13, Method: Compositional matrix adjust.
 Identities = 49/121 (40%), Positives = 64/121 (53%), Gaps = 10/121 (8%)

Query  841   SSYDEALSPTSPGPLS------VRAGHGERDLGNPNTAPPTPELHGINPVFLSSNPSRWS  894
             S Y    SPTS  P S      +R      D+G   + P    ++G +P  L +NPS WS
Sbjct  1445  SRYSPYNSPTSNSPFSPLGNRPIRPKPFCVDVGTNTSLPGC--VNGRSPD-LPANPSTWS  1501

Query  895   VEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKICAKINVL  954
             VE+V  ++ S   C   A  F  QEIDGQAL+LL  + LM    +KLGP LK+C+ I  L
Sbjct  1502  VEQVVRYVRSTD-CLHYANIFLDQEIDGQALMLLSRDSLMQFTRMKLGPTLKMCSYIAQL  1560

Query  955   K  955
             K
Sbjct  1561  K  1561


> XP_022788866.1 sex comb on midleg-like protein 2 isoform X1 [Stylophora 
pistillata]
Length=851

 Score = 68.9 bits (167),  Expect = 2e-11, Method: Compositional matrix adjust.
 Identities = 36/69 (52%), Positives = 45/69 (65%), Gaps = 1/69 (1%)

Query  887  SSNPSRWSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALK  946
            +S P  WSV EV +FI + +   E AE FR  EIDG+ALLLL  E +MS M +KLGPA+K
Sbjct  779  TSPPKTWSVSEVVKFIENSE-LAEHAEMFRKHEIDGKALLLLTREMIMSYMGLKLGPAIK  837

Query  947  ICAKINVLK  955
            +   I  LK
Sbjct  838  LLCFIEELK  846


> XP_022788860.1 sex comb on midleg-like protein 2 isoform X1 [Stylophora 
pistillata]
Length=851

 Score = 68.9 bits (167),  Expect = 2e-11, Method: Compositional matrix adjust.
 Identities = 36/69 (52%), Positives = 45/69 (65%), Gaps = 1/69 (1%)

Query  887  SSNPSRWSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALK  946
            +S P  WSV EV +FI + +   E AE FR  EIDG+ALLLL  E +MS M +KLGPA+K
Sbjct  779  TSPPKTWSVSEVVKFIENSE-LAEHAEMFRKHEIDGKALLLLTREMIMSYMGLKLGPAIK  837

Query  947  ICAKINVLK  955
            +   I  LK
Sbjct  838  LLCFIEELK  846


> XP_022788872.1 sex comb on midleg-like protein 2 isoform X2 [Stylophora 
pistillata]
Length=827

 Score = 68.6 bits (166),  Expect = 2e-11, Method: Compositional matrix adjust.
 Identities = 36/69 (52%), Positives = 45/69 (65%), Gaps = 1/69 (1%)

Query  887  SSNPSRWSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALK  946
            +S P  WSV EV +FI + +   E AE FR  EIDG+ALLLL  E +MS M +KLGPA+K
Sbjct  755  TSPPKTWSVSEVVKFIENSE-LAEHAEMFRKHEIDGKALLLLTREMIMSYMGLKLGPAIK  813

Query  947  ICAKINVLK  955
            +   I  LK
Sbjct  814  LLCFIEELK  822


> XP_022797047.1 histone acetyltransferase KAT6A-like [Stylophora 
pistillata]
Length=491

 Score = 62.4 bits (150),  Expect = 1e-09, Method: Compositional matrix adjust.
 Identities = 31/59 (53%), Positives = 43/59 (73%), Gaps = 1/59 (2%)

Query  889  NPSRWSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKI  947
            + S W+V EV  F +SL G  E A+ F++QEIDG+ALLL+    +++ M+IKLGPALKI
Sbjct  418  DTSLWTVTEVVSFFSSL-GFPEEAKSFQNQEIDGKALLLMSRNDVLTGMSIKLGPALKI  475


> XP_022777959.1 polyhomeotic-proximal chromatin protein-like isoform 
X2 [Stylophora pistillata]
Length=723

 Score = 61.6 bits (148),  Expect = 2e-09, Method: Compositional matrix adjust.
 Identities = 29/65 (45%), Positives = 43/65 (66%), Gaps = 1/65 (2%)

Query  891  SRWSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKICAK  950
            ++W  + V +FIA+   C++ AE F  +EIDG+ALL L  E LM  M++KLGPA+K+   
Sbjct  655  TKWDAKNVRDFIAATD-CKDKAELFLEEEIDGKALLSLSPEMLMKGMSLKLGPAVKLYNH  713

Query  951  INVLK  955
            I  L+
Sbjct  714  IVNLR  718


> XP_022777953.1 uncharacterized protein LOC111319432 isoform X1 
[Stylophora pistillata]
Length=744

 Score = 61.6 bits (148),  Expect = 3e-09, Method: Compositional matrix adjust.
 Identities = 29/65 (45%), Positives = 43/65 (66%), Gaps = 1/65 (2%)

Query  891  SRWSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKICAK  950
            ++W  + V +FIA+   C++ AE F  +EIDG+ALL L  E LM  M++KLGPA+K+   
Sbjct  676  TKWDAKNVRDFIAATD-CKDKAELFLEEEIDGKALLSLSPEMLMKGMSLKLGPAVKLYNH  734

Query  951  INVLK  955
            I  L+
Sbjct  735  IVNLR  739


> XP_022777946.1 uncharacterized protein LOC111319432 isoform X1 
[Stylophora pistillata]
Length=744

 Score = 61.6 bits (148),  Expect = 3e-09, Method: Compositional matrix adjust.
 Identities = 29/65 (45%), Positives = 43/65 (66%), Gaps = 1/65 (2%)

Query  891  SRWSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKICAK  950
            ++W  + V +FIA+   C++ AE F  +EIDG+ALL L  E LM  M++KLGPA+K+   
Sbjct  676  TKWDAKNVRDFIAATD-CKDKAELFLEEEIDGKALLSLSPEMLMKGMSLKLGPAVKLYNH  734

Query  951  INVLK  955
            I  L+
Sbjct  735  IVNLR  739


> XP_022796962.1 scm-like with four MBT domains protein 1 [Stylophora 
pistillata]
Length=952

 Score = 60.1 bits (144),  Expect = 9e-09, Method: Compositional matrix adjust.
 Identities = 26/57 (46%), Positives = 40/57 (70%), Gaps = 1/57 (2%)

Query  891  SRWSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKI  947
            S+WSV+EV +FI +   C + A+EF+ QEIDG+AL LL  + +   + + LGPA+K+
Sbjct  885  SQWSVKEVVDFIRT-TDCFKFADEFQRQEIDGRALTLLSLDEIHKCLGVTLGPAIKL  940


> XP_022781212.1 lethal(3)malignant brain tumor-like protein 2 
isoform X3 [Stylophora pistillata]
Length=1045

 Score = 59.7 bits (143),  Expect = 1e-08, Method: Compositional matrix adjust.
 Identities = 31/68 (46%), Positives = 43/68 (63%), Gaps = 1/68 (1%)

Query  889   NPSRWSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKIC  948
             +P  W+V+EV  F+ S+ GC   A+ F  +EIDG+ALLLL +E L S    KLGP  KI 
Sbjct  975   DPRLWNVQEVTSFMFSI-GCSNYADAFVKEEIDGRALLLLTQEMLESLTENKLGPMTKIQ  1033

Query  949   AKINVLKE  956
             + +  LK+
Sbjct  1034  SAVKALKQ  1041


 Score = 45.1 bits (105),  Expect = 3e-04, Method: Compositional matrix adjust.
 Identities = 19/36 (53%), Positives = 22/36 (61%), Gaps = 1/36 (3%)

Query  749  NLLKCEYCGKYAPAEQFRG-SKRFCSMTCAKRYNVS  783
            N+ KCE+C      E F   SKRFC M CAKRY+ S
Sbjct  144  NMAKCEFCCYIGVREHFFSKSKRFCKMECAKRYSAS  179


> XP_022781210.1 MBT domain-containing protein 1-like isoform X1 
[Stylophora pistillata]
Length=1123

 Score = 59.7 bits (143),  Expect = 1e-08, Method: Compositional matrix adjust.
 Identities = 31/68 (46%), Positives = 43/68 (63%), Gaps = 1/68 (1%)

Query  889   NPSRWSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKIC  948
             +P  W+V+EV  F+ S+ GC   A+ F  +EIDG+ALLLL +E L S    KLGP  KI 
Sbjct  1053  DPRLWNVQEVTSFMFSI-GCSNYADAFVKEEIDGRALLLLTQEMLESLTENKLGPMTKIQ  1111

Query  949   AKINVLKE  956
             + +  LK+
Sbjct  1112  SAVKALKQ  1119


 Score = 43.9 bits (102),  Expect = 8e-04, Method: Compositional matrix adjust.
 Identities = 23/51 (45%), Positives = 28/51 (55%), Gaps = 8/51 (16%)

Query  741  SAELDKKANLL-------KCEYCGKYAPAEQFRG-SKRFCSMTCAKRYNVS  783
            S +LD  +NL+       KCE+C      E F   SKRFC M CAKRY+ S
Sbjct  207  SLKLDLLSNLVTVLSDTAKCEFCCYIGVREHFFSKSKRFCKMECAKRYSAS  257


 Score = 43.1 bits (100),  Expect = 0.001, Method: Compositional matrix adjust.
 Identities = 18/33 (55%), Positives = 20/33 (61%), Gaps = 1/33 (3%)

Query  749  NLLKCEYCGKYAPAEQFRG-SKRFCSMTCAKRY  780
            N+ KCE+C      E F   SKRFC M CAKRY
Sbjct  144  NMAKCEFCHYIGIRESFFSKSKRFCKMECAKRY  176


> XP_022781211.1 MBT domain-containing protein 1-like isoform X2 
[Stylophora pistillata]
Length=1122

 Score = 59.7 bits (143),  Expect = 1e-08, Method: Compositional matrix adjust.
 Identities = 31/68 (46%), Positives = 43/68 (63%), Gaps = 1/68 (1%)

Query  889   NPSRWSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKIC  948
             +P  W+V+EV  F+ S+ GC   A+ F  +EIDG+ALLLL +E L S    KLGP  KI 
Sbjct  1052  DPRLWNVQEVTSFMFSI-GCSNYADAFVKEEIDGRALLLLTQEMLESLTENKLGPMTKIQ  1110

Query  949   AKINVLKE  956
             + +  LK+
Sbjct  1111  SAVKALKQ  1118


 Score = 43.9 bits (102),  Expect = 8e-04, Method: Compositional matrix adjust.
 Identities = 23/51 (45%), Positives = 28/51 (55%), Gaps = 8/51 (16%)

Query  741  SAELDKKANLL-------KCEYCGKYAPAEQFRG-SKRFCSMTCAKRYNVS  783
            S +LD  +NL+       KCE+C      E F   SKRFC M CAKRY+ S
Sbjct  207  SLKLDLLSNLVTVLSDTAKCEFCCYIGVREHFFSKSKRFCKMECAKRYSAS  257


 Score = 43.1 bits (100),  Expect = 0.001, Method: Compositional matrix adjust.
 Identities = 18/33 (55%), Positives = 20/33 (61%), Gaps = 1/33 (3%)

Query  749  NLLKCEYCGKYAPAEQFRG-SKRFCSMTCAKRY  780
            N+ KCE+C      E F   SKRFC M CAKRY
Sbjct  144  NMAKCEFCHYIGIRESFFSKSKRFCKMECAKRY  176


> XP_022779947.1 polycomb protein Scm-like [Stylophora pistillata]
Length=438

 Score = 57.4 bits (137),  Expect = 4e-08, Method: Compositional matrix adjust.
 Identities = 29/65 (45%), Positives = 40/65 (62%), Gaps = 3/65 (5%)

Query  893  WSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKICAKIN  952
            WS  +V +F  +   C E A  F+ QEIDG+AL+LL  + L+  +  K+GPALKI  KI 
Sbjct  366  WSTTDVVQFFYNTD-CAEYAGFFQEQEIDGRALMLLNRDTLLQFL--KVGPALKILQKIG  422

Query  953  VLKET  957
             L+ T
Sbjct  423  ELRST  427


> XP_022804678.1 lethal(3)malignant brain tumor-like protein 3 
isoform X1 [Stylophora pistillata]
Length=978

 Score = 55.1 bits (131),  Expect = 3e-07, Method: Compositional matrix adjust.
 Identities = 26/60 (43%), Positives = 40/60 (67%), Gaps = 1/60 (2%)

Query  892  RWSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKICAKI  951
            +W+V++V   + SL GC E A+ F  + IDG+A LL+ +  +++ + IKLGPALKI   I
Sbjct  917  KWTVDDVMSHVRSL-GCLEQAKIFGDELIDGEAFLLVTQNDIVNILKIKLGPALKIFNTI  975


 Score = 38.1 bits (87),  Expect = 0.040, Method: Compositional matrix adjust.
 Identities = 17/50 (34%), Positives = 26/50 (52%), Gaps = 8/50 (16%)

Query  749  NLLKCEYCGKYAPAEQFRGSKRFCSMTCAKRYNVSCSHQFRLKRKKMKEF  798
            ++L CE CGK+   ++F  S RFC ++C   Y          +R K +EF
Sbjct  234  DVLCCEVCGKHGLLQEFSASGRFCGLSCVGVYTG--------RRNKGREF  275


> XP_022788944.1 lethal(3)malignant brain tumor-like protein 3 
[Stylophora pistillata]
Length=497

 Score = 54.3 bits (129),  Expect = 3e-07, Method: Compositional matrix adjust.
 Identities = 26/55 (47%), Positives = 37/55 (67%), Gaps = 1/55 (2%)

Query  893  WSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKI  947
            W V +V   I+ L GC + A+ F  Q+IDG+A LLL +  +++ + IKLGPALKI
Sbjct  437  WDVNDVASHISCL-GCGDQAKIFIEQQIDGEAFLLLNQSDIVNILKIKLGPALKI  490


> XP_022778323.1 lethal(3)malignant brain tumor-like protein 3 
[Stylophora pistillata]
Length=500

 Score = 42.0 bits (97),  Expect = 0.002, Method: Compositional matrix adjust.
 Identities = 16/33 (48%), Positives = 21/33 (64%), Gaps = 0/33 (0%)

Query  748  ANLLKCEYCGKYAPAEQFRGSKRFCSMTCAKRY  780
            +++L CE CGKY    QF  S RFCS++C   Y
Sbjct  222  SSILCCEVCGKYGLPHQFSASGRFCSLSCVGVY  254


> XP_022796115.1 uncharacterized protein LOC111334594 [Stylophora 
pistillata]
Length=266

 Score = 37.4 bits (85),  Expect = 0.045, Method: Composition-based stats.
 Identities = 29/91 (32%), Positives = 44/91 (48%), Gaps = 5/91 (5%)

Query  866  LGNPNTAPPTPE-LHGINPVFLSSNPSRWSVEEVYEFIASLQGCQEIAEEFRSQEIDGQA  924
            +  P    PT E +  ++P  +S+    WSVE+V        G   +AE+F   +I+G+ 
Sbjct  29   VNQPAKLSPTEEYVLNLDPATVST----WSVEDVNRNFLERAGLGYMAEQFSVNKINGKC  84

Query  925  LLLLKEEHLMSAMNIKLGPALKICAKINVLK  955
            L+LL E HL       LG  L +   I +LK
Sbjct  85   LMLLTEGHLHELGITVLGDRLYLMDLIGLLK  115


> XP_022804679.1 lethal(3)malignant brain tumor-like protein 4 
isoform X2 [Stylophora pistillata]
Length=790

 Score = 37.4 bits (85),  Expect = 0.065, Method: Compositional matrix adjust.
 Identities = 17/50 (34%), Positives = 26/50 (52%), Gaps = 8/50 (16%)

Query  749  NLLKCEYCGKYAPAEQFRGSKRFCSMTCAKRYNVSCSHQFRLKRKKMKEF  798
            ++L CE CGK+   ++F  S RFC ++C   Y          +R K +EF
Sbjct  234  DVLCCEVCGKHGLLQEFSASGRFCGLSCVGVYTG--------RRNKGREF  275


> XP_022800592.1 sterile alpha motif domain-containing protein 
14-like [Stylophora pistillata]
Length=441

 Score = 32.7 bits (73),  Expect = 1.5, Method: Compositional matrix adjust.
 Identities = 15/38 (39%), Positives = 24/38 (63%), Gaps = 1/38 (3%)

Query  891  SRWSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLL  928
            S W  ++VY ++   +   E AEEF +++IDG+ LL L
Sbjct  338  SDWDTDQVYAWLIQNE-LDEYAEEFIAKKIDGKQLLNL  374


> XP_022798456.1 uncharacterized protein LOC111336591 [Stylophora 
pistillata]
Length=745

 Score = 32.7 bits (73),  Expect = 1.9, Method: Compositional matrix adjust.
 Identities = 15/49 (31%), Positives = 27/49 (55%), Gaps = 0/49 (0%)

Query  891  SRWSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNI  939
            S W++ +V  ++AS +     A  F   EIDG  L+ ++E  + + +NI
Sbjct  312  SSWNIHDVQAWLASFKMQAHCALSFEENEIDGFLLVSMRETDMETHLNI  360


> XP_022789856.1 lipoma-preferred partner-like [Stylophora pistillata]
Length=286

 Score = 32.0 bits (71),  Expect = 2.4, Method: Composition-based stats.
 Identities = 20/68 (29%), Positives = 31/68 (46%), Gaps = 7/68 (10%)

Query  429  MLQSSPLSLPPDAAPKPPIPIQSKPPVAPIKPPQLGAAKMSAAQQPPPHIPVQVVGTRQP  488
            M   +P+ L P    K P      PPV+P KP ++ A +++ A   PP   +Q  G  + 
Sbjct  1    MASRTPVRLDPATGKKIP------PPVSP-KPVRMTATRLTKANSRPPPPSLQSSGGMKS  53

Query  489  GTAQAQAL  496
               +  AL
Sbjct  54   TEEELDAL  61


> XP_022804166.1 WD repeat, SAM and U-box domain-containing protein 
1-like isoform X2 [Stylophora pistillata]
Length=508

 Score = 32.0 bits (71),  Expect = 2.6, Method: Compositional matrix adjust.
 Identities = 20/52 (38%), Positives = 26/52 (50%), Gaps = 3/52 (6%)

Query  893  WSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPA  944
            WSV++V  ++ SL       E FR   IDG  L  +  E L S + I  GPA
Sbjct  364  WSVDDVCSWLISLD-LDIYCESFRQNAIDGMELSNMTSEVLASDLGI--GPA  412


> XP_022804165.1 WD repeat, SAM and U-box domain-containing protein 
1-like isoform X1 [Stylophora pistillata]
Length=531

 Score = 32.0 bits (71),  Expect = 2.8, Method: Compositional matrix adjust.
 Identities = 20/52 (38%), Positives = 26/52 (50%), Gaps = 3/52 (6%)

Query  893  WSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPA  944
            WSV++V  ++ SL       E FR   IDG  L  +  E L S + I  GPA
Sbjct  387  WSVDDVCSWLISLD-LDIYCESFRQNAIDGMELSNMTSEVLASDLGI--GPA  435


> XP_022791263.1 nesprin-1-like [Stylophora pistillata]
Length=7500

 Score = 32.0 bits (71),  Expect = 4.0, Method: Composition-based stats.
 Identities = 23/79 (29%), Positives = 33/79 (42%), Gaps = 9/79 (11%)

Query  393  PQATTLTAPQPPQVPPTQQVPPSQSQQQAQTLVVQPMLQSSPLSLPPDAAPKPPIPIQSK  452
            P + T TA +P  + P ++  PS  +++            SP   P  AAPK P   Q K
Sbjct  404  PSSKTSTAEEPMDISPAKETNPSPPKRRET---------RSPSPSPTSAAPKKPRVSQGK  454

Query  453  PPVAPIKPPQLGAAKMSAA  471
              VA     +L   + S A
Sbjct  455  LSVASDHADRLKNKRHSLA  473


> XP_022800614.1 LOW QUALITY PROTEIN: titin-like [Stylophora pistillata]
Length=12750

 Score = 31.6 bits (70),  Expect = 5.3, Method: Composition-based stats.
 Identities = 18/53 (34%), Positives = 30/53 (57%), Gaps = 7/53 (13%)

Query  389  QQQQPQATTLTAPQPPQVPPTQQVP------PSQSQQQAQTLVVQPMLQSSPL  435
            Q+ +   +++ A +PP +PP+Q  P      PSQ +QQ + +V Q  L+ S L
Sbjct  266  QRGEHLISSIKANEPP-IPPSQNFPGSPASLPSQERQQVEGIVTQLNLRCSQL  317


> XP_022784542.1 sterile alpha motif domain-containing protein 
3-like isoform X1 [Stylophora pistillata]
Length=328

 Score = 30.4 bits (67),  Expect = 7.0, Method: Compositional matrix adjust.
 Identities = 21/64 (33%), Positives = 35/64 (55%), Gaps = 2/64 (3%)

Query  894  SVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKICAKINV  953
            SV+EV +++    G    A  F   EIDG+A+  L E  L+  + + +GP +K   ++  
Sbjct  7    SVDEVKQWMIQ-NGFSSHASTFEENEIDGEAMQCLTENALVQLIPV-VGPRMKFIKQLES  64

Query  954  LKET  957
            LK+T
Sbjct  65   LKKT  68


> XP_022803195.1 doublesex- and mab-3-related transcription factor 
A2-like isoform X1 [Stylophora pistillata]
Length=243

 Score = 30.0 bits (66),  Expect = 8.0, Method: Compositional matrix adjust.
 Identities = 18/63 (29%), Positives = 27/63 (43%), Gaps = 9/63 (14%)

Query  753  CEYCGKYAPAEQFRGSKR----FCSMTC-----AKRYNVSCSHQFRLKRKKMKEFQEANY  803
            C  C  +    + +G KR    F    C      ++  V+   Q RL+RK+MKE    NY
Sbjct  20   CTLCFNHGVRSKLKGHKRLGCPFLRCDCKQCVNGRKKRVTMKMQVRLRRKQMKEIGRRNY  79

Query  804  ARV  806
              +
Sbjct  80   QYI  82


> XP_022803202.1 doublesex- and mab-3-related transcription factor 
A2-like isoform X2 [Stylophora pistillata]
Length=238

 Score = 30.0 bits (66),  Expect = 8.0, Method: Compositional matrix adjust.
 Identities = 18/63 (29%), Positives = 27/63 (43%), Gaps = 9/63 (14%)

Query  753  CEYCGKYAPAEQFRGSKR----FCSMTC-----AKRYNVSCSHQFRLKRKKMKEFQEANY  803
            C  C  +    + +G KR    F    C      ++  V+   Q RL+RK+MKE    NY
Sbjct  20   CTLCFNHGVRSKLKGHKRLGCPFLRCDCKQCVNGRKKRVTMKMQVRLRRKQMKEIGRRNY  79

Query  804  ARV  806
              +
Sbjct  80   QYI  82



Lambda      K        H        a         alpha
   0.308    0.122    0.340    0.792     4.96 

Gapped
Lambda      K        H        a         alpha    sigma
   0.267   0.0410    0.140     1.90     42.6     43.6 

Effective search space used: 12295606830


  Database: Cnidaria/GCF_002571385.1_Stylophora_pistillata_v1_protein.faa
    Posted date:  Jun 24, 2020  11:20 AM
  Number of letters in database: 18,275,238
  Number of sequences in database:  33,252



Matrix: BLOSUM62
Gap Penalties: Existence: 11, Extension: 1
Neighboring words threshold: 11
Window for multiple hits: 40
