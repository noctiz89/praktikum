BLASTP 2.6.0+


Reference: Stephen F. Altschul, Thomas L. Madden, Alejandro A.
Schaffer, Jinghui Zhang, Zheng Zhang, Webb Miller, and David J.
Lipman (1997), "Gapped BLAST and PSI-BLAST: a new generation of
protein database search programs", Nucleic Acids Res. 25:3389-3402.


Reference for composition-based statistics: Alejandro A. Schaffer,
L. Aravind, Thomas L. Madden, Sergei Shavirin, John L. Spouge, Yuri
I. Wolf, Eugene V. Koonin, and Stephen F. Altschul (2001),
"Improving the accuracy of PSI-BLAST protein database searches with
composition-based statistics and other refinements", Nucleic Acids
Res. 29:2994-3005.



Database: Cnidaria/GCF_001417965.1_Aiptasia_genome_1.1_protein.faa
           27,753 sequences; 14,880,949 total letters



Query= AAI44122.1 PHC2 protein [Homo sapiens]

Length=830
                                                                      Score     E
Sequences producing significant alignments:                          (Bits)  Value

  XP_020903333.1 polyhomeotic-like protein 2 [Exaiptasia pallida]     125     2e-31
  XP_020916618.1 uncharacterized protein LOC110254004 [Exaiptasia...  81.3    1e-15
  XP_020902826.1 uncharacterized protein LOC110241294 isoform X2 ...  70.1    5e-12
  XP_028515492.1 uncharacterized protein LOC110241294 isoform X1 ...  70.1    5e-12
  XP_028515490.1 uncharacterized protein LOC110241294 isoform X1 ...  70.1    5e-12
  XP_020897473.1 histone acetyltransferase KAT6B isoform X2 [Exai...  67.4    7e-12
  XP_020897472.1 histone acetyltransferase KAT6A isoform X1 [Exai...  66.6    3e-11
  XP_020896074.1 lethal(3)malignant brain tumor-like protein 1 [E...  67.0    4e-11
  XP_020906324.1 lethal(3)malignant brain tumor-like protein 1 [E...  67.0    4e-11
  XP_028513624.1 lethal(3)malignant brain tumor-like protein 3 [E...  65.5    1e-10
  XP_020896090.1 lethal(3)malignant brain tumor-like protein 3 [E...  65.5    1e-10
  XP_020896089.1 lethal(3)malignant brain tumor-like protein 3 [E...  65.5    1e-10
  XP_020899614.1 uncharacterized protein LOC110238295 [Exaiptasia...  62.8    7e-10
  XP_020912289.1 polycomb protein SCMH1 [Exaiptasia pallida]          62.8    8e-10
  XP_020912288.1 polycomb protein SCMH1 [Exaiptasia pallida]          62.8    8e-10
  XP_020915016.1 MBT domain-containing protein 1 isoform X1 [Exai...  62.0    1e-09
  XP_020915017.1 MBT domain-containing protein 1 isoform X2 [Exai...  62.0    2e-09
  XP_028513994.1 scm-like with four MBT domains protein 2 isoform...  58.9    1e-08
  XP_020897274.1 scm-like with four MBT domains protein 2 isoform...  58.9    1e-08
  XP_028513995.1 scm-like with four MBT domains protein 2 isoform...  58.9    1e-08
  XP_020916235.2 uncharacterized protein LOC110253628 [Exaiptasia...  48.1    2e-05
  XP_020912502.1 uncharacterized protein LOC110250236 [Exaiptasia...  34.3    0.53 
  XP_028519136.1 uncharacterized protein LOC114576538 [Exaiptasia...  33.1    0.86 
  XP_020896399.1 uncharacterized protein LOC110235292 isoform X2 ...  32.7    1.0  
  XP_020894959.1 uncharacterized protein LOC110233964 [Exaiptasia...  32.3    1.4  
  XP_020916484.1 LOW QUALITY PROTEIN: uncharacterized protein LOC...  31.2    2.6  
  XP_028512859.1 titin [Exaiptasia pallida]                           31.2    4.4  
  XP_020916393.1 zinc finger protein 184 [Exaiptasia pallida]         30.8    5.7  
  XP_028516916.1 uncharacterized protein LOC110245945 [Exaiptasia...  30.4    7.9  
  XP_028513911.1 uncharacterized protein LOC110235804, partial [E...  30.4    8.4  
  XP_028515425.1 sporulation-specific protein 15 isoform X2 [Exai...  30.0    9.3  


> XP_020903333.1 polyhomeotic-like protein 2 [Exaiptasia pallida]
Length=298

 Score = 125 bits (313),  Expect = 2e-31, Method: Compositional matrix adjust.
 Identities = 88/316 (28%), Positives = 141/316 (45%), Gaps = 66/316 (21%)

Query  533  QAIVKPQILTHVIEGFVIQEGAEPFPVGRSSLLVGNLKKKYAQGFLPEKLPQQDHTTTTD  592
            Q I   ++LTHV+EGF+IQE   PF           L+               DH    D
Sbjct  19   QNIAAHRVLTHVVEGFIIQESNVPFTADDDDDDDDPLRN-----------ANHDHNGDID  67

Query  593  SEMEEPYLQESKEEGAPLKLK---------------CELCGRVDFAYKFKRSKRFCSMAC  637
             E+      E    G+PL                  CE CG     +  +  +RFCS +C
Sbjct  68   MEIGIEADGEVHSPGSPLSNDESNDVFYDSNYPPGYCEQCGTATEVHHQRGPRRFCSTSC  127

Query  638  AKRYNVGCTKRVGLFHSDRSKLQKAGAATHNRRRASKASLPPLTKDTKKQPTGTV--PLS  695
            A+RY+V C++++  FH+   +++   +   +    S+  +P   K       G++  P  
Sbjct  128  ARRYSVSCSRKMMAFHA---RMKSPTSRQMSSSSCSRTGIPSYRKSFHSSTKGSLDHPFE  184

Query  696  ---VTAALQLTHSQEDSSRCSDNSSYEEPLSPISASSSTSRRRQGQRDLELPDMHMRDLV  752
               V A++Q+  ++ D++ C         + P+                           
Sbjct  185  SSMVNASVQV--NEFDNAPCMYFDWSRGEIEPLWIYEHI---------------------  221

Query  753  GMGHHFLPSEPTKWNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKEDHLMSAMNI  812
                     E  KWNV+ V E+IR LPGC+E A+ F ++EIDGQAL+LLKE+H++ A+ +
Sbjct  222  ---------EEAKWNVKQVSEYIRGLPGCEEYADAFVSEEIDGQALMLLKEEHMVVALKM  272

Query  813  KLGPALKIYARISMLK  828
            KLGPALK+ A+++ +K
Sbjct  273  KLGPALKVVAKVNAMK  288


> XP_020916618.1 uncharacterized protein LOC110254004 [Exaiptasia 
pallida]
Length=657

 Score = 81.3 bits (199),  Expect = 1e-15, Method: Compositional matrix adjust.
 Identities = 35/62 (56%), Positives = 45/62 (73%), Gaps = 0/62 (0%)

Query  766  WNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKEDHLMSAMNIKLGPALKIYARIS  825
            W+V  V EF+ SL G  EI  EF+ Q IDGQ+L+LLKE+HL++ + IKLGPALKI A + 
Sbjct  579  WDVNQVCEFVSSLTGSSEIVNEFKEQCIDGQSLILLKEEHLLNKLGIKLGPALKILAHVE  638

Query  826  ML  827
             L
Sbjct  639  KL  640


> XP_020902826.1 uncharacterized protein LOC110241294 isoform X2 
[Exaiptasia pallida]
Length=1690

 Score = 70.1 bits (170),  Expect = 5e-12, Method: Compositional matrix adjust.
 Identities = 53/153 (35%), Positives = 75/153 (49%), Gaps = 24/153 (16%)

Query  693   PLSVT------AALQLTHSQEDSSRCSDNSSYEEPL-------SPISASSSTSRRRQGQR  739
             PLSV+       + +  H+Q +S    D  S  +P        SPIS  S + +RR+   
Sbjct  1541  PLSVSIDNDEGVSFKGKHAQSNSYSTEDGYSSRQPFKSTSSHASPISPKSPSLKRRR---  1597

Query  740   DLELPDMHMRD----LVGMGHHFLPSEPTKWNVEDVYEFIRSLPGCQEIAEEFRAQEIDG  795
                L D+ +       V      LPS P  W+V+ V  ++RS   C   A  F+ +EIDG
Sbjct  1598  ---LVDVGVNTSLPGCVNGRSPDLPSNPLSWSVDQVIRYVRST-DCGHYAVIFKEEEIDG  1653

Query  796   QALLLLKEDHLMSAMNIKLGPALKIYARISMLK  828
             QALL+L  D LM    +KLGP LK+ + IS L+
Sbjct  1654  QALLMLSRDALMQFTPMKLGPTLKMCSYISQLR  1686


> XP_028515492.1 uncharacterized protein LOC110241294 isoform X1 
[Exaiptasia pallida]
Length=1691

 Score = 70.1 bits (170),  Expect = 5e-12, Method: Compositional matrix adjust.
 Identities = 53/153 (35%), Positives = 75/153 (49%), Gaps = 24/153 (16%)

Query  693   PLSVT------AALQLTHSQEDSSRCSDNSSYEEPL-------SPISASSSTSRRRQGQR  739
             PLSV+       + +  H+Q +S    D  S  +P        SPIS  S + +RR+   
Sbjct  1542  PLSVSIDNDEGVSFKGKHAQSNSYSTEDGYSSRQPFKSTSSHASPISPKSPSLKRRR---  1598

Query  740   DLELPDMHMRD----LVGMGHHFLPSEPTKWNVEDVYEFIRSLPGCQEIAEEFRAQEIDG  795
                L D+ +       V      LPS P  W+V+ V  ++RS   C   A  F+ +EIDG
Sbjct  1599  ---LVDVGVNTSLPGCVNGRSPDLPSNPLSWSVDQVIRYVRST-DCGHYAVIFKEEEIDG  1654

Query  796   QALLLLKEDHLMSAMNIKLGPALKIYARISMLK  828
             QALL+L  D LM    +KLGP LK+ + IS L+
Sbjct  1655  QALLMLSRDALMQFTPMKLGPTLKMCSYISQLR  1687


> XP_028515490.1 uncharacterized protein LOC110241294 isoform X1 
[Exaiptasia pallida]
Length=1691

 Score = 70.1 bits (170),  Expect = 5e-12, Method: Compositional matrix adjust.
 Identities = 53/153 (35%), Positives = 75/153 (49%), Gaps = 24/153 (16%)

Query  693   PLSVT------AALQLTHSQEDSSRCSDNSSYEEPL-------SPISASSSTSRRRQGQR  739
             PLSV+       + +  H+Q +S    D  S  +P        SPIS  S + +RR+   
Sbjct  1542  PLSVSIDNDEGVSFKGKHAQSNSYSTEDGYSSRQPFKSTSSHASPISPKSPSLKRRR---  1598

Query  740   DLELPDMHMRD----LVGMGHHFLPSEPTKWNVEDVYEFIRSLPGCQEIAEEFRAQEIDG  795
                L D+ +       V      LPS P  W+V+ V  ++RS   C   A  F+ +EIDG
Sbjct  1599  ---LVDVGVNTSLPGCVNGRSPDLPSNPLSWSVDQVIRYVRST-DCGHYAVIFKEEEIDG  1654

Query  796   QALLLLKEDHLMSAMNIKLGPALKIYARISMLK  828
             QALL+L  D LM    +KLGP LK+ + IS L+
Sbjct  1655  QALLMLSRDALMQFTPMKLGPTLKMCSYISQLR  1687


> XP_020897473.1 histone acetyltransferase KAT6B isoform X2 [Exaiptasia 
pallida]
Length=311

 Score = 67.4 bits (163),  Expect = 7e-12, Method: Compositional matrix adjust.
 Identities = 32/70 (46%), Positives = 49/70 (70%), Gaps = 1/70 (1%)

Query  759  LPSEPTKWNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKEDHLMSAMNIKLGPAL  818
            + ++ +KW V DV +F+ S+   QE A+ F  QEIDG+ALLL+  + +++ M +KLGPAL
Sbjct  238  MMADASKWEVADVMKFLESVGFPQE-AKNFGDQEIDGKALLLMSRNDVLTGMALKLGPAL  296

Query  819  KIYARISMLK  828
            KIY  +S L+
Sbjct  297  KIYVHVSKLQ  306


> XP_020897472.1 histone acetyltransferase KAT6A isoform X1 [Exaiptasia 
pallida]
Length=464

 Score = 66.6 bits (161),  Expect = 3e-11, Method: Compositional matrix adjust.
 Identities = 32/70 (46%), Positives = 49/70 (70%), Gaps = 1/70 (1%)

Query  759  LPSEPTKWNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKEDHLMSAMNIKLGPAL  818
            + ++ +KW V DV +F+ S+   QE A+ F  QEIDG+ALLL+  + +++ M +KLGPAL
Sbjct  391  MMADASKWEVADVMKFLESVGFPQE-AKNFGDQEIDGKALLLMSRNDVLTGMALKLGPAL  449

Query  819  KIYARISMLK  828
            KIY  +S L+
Sbjct  450  KIYVHVSKLQ  459


> XP_020896074.1 lethal(3)malignant brain tumor-like protein 1 
[Exaiptasia pallida]
Length=1025

 Score = 67.0 bits (162),  Expect = 4e-11, Method: Compositional matrix adjust.
 Identities = 28/61 (46%), Positives = 46/61 (75%), Gaps = 1/61 (2%)

Query  764   TKWNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKEDHLMSAMNIKLGPALKIYAR  823
             +KW+++DV + ++SL GC E A+ F  Q+IDG++ LLL ++ +++ + IKLGPALKI+  
Sbjct  963   SKWSIDDVVKHVKSL-GCTEQAKLFEEQKIDGESFLLLTQNDIVNILKIKLGPALKIFKS  1021

Query  824   I  824
             I
Sbjct  1022  I  1022


 Score = 32.3 bits (72),  Expect = 1.7, Method: Compositional matrix adjust.
 Identities = 13/30 (43%), Positives = 16/30 (53%), Gaps = 0/30 (0%)

Query  612  LKCELCGRVDFAYKFKRSKRFCSMACAKRY  641
            L CE+CG       F  S RFCS++C   Y
Sbjct  279  LCCEVCGIYGLPQDFCSSGRFCSLSCVGSY  308


> XP_020906324.1 lethal(3)malignant brain tumor-like protein 1 
[Exaiptasia pallida]
Length=1121

 Score = 67.0 bits (162),  Expect = 4e-11, Method: Compositional matrix adjust.
 Identities = 28/61 (46%), Positives = 46/61 (75%), Gaps = 1/61 (2%)

Query  764   TKWNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKEDHLMSAMNIKLGPALKIYAR  823
             +KW+++DV + ++SL GC E A+ F  Q+IDG++ LLL ++ +++ + IKLGPALKI+  
Sbjct  1059  SKWSIDDVVKHVKSL-GCTEQAKLFEEQKIDGESFLLLTQNDIVNILKIKLGPALKIFKS  1117

Query  824   I  824
             I
Sbjct  1118  I  1118


 Score = 32.3 bits (72),  Expect = 1.6, Method: Compositional matrix adjust.
 Identities = 13/30 (43%), Positives = 16/30 (53%), Gaps = 0/30 (0%)

Query  612  LKCELCGRVDFAYKFKRSKRFCSMACAKRY  641
            L CE+CG       F  S RFCS++C   Y
Sbjct  375  LCCEVCGIYGLPQDFCSSGRFCSLSCVGSY  404


> XP_028513624.1 lethal(3)malignant brain tumor-like protein 3 
[Exaiptasia pallida]
Length=971

 Score = 65.5 bits (158),  Expect = 1e-10, Method: Compositional matrix adjust.
 Identities = 32/60 (53%), Positives = 41/60 (68%), Gaps = 1/60 (2%)

Query  765  KWNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKEDHLMSAMNIKLGPALKIYARI  824
            KW  E V + + SL GC E A+ F  QEIDG+A LLL +  +++ MNIKLGPALKI+  I
Sbjct  910  KWTTEQVEKHVMSL-GCTEQAKLFVQQEIDGEAFLLLTQSDIVNIMNIKLGPALKIFNSI  968


 Score = 35.8 bits (81),  Expect = 0.17, Method: Compositional matrix adjust.
 Identities = 13/30 (43%), Positives = 17/30 (57%), Gaps = 0/30 (0%)

Query  612  LKCELCGRVDFAYKFKRSKRFCSMACAKRY  641
            L CE+CG+      F  S RFCS+ C  +Y
Sbjct  245  LCCEVCGKYGMPKDFSASGRFCSLKCVGQY  274


> XP_020896090.1 lethal(3)malignant brain tumor-like protein 3 
[Exaiptasia pallida]
Length=971

 Score = 65.5 bits (158),  Expect = 1e-10, Method: Compositional matrix adjust.
 Identities = 32/60 (53%), Positives = 41/60 (68%), Gaps = 1/60 (2%)

Query  765  KWNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKEDHLMSAMNIKLGPALKIYARI  824
            KW  E V + + SL GC E A+ F  QEIDG+A LLL +  +++ MNIKLGPALKI+  I
Sbjct  910  KWTTEQVEKHVMSL-GCTEQAKLFVQQEIDGEAFLLLTQSDIVNIMNIKLGPALKIFNSI  968


 Score = 35.8 bits (81),  Expect = 0.17, Method: Compositional matrix adjust.
 Identities = 13/30 (43%), Positives = 17/30 (57%), Gaps = 0/30 (0%)

Query  612  LKCELCGRVDFAYKFKRSKRFCSMACAKRY  641
            L CE+CG+      F  S RFCS+ C  +Y
Sbjct  245  LCCEVCGKYGMPKDFSASGRFCSLKCVGQY  274


> XP_020896089.1 lethal(3)malignant brain tumor-like protein 3 
[Exaiptasia pallida]
Length=971

 Score = 65.5 bits (158),  Expect = 1e-10, Method: Compositional matrix adjust.
 Identities = 32/60 (53%), Positives = 41/60 (68%), Gaps = 1/60 (2%)

Query  765  KWNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKEDHLMSAMNIKLGPALKIYARI  824
            KW  E V + + SL GC E A+ F  QEIDG+A LLL +  +++ MNIKLGPALKI+  I
Sbjct  910  KWTTEQVEKHVMSL-GCTEQAKLFVQQEIDGEAFLLLTQSDIVNIMNIKLGPALKIFNSI  968


 Score = 35.8 bits (81),  Expect = 0.17, Method: Compositional matrix adjust.
 Identities = 13/30 (43%), Positives = 17/30 (57%), Gaps = 0/30 (0%)

Query  612  LKCELCGRVDFAYKFKRSKRFCSMACAKRY  641
            L CE+CG+      F  S RFCS+ C  +Y
Sbjct  245  LCCEVCGKYGMPKDFSASGRFCSLKCVGQY  274


> XP_020899614.1 uncharacterized protein LOC110238295 [Exaiptasia 
pallida]
Length=741

 Score = 62.8 bits (151),  Expect = 7e-10, Method: Compositional matrix adjust.
 Identities = 27/65 (42%), Positives = 44/65 (68%), Gaps = 1/65 (2%)

Query  764  TKWNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKEDHLMSAMNIKLGPALKIYAR  823
            + W  ++V EFI S   C E A+ F  Q+IDG+A+LLL +D  ++ +N+KLG A+K+Y  
Sbjct  673  SNWTAQNVAEFI-SATDCSESAKIFIEQDIDGKAVLLLSQDIFVNRLNLKLGTAIKLYNH  731

Query  824  ISMLK  828
            ++ L+
Sbjct  732  VANLR  736


> XP_020912289.1 polycomb protein SCMH1 [Exaiptasia pallida]
Length=822

 Score = 62.8 bits (151),  Expect = 8e-10, Method: Compositional matrix adjust.
 Identities = 30/66 (45%), Positives = 44/66 (67%), Gaps = 7/66 (11%)

Query  766  WNVEDVYEFIRSLPGCQEIAEE---FRAQEIDGQALLLLKEDHLMSAMNIKLGPALKIYA  822
            W+V++V ++I S     E+A+    FR  EIDG+ALLLL  D +MS M +KLGPA+K+ +
Sbjct  754  WSVDEVIKYIES----TELADHVTLFRVHEIDGRALLLLNRDMIMSYMGLKLGPAIKLLS  809

Query  823  RISMLK  828
             +  LK
Sbjct  810  IVDDLK  815


> XP_020912288.1 polycomb protein SCMH1 [Exaiptasia pallida]
Length=822

 Score = 62.8 bits (151),  Expect = 8e-10, Method: Compositional matrix adjust.
 Identities = 30/66 (45%), Positives = 44/66 (67%), Gaps = 7/66 (11%)

Query  766  WNVEDVYEFIRSLPGCQEIAEE---FRAQEIDGQALLLLKEDHLMSAMNIKLGPALKIYA  822
            W+V++V ++I S     E+A+    FR  EIDG+ALLLL  D +MS M +KLGPA+K+ +
Sbjct  754  WSVDEVIKYIES----TELADHVTLFRVHEIDGRALLLLNRDMIMSYMGLKLGPAIKLLS  809

Query  823  RISMLK  828
             +  LK
Sbjct  810  IVDDLK  815


> XP_020915016.1 MBT domain-containing protein 1 isoform X1 [Exaiptasia 
pallida]
Length=1010

 Score = 62.0 bits (149),  Expect = 1e-09, Method: Compositional matrix adjust.
 Identities = 32/69 (46%), Positives = 43/69 (62%), Gaps = 1/69 (1%)

Query  762   EPTKWNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKEDHLMSAMNIKLGPALKIY  821
             +P  WN++DV  F+ S+ GC   AE F  +EIDG+ALLLL ++ L S    KLGP  KI+
Sbjct  940   DPRTWNMQDVANFMLSI-GCSNYAEAFIKEEIDGKALLLLTQEMLESLTENKLGPMTKIH  998

Query  822   ARISMLKDS  830
               +  LK S
Sbjct  999   NAVRALKQS  1007


 Score = 40.8 bits (94),  Expect = 0.004, Method: Compositional matrix adjust.
 Identities = 17/37 (46%), Positives = 21/37 (57%), Gaps = 1/37 (3%)

Query  613  KCELCGRVDFAYKF-KRSKRFCSMACAKRYNVGCTKR  648
            KCE C  +     F  +SKRFC M CAKRY+    K+
Sbjct  108  KCEFCSHIGIRESFFSKSKRFCKMECAKRYSASNIKK  144


> XP_020915017.1 MBT domain-containing protein 1 isoform X2 [Exaiptasia 
pallida]
Length=1002

 Score = 62.0 bits (149),  Expect = 2e-09, Method: Compositional matrix adjust.
 Identities = 32/69 (46%), Positives = 43/69 (62%), Gaps = 1/69 (1%)

Query  762  EPTKWNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKEDHLMSAMNIKLGPALKIY  821
            +P  WN++DV  F+ S+ GC   AE F  +EIDG+ALLLL ++ L S    KLGP  KI+
Sbjct  932  DPRTWNMQDVANFMLSI-GCSNYAEAFIKEEIDGKALLLLTQEMLESLTENKLGPMTKIH  990

Query  822  ARISMLKDS  830
              +  LK S
Sbjct  991  NAVRALKQS  999


 Score = 40.8 bits (94),  Expect = 0.005, Method: Compositional matrix adjust.
 Identities = 17/37 (46%), Positives = 21/37 (57%), Gaps = 1/37 (3%)

Query  613  KCELCGRVDFAYKF-KRSKRFCSMACAKRYNVGCTKR  648
            KCE C  +     F  +SKRFC M CAKRY+    K+
Sbjct  108  KCEFCSHIGIRESFFSKSKRFCKMECAKRYSASNIKK  144


> XP_028513994.1 scm-like with four MBT domains protein 2 isoform 
X1 [Exaiptasia pallida]
Length=1063

 Score = 58.9 bits (141),  Expect = 1e-08, Method: Compositional matrix adjust.
 Identities = 24/63 (38%), Positives = 43/63 (68%), Gaps = 1/63 (2%)

Query  766   WNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKEDHLMSAMNIKLGPALKIYARIS  825
             W+++DV EF+ +  GC++ A  F+ QE+DG+AL+LL  D +   + + LGPA+K++  + 
Sbjct  998   WSMDDVIEFV-TPRGCKKFASVFQEQEVDGKALMLLSLDDIHKVLGVTLGPAIKLHDDVQ  1056

Query  826   MLK  828
              L+
Sbjct  1057  KLR  1059


> XP_020897274.1 scm-like with four MBT domains protein 2 isoform 
X1 [Exaiptasia pallida]
Length=1063

 Score = 58.9 bits (141),  Expect = 1e-08, Method: Compositional matrix adjust.
 Identities = 24/63 (38%), Positives = 43/63 (68%), Gaps = 1/63 (2%)

Query  766   WNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKEDHLMSAMNIKLGPALKIYARIS  825
             W+++DV EF+ +  GC++ A  F+ QE+DG+AL+LL  D +   + + LGPA+K++  + 
Sbjct  998   WSMDDVIEFV-TPRGCKKFASVFQEQEVDGKALMLLSLDDIHKVLGVTLGPAIKLHDDVQ  1056

Query  826   MLK  828
              L+
Sbjct  1057  KLR  1059


> XP_028513995.1 scm-like with four MBT domains protein 2 isoform 
X2 [Exaiptasia pallida]
Length=1062

 Score = 58.9 bits (141),  Expect = 1e-08, Method: Compositional matrix adjust.
 Identities = 24/63 (38%), Positives = 43/63 (68%), Gaps = 1/63 (2%)

Query  766   WNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKEDHLMSAMNIKLGPALKIYARIS  825
             W+++DV EF+ +  GC++ A  F+ QE+DG+AL+LL  D +   + + LGPA+K++  + 
Sbjct  997   WSMDDVIEFV-TPRGCKKFASVFQEQEVDGKALMLLSLDDIHKVLGVTLGPAIKLHDDVQ  1055

Query  826   MLK  828
              L+
Sbjct  1056  KLR  1058


> XP_020916235.2 uncharacterized protein LOC110253628 [Exaiptasia 
pallida]
Length=388

 Score = 48.1 bits (113),  Expect = 2e-05, Method: Compositional matrix adjust.
 Identities = 26/67 (39%), Positives = 38/67 (57%), Gaps = 3/67 (4%)

Query  764  TKWNVEDVYEFIRSLPGCQEIAEEF-RAQEIDGQALLLLKEDHLMSAMNIKLGPALKIYA  822
            +KW+V +V ++ ++   C E    F   QEIDG AL+L++E  L+  M  KLGP LK   
Sbjct  322  SKWSVAEVAKYYKNQEECSEAMVAFIEEQEIDGNALMLIEESSLLYFM--KLGPVLKFLN  379

Query  823  RISMLKD  829
                LK+
Sbjct  380  LREKLKN  386


> XP_020912502.1 uncharacterized protein LOC110250236 [Exaiptasia 
pallida]
Length=2676

 Score = 34.3 bits (77),  Expect = 0.53, Method: Compositional matrix adjust.
 Identities = 26/86 (30%), Positives = 40/86 (47%), Gaps = 13/86 (15%)

Query  175  SSPALTASQAQMYLRAQMVQNLTLRTQQTPAAAASGPTPTQPVLPSLALKPTPGGSQP--  232
            ++ A+T S+  M + ++   N  + T   P+      TPTQ V PS A KPT  GS+   
Sbjct  518  TTAAITISKENMVVVSEQPSNANMNTNPAPSNPVHQNTPTQMVSPSDAKKPTGAGSRNNK  577

Query  233  -----------LPTPAQSRNTAQASP  247
                       +  P++S+NT Q  P
Sbjct  578  PNNFDISFHTHVHNPSKSKNTVQIRP  603


> XP_028519136.1 uncharacterized protein LOC114576538 [Exaiptasia 
pallida]
Length=388

 Score = 33.1 bits (74),  Expect = 0.86, Method: Compositional matrix adjust.
 Identities = 16/38 (42%), Positives = 26/38 (68%), Gaps = 1/38 (3%)

Query  759  LPSEPTKWNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQ  796
            LP+ P +W+  +V+ +I  L    EIA +F+A+E+DGQ
Sbjct  57   LPNNPKEWSCANVHAWITKLFDG-EIAGKFQAEEVDGQ  93


> XP_020896399.1 uncharacterized protein LOC110235292 isoform X2 
[Exaiptasia pallida]
Length=340

 Score = 32.7 bits (73),  Expect = 1.0, Method: Compositional matrix adjust.
 Identities = 14/41 (34%), Positives = 25/41 (61%), Gaps = 0/41 (0%)

Query  759  LPSEPTKWNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALL  799
            LP E  +W+  DV +++ S    ++I + F+ Q+IDG  L+
Sbjct  22   LPKEVREWDTGDVVQWLHSKGIEEQIKKGFKDQKIDGLCLM  62


> XP_020894959.1 uncharacterized protein LOC110233964 [Exaiptasia 
pallida]
Length=457

 Score = 32.3 bits (72),  Expect = 1.4, Method: Compositional matrix adjust.
 Identities = 19/54 (35%), Positives = 31/54 (57%), Gaps = 1/54 (2%)

Query  759  LPSEPTKWNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKEDHLMSAMNI  812
            +P + T  NVE V + ++ L   Q I E+F+ + IDG  L+ L ED +  ++ I
Sbjct  386  IPDDLTALNVEGVTDSLKYLKMDQYI-EKFKEELIDGTMLVELSEDMMEESLGI  438


> XP_020916484.1 LOW QUALITY PROTEIN: uncharacterized protein LOC110253872 
[Exaiptasia pallida]
Length=284

 Score = 31.2 bits (69),  Expect = 2.6, Method: Compositional matrix adjust.
 Identities = 13/44 (30%), Positives = 28/44 (64%), Gaps = 0/44 (0%)

Query  760  PSEPTKWNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKE  803
            P+    W+V +V +   +  G   ++E+F+A +I+G+ L+L++E
Sbjct  43   PTSVQAWSVHEVCQNFLARIGMDFLSEQFQANKINGKCLILIQE  86


> XP_028512859.1 titin [Exaiptasia pallida]
Length=17855

 Score = 31.2 bits (69),  Expect = 4.4, Method: Composition-based stats.
 Identities = 46/237 (19%), Positives = 89/237 (38%), Gaps = 20/237 (8%)

Query  104    QSLAAVQQASLVSNRQGSTSGSNVSAQAPAQSSSINLAASPAAAQLLNRAQSVNSAAASG  163
              ++L A++  + +S   GS      S Q+                 LL R ++++ A    
Sbjct  17296  KALTAIRSTNFLSRLLGSRGTEEGSKQSGGGGGG----------GLLARVKAMHVAGV--  17343

Query  164    IAQQAVLLGNTSSPALTASQAQMYLRAQMVQNLTLRTQQTPAAAASGPTPTQPVLPSLAL  223
                 QA      SSP L+   A     A+   + + + + + A++ + P  T   +P    
Sbjct  17344  ---QAPESAPASSPFLSPDSAAEQKPAEKKPSSSEKKETSTASSTTTPDKTPNGVP----  17396

Query  224    KPTPGGSQPLPTPAQSRNTAQASPAGAKPGIADSVMEPHKKGDGNSSVPGSMEGRAGLSR  283
              KP P  S+   + +    + + +P+  K  +  S  +  K    N      +E +   S+
Sbjct  17397  KPEPESSKSPASKSNDVTSNRNNPSALKDNLTKSTKDDSKTIVSNDISKKEVESKPTASK  17456

Query  284    TVPAVAAHPLIAPAYAQLQPHQLLPQPSSKHLQPQFVIQQQPQPQQQQ-PPPQQSRP  339
                 +  +     P+       +   +PSS     +     QP+ +  Q PPP + RP
Sbjct  17457  DSTSKVSKESTTPSTKISNNEKSTSEPSSTRSTSRTPKGPQPKGKLPQWPPPPKERP  17513


> XP_020916393.1 zinc finger protein 184 [Exaiptasia pallida]
Length=1263

 Score = 30.8 bits (68),  Expect = 5.7, Method: Compositional matrix adjust.
 Identities = 30/115 (26%), Positives = 50/115 (43%), Gaps = 14/115 (12%)

Query  531  PPQAIVKPQILTHVIEGFVIQEGAEPFP----VGRSSLLVGNLKKKYAQGFLPEKLPQQD  586
            P +  ++P+  ++++ G V+    EP P     GRS    G + KK  Q  L        
Sbjct  74   PTKVKIEPKECSNLVSGEVVTVILEPCPENAKKGRSGSSKGKVNKKPVQTAL--------  125

Query  587  HTTTTDSEMEEPYLQESKEEGAPLKLKCELCGRVDFAYKFKRSKRFCSMACAKRY  641
             +T  D    +  L+E +    P K+K E+    +F  K+ + K F S A   R+
Sbjct  126  -STDVDQSSRKRKLEEDESTSIPTKIKKEVSRVEEFVCKYCK-KSFSSKALLSRH  178


> XP_028516916.1 uncharacterized protein LOC110245945 [Exaiptasia 
pallida]
Length=3578

 Score = 30.4 bits (67),  Expect = 7.9, Method: Compositional matrix adjust.
 Identities = 25/57 (44%), Positives = 30/57 (53%), Gaps = 7/57 (12%)

Query  289  AAHPLIAPAYAQLQPHQLLPQP--SSKHLQPQFVIQQQPQPQQQQP--PPQQSRPVL  341
            AA PL   + A +QP Q+ PQP  S   +QPQ   Q  PQ  QQQ   PPQ   P +
Sbjct  249  AAQPLQDMSQAAVQPQQVSPQPDMSQYAMQPQ---QDMPQDAQQQDAAPPQMMAPNM  302


> XP_028513911.1 uncharacterized protein LOC110235804, partial 
[Exaiptasia pallida]
Length=2910

 Score = 30.4 bits (67),  Expect = 8.4, Method: Composition-based stats.
 Identities = 20/74 (27%), Positives = 30/74 (41%), Gaps = 3/74 (4%)

Query  203  TPAAAASGPTPTQPVLPSLALKPTPGGSQPLPTPAQSRNTAQASPAGAKPGIADSVMEPH  262
            TPA  A+  TPT    P+    P+ G +   PT   +         GA P    +  +  
Sbjct  629  TPADGATPTTPTDGATPTDGATPSDGDT---PTDGATPTDGDTPTDGATPTDGSTPSDGA  685

Query  263  KKGDGNSSVPGSME  276
               DG +SV  ++E
Sbjct  686  TPTDGAASVSSTVE  699


> XP_028515425.1 sporulation-specific protein 15 isoform X2 [Exaiptasia 
pallida]
Length=2871

 Score = 30.0 bits (66),  Expect = 9.3, Method: Composition-based stats.
 Identities = 27/100 (27%), Positives = 46/100 (46%), Gaps = 10/100 (10%)

Query  653   HSDRSKLQKAGAATHNRRRASKASLPPLTKDTKKQPTGTVPLSVTAALQLTHSQEDSSRC  712
             H  +SKL+     T NR RA +  L  + +D +       PL V    +L    E   + 
Sbjct  1470  HGHKSKLESQLDHTRNRNRALEKELEAIKRDYE-------PLRVKYE-ELVRELELLKKD  1521

Query  713   SDNSSYEEPLSPISASSSTSRRRQGQRDLE--LPDMHMRD  750
             SD++        +   + T++R+Q QRDL+  L D+  ++
Sbjct  1522  SDDAHNRLGDVEVQLDAETTKRKQLQRDLDDALEDLQQKE  1561



Lambda      K        H        a         alpha
   0.310    0.125    0.359    0.792     4.96 

Gapped
Lambda      K        H        a         alpha    sigma
   0.267   0.0410    0.140     1.90     42.6     43.6 

Effective search space used: 8548083712


  Database: Cnidaria/GCF_001417965.1_Aiptasia_genome_1.1_protein.faa
    Posted date:  Jun 24, 2020  11:20 AM
  Number of letters in database: 14,880,949
  Number of sequences in database:  27,753



Matrix: BLOSUM62
Gap Penalties: Existence: 11, Extension: 1
Neighboring words threshold: 11
Window for multiple hits: 40
