BLASTP 2.6.0+


Reference: Stephen F. Altschul, Thomas L. Madden, Alejandro A.
Schaffer, Jinghui Zhang, Zheng Zhang, Webb Miller, and David J.
Lipman (1997), "Gapped BLAST and PSI-BLAST: a new generation of
protein database search programs", Nucleic Acids Res. 25:3389-3402.


Reference for composition-based statistics: Alejandro A. Schaffer,
L. Aravind, Thomas L. Madden, Sergei Shavirin, John L. Spouge, Yuri
I. Wolf, Eugene V. Koonin, and Stephen F. Altschul (2001),
"Improving the accuracy of PSI-BLAST protein database searches with
composition-based statistics and other refinements", Nucleic Acids
Res. 29:2994-3005.



Database: Cnidaria/GCF_004324835.1_DenGig_1.0_protein.faa
           28,741 sequences; 17,717,918 total letters



Query= AAH73964.1 PHC1 protein [Homo sapiens]

Length=957
                                                                      Score     E
Sequences producing significant alignments:                          (Bits)  Value

  XP_028393778.1 polyhomeotic-like protein 2 isoform X2 [Dendrone...  102     6e-24
  XP_028393777.1 polyhomeotic-like protein 2 isoform X1 [Dendrone...  100     4e-23
  XP_028394259.1 uncharacterized protein LOC114518466 [Dendroneph...  87.0    2e-17
  XP_028409566.1 sex comb on midleg-like protein 2 isoform X2 [De...  67.4    4e-11
  XP_028409565.1 sex comb on midleg-like protein 2 isoform X1 [De...  67.0    5e-11
  XP_028398470.1 putative GPI-anchored protein pfl2 [Dendronephth...  63.2    4e-10
  XP_028402346.1 lethal(3)malignant brain tumor-like protein 1 is...  55.8    1e-07
  XP_028402345.1 lethal(3)malignant brain tumor-like protein 1 is...  55.8    1e-07
  XP_028402344.1 lethal(3)malignant brain tumor-like protein 1 is...  55.8    1e-07
  XP_028395289.1 uncharacterized protein LOC114519364 [Dendroneph...  54.3    5e-07
  XP_028404723.1 histone acetyltransferase KAT6A-like [Dendroneph...  53.1    8e-07
  XP_028400132.1 MBT domain-containing protein 1-like isoform X3 ...  44.7    4e-04
  XP_028400130.1 MBT domain-containing protein 1-like isoform X1 ...  44.7    5e-04
  XP_028400131.1 MBT domain-containing protein 1-like isoform X2 ...  44.3    6e-04
  XP_028410478.1 uncharacterized protein LOC114533086 isoform X2 ...  42.0    0.003
  XP_028410472.1 uncharacterized protein LOC114533086 isoform X1 ...  42.0    0.003
  XP_028410482.1 uncharacterized protein LOC114533086 isoform X3 ...  42.0    0.003
  XP_028392389.1 uncharacterized protein LOC114516961 [Dendroneph...  40.8    0.004
  XP_028413638.1 uncharacterized protein LOC114536487 [Dendroneph...  40.4    0.006
  XP_028397165.1 SH3 and multiple ankyrin repeat domains protein ...  40.8    0.006
  XP_028414671.1 uncharacterized protein LOC114537777 [Dendroneph...  40.0    0.008
  XP_028397135.1 uncharacterized protein LOC114520968 [Dendroneph...  40.0    0.008
  XP_028399973.1 uncharacterized protein LOC114523295 [Dendroneph...  40.0    0.009
  XP_028418060.1 uncharacterized protein LOC114542872 [Dendroneph...  40.0    0.010
  XP_028417239.1 uncharacterized protein LOC114541522 [Dendroneph...  40.0    0.010
  XP_028408983.1 LOW QUALITY PROTEIN: muscle M-line assembly prot...  37.4    0.085
  XP_028391350.1 phospholipase DDHD2-like [Dendronephthya gigantea]   33.5    1.0  
  XP_028393858.1 uncharacterized protein LOC114518127 isoform X3 ...  33.1    1.5  
  XP_028393856.1 uncharacterized protein LOC114518127 isoform X1 ...  33.1    1.5  
  XP_028393857.1 uncharacterized protein LOC114518127 isoform X2 ...  32.7    1.7  
  XP_028413398.1 microtubule-associated protein futsch-like [Dend...  32.7    1.9  
  XP_028406223.1 suppression of tumorigenicity 18 protein-like is...  31.6    4.0  
  XP_028406222.1 myelin transcription factor 1-like isoform X1 [D...  31.6    4.0  
  XP_028410284.1 THAP domain-containing protein 1-like [Dendronep...  30.4    7.1  


> XP_028393778.1 polyhomeotic-like protein 2 isoform X2 [Dendronephthya 
gigantea]
Length=269

 Score = 102 bits (255),  Expect = 6e-24, Method: Composition-based stats.
 Identities = 81/279 (29%), Positives = 126/279 (45%), Gaps = 40/279 (14%)

Query  683  KPQILTHIIEGFVIQEGAEPFPV-GCSQLLKE--SEKPLQTGLPTGLTENQSGGSLGVDS  739
            +P +L H IE F+IQE + PF V G +    +  +++P   G P   TEN+   +   +S
Sbjct  9    EPCVLNHFIENFIIQESSWPFTVKGQTYEFGKLPTDEPDIMGTPFFETENEDFATSEEES  68

Query  740  PSAELDKKANLLKCEYCGK--YAPAEQFRGSKRFCSMTCAKRYNVSCSHQFRLKRKKMKE  797
                 D    L  C YCG+   + A  F  SK+FCS TCA+ + +    +       +  
Sbjct  69   SEGSFD----LDNCSYCGRACTSMARSFE-SKKFCSNTCARLFGIPVPKRGGYSGSGIGG  123

Query  798  FQEANYARVRRRGPRRSSSDIARAKIQGKCHRGQE-DSSRGSDNSSYDEALSPTSPGPLS  856
                 +     R P      + +++   + +  Q  ++S   D +   EA    S   L 
Sbjct  124  RGRGGWKHYLNRKP-----SVTQSRKMARSYSNQSYEASTQVDETILREAFDGRSDNELD  178

Query  857  VRAGHGERDLGNPNTAPPTPELHGINPVFLSSNPSRWSVEEVYEFIASLQGCQEIAEEFR  916
                     L      P                 ++W+V+EV  F+  + GC E AE F 
Sbjct  179  C--------LWQFEYVPA----------------AKWNVKEVAAFVRDVSGCSEYAESFV  214

Query  917  SQEIDGQALLLLKEEHLMSAMNIKLGPALKICAKINVLK  955
            SQEIDG AL+L+KEEHL+  + +KLGPALK+  ++N +K
Sbjct  215  SQEIDGYALMLVKEEHLVVGLQMKLGPALKLVGRVNQMK  253


> XP_028393777.1 polyhomeotic-like protein 2 isoform X1 [Dendronephthya 
gigantea]
Length=272

 Score = 100 bits (249),  Expect = 4e-23, Method: Composition-based stats.
 Identities = 79/279 (28%), Positives = 124/279 (44%), Gaps = 37/279 (13%)

Query  683  KPQILTHIIEGFVIQEGAEPFPV-GCSQLLKE--SEKPLQTGLPTGLTENQSGGSLGVDS  739
            +P +L H IE F+IQE + PF V G +    +  +++P   G P   TEN+         
Sbjct  9    EPCVLNHFIENFIIQESSWPFTVKGQTYEFGKLPTDEPDIMGTPFFETENEVFTDFATSE  68

Query  740  PSAELDKKANLLKCEYCGK--YAPAEQFRGSKRFCSMTCAKRYNVSCSHQFRLKRKKMKE  797
                 +   +L  C YCG+   + A  F  SK+FCS TCA+ + +    +       +  
Sbjct  69   -EESSEGSFDLDNCSYCGRACTSMARSFE-SKKFCSNTCARLFGIPVPKRGGYSGSGIGG  126

Query  798  FQEANYARVRRRGPRRSSSDIARAKIQGKCHRGQE-DSSRGSDNSSYDEALSPTSPGPLS  856
                 +     R P      + +++   + +  Q  ++S   D +   EA    S   L 
Sbjct  127  RGRGGWKHYLNRKP-----SVTQSRKMARSYSNQSYEASTQVDETILREAFDGRSDNELD  181

Query  857  VRAGHGERDLGNPNTAPPTPELHGINPVFLSSNPSRWSVEEVYEFIASLQGCQEIAEEFR  916
                     L      P                 ++W+V+EV  F+  + GC E AE F 
Sbjct  182  C--------LWQFEYVPA----------------AKWNVKEVAAFVRDVSGCSEYAESFV  217

Query  917  SQEIDGQALLLLKEEHLMSAMNIKLGPALKICAKINVLK  955
            SQEIDG AL+L+KEEHL+  + +KLGPALK+  ++N +K
Sbjct  218  SQEIDGYALMLVKEEHLVVGLQMKLGPALKLVGRVNQMK  256


> XP_028394259.1 uncharacterized protein LOC114518466 [Dendronephthya 
gigantea]
Length=567

 Score = 87.0 bits (214),  Expect = 2e-17, Method: Compositional matrix adjust.
 Identities = 36/63 (57%), Positives = 49/63 (78%), Gaps = 0/63 (0%)

Query  893  WSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKICAKIN  952
            W +EEV +F++S+ GC E  + FR QEIDGQAL+LL EEHL + M +KLGPALK+ +KI+
Sbjct  500  WDIEEVCKFVSSVTGCSEYCDVFREQEIDGQALILLTEEHLSNKMGLKLGPALKLKSKID  559

Query  953  VLK  955
             L+
Sbjct  560  ELR  562


> XP_028409566.1 sex comb on midleg-like protein 2 isoform X2 [Dendronephthya 
gigantea]
Length=699

 Score = 67.4 bits (163),  Expect = 4e-11, Method: Compositional matrix adjust.
 Identities = 36/79 (46%), Positives = 49/79 (62%), Gaps = 1/79 (1%)

Query  877  ELHGINPVFLSSNPSRWSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSA  936
            EL+ ++ +  +S+P  WS++EV E+ A      + AE F+  EIDG ALLLL  E +MS 
Sbjct  617  ELNEVSDIGSTSSPLTWSIDEVVEYFAK-SDISKYAELFKQHEIDGGALLLLNRETIMSC  675

Query  937  MNIKLGPALKICAKINVLK  955
            M  KLGPALK+   I  LK
Sbjct  676  MQFKLGPALKLLNHIAELK  694


> XP_028409565.1 sex comb on midleg-like protein 2 isoform X1 [Dendronephthya 
gigantea]
Length=729

 Score = 67.0 bits (162),  Expect = 5e-11, Method: Compositional matrix adjust.
 Identities = 36/79 (46%), Positives = 49/79 (62%), Gaps = 1/79 (1%)

Query  877  ELHGINPVFLSSNPSRWSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSA  936
            EL+ ++ +  +S+P  WS++EV E+ A      + AE F+  EIDG ALLLL  E +MS 
Sbjct  647  ELNEVSDIGSTSSPLTWSIDEVVEYFAK-SDISKYAELFKQHEIDGGALLLLNRETIMSC  705

Query  937  MNIKLGPALKICAKINVLK  955
            M  KLGPALK+   I  LK
Sbjct  706  MQFKLGPALKLLNHIAELK  724


> XP_028398470.1 putative GPI-anchored protein pfl2 [Dendronephthya 
gigantea]
Length=426

 Score = 63.2 bits (152),  Expect = 4e-10, Method: Compositional matrix adjust.
 Identities = 28/64 (44%), Positives = 43/64 (67%), Gaps = 2/64 (3%)

Query  891  SRWSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKI-CA  949
            ++W+ E+V +F+ +   C E A  F  QEIDG+AL LL ++ L+  M +KLGPA+K+ C 
Sbjct  357  TKWTAEDVAKFVRA-TDCSEYAHVFVDQEIDGKALTLLNQDFLIQWMKLKLGPAMKLNCH  415

Query  950  KINV  953
             IN+
Sbjct  416  IINL  419


> XP_028402346.1 lethal(3)malignant brain tumor-like protein 1 
isoform X2 [Dendronephthya gigantea]
Length=887

 Score = 55.8 bits (133),  Expect = 1e-07, Method: Compositional matrix adjust.
 Identities = 27/59 (46%), Positives = 41/59 (69%), Gaps = 1/59 (2%)

Query  893  WSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKICAKI  951
            W+ E+V E I S+ GC   A+ F  Q+IDG+A LLL++  +++ + IKLGPA+KI + I
Sbjct  826  WTCEQVAEHIRSI-GCVNQAQVFIDQQIDGEAFLLLQQADIVNILKIKLGPAVKIYSSI  883


 Score = 38.5 bits (88),  Expect = 0.031, Method: Compositional matrix adjust.
 Identities = 14/28 (50%), Positives = 16/28 (57%), Gaps = 0/28 (0%)

Query  753  CEYCGKYAPAEQFRGSKRFCSMTCAKRY  780
            CE CGKY    +F  S RFC +TC   Y
Sbjct  154  CEICGKYGLRSEFGASGRFCGLTCVGVY  181


> XP_028402345.1 lethal(3)malignant brain tumor-like protein 1 
isoform X1 [Dendronephthya gigantea]
Length=888

 Score = 55.8 bits (133),  Expect = 1e-07, Method: Compositional matrix adjust.
 Identities = 27/59 (46%), Positives = 41/59 (69%), Gaps = 1/59 (2%)

Query  893  WSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKICAKI  951
            W+ E+V E I S+ GC   A+ F  Q+IDG+A LLL++  +++ + IKLGPA+KI + I
Sbjct  827  WTCEQVAEHIRSI-GCVNQAQVFIDQQIDGEAFLLLQQADIVNILKIKLGPAVKIYSSI  884


 Score = 38.5 bits (88),  Expect = 0.032, Method: Compositional matrix adjust.
 Identities = 14/28 (50%), Positives = 16/28 (57%), Gaps = 0/28 (0%)

Query  753  CEYCGKYAPAEQFRGSKRFCSMTCAKRY  780
            CE CGKY    +F  S RFC +TC   Y
Sbjct  154  CEICGKYGLRSEFGASGRFCGLTCVGVY  181


> XP_028402344.1 lethal(3)malignant brain tumor-like protein 1 
isoform X1 [Dendronephthya gigantea]
Length=888

 Score = 55.8 bits (133),  Expect = 1e-07, Method: Compositional matrix adjust.
 Identities = 27/59 (46%), Positives = 41/59 (69%), Gaps = 1/59 (2%)

Query  893  WSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKICAKI  951
            W+ E+V E I S+ GC   A+ F  Q+IDG+A LLL++  +++ + IKLGPA+KI + I
Sbjct  827  WTCEQVAEHIRSI-GCVNQAQVFIDQQIDGEAFLLLQQADIVNILKIKLGPAVKIYSSI  884


 Score = 38.5 bits (88),  Expect = 0.032, Method: Compositional matrix adjust.
 Identities = 14/28 (50%), Positives = 16/28 (57%), Gaps = 0/28 (0%)

Query  753  CEYCGKYAPAEQFRGSKRFCSMTCAKRY  780
            CE CGKY    +F  S RFC +TC   Y
Sbjct  154  CEICGKYGLRSEFGASGRFCGLTCVGVY  181


> XP_028395289.1 uncharacterized protein LOC114519364 [Dendronephthya 
gigantea]
Length=1829

 Score = 54.3 bits (129),  Expect = 5e-07, Method: Compositional matrix adjust.
 Identities = 31/72 (43%), Positives = 44/72 (61%), Gaps = 1/72 (1%)

Query  884   VFLSSNPSRWSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGP  943
             ++   NP +W+V++V E+I S   C+  A  F  QEIDG++LLLL    LM   N+KLGP
Sbjct  1750  LYHPDNPRKWNVDQVGEYIMSTD-CKNYATYFMDQEIDGESLLLLTRSTLMQFTNMKLGP  1808

Query  944   ALKICAKINVLK  955
              LK+   I  L+
Sbjct  1809  TLKLSNYIAQLR  1820


> XP_028404723.1 histone acetyltransferase KAT6A-like [Dendronephthya 
gigantea]
Length=481

 Score = 53.1 bits (126),  Expect = 8e-07, Method: Compositional matrix adjust.
 Identities = 27/59 (46%), Positives = 39/59 (66%), Gaps = 1/59 (2%)

Query  893  WSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKICAKI  951
            W+  +V+E+I SL G  + A  F  QEIDG +L+L+    L++++  KLGPALKI A I
Sbjct  411  WNKTDVFEYIKSL-GFPKEAAVFEEQEIDGPSLMLMSRTDLVTSLQFKLGPALKIYANI  468


> XP_028400132.1 MBT domain-containing protein 1-like isoform X3 
[Dendronephthya gigantea]
Length=651

 Score = 44.7 bits (104),  Expect = 4e-04, Method: Compositional matrix adjust.
 Identities = 24/72 (33%), Positives = 38/72 (53%), Gaps = 7/72 (10%)

Query  753  CEYCGKYAPAEQFRG-SKRFCSMTCAKRYNVSCSHQFRLKRKKMKEFQEANYARVRRRGP  811
            CE+CG +   EQF   SKRFC + CAK+Y+ S      +K++K           VR+  P
Sbjct  133  CEFCGFFGVKEQFFSKSKRFCKVECAKKYSAS-----NIKKRKTDRITPTKKRTVRKPTP  187

Query  812  -RRSSSDIARAK  822
             ++  +D++  K
Sbjct  188  SKKIKTDVSLIK  199


> XP_028400130.1 MBT domain-containing protein 1-like isoform X1 
[Dendronephthya gigantea]
Length=870

 Score = 44.7 bits (104),  Expect = 5e-04, Method: Compositional matrix adjust.
 Identities = 24/72 (33%), Positives = 38/72 (53%), Gaps = 7/72 (10%)

Query  753  CEYCGKYAPAEQFRG-SKRFCSMTCAKRYNVSCSHQFRLKRKKMKEFQEANYARVRRRGP  811
            CE+CG +   EQF   SKRFC + CAK+Y+ S      +K++K           VR+  P
Sbjct  133  CEFCGFFGVKEQFFSKSKRFCKVECAKKYSAS-----NIKKRKTDRITPTKKRTVRKPTP  187

Query  812  -RRSSSDIARAK  822
             ++  +D++  K
Sbjct  188  SKKIKTDVSLIK  199


> XP_028400131.1 MBT domain-containing protein 1-like isoform X2 
[Dendronephthya gigantea]
Length=844

 Score = 44.3 bits (103),  Expect = 6e-04, Method: Compositional matrix adjust.
 Identities = 24/72 (33%), Positives = 38/72 (53%), Gaps = 7/72 (10%)

Query  753  CEYCGKYAPAEQFRG-SKRFCSMTCAKRYNVSCSHQFRLKRKKMKEFQEANYARVRRRGP  811
            CE+CG +   EQF   SKRFC + CAK+Y+ S      +K++K           VR+  P
Sbjct  107  CEFCGFFGVKEQFFSKSKRFCKVECAKKYSAS-----NIKKRKTDRITPTKKRTVRKPTP  161

Query  812  -RRSSSDIARAK  822
             ++  +D++  K
Sbjct  162  SKKIKTDVSLIK  173


> XP_028410478.1 uncharacterized protein LOC114533086 isoform X2 
[Dendronephthya gigantea]
Length=17103

 Score = 42.0 bits (97),  Expect = 0.003, Method: Composition-based stats.
 Identities = 36/142 (25%), Positives = 58/142 (41%), Gaps = 14/142 (10%)

Query  403    PPQVPPTQQVPPSQSQQQAQTLVVQPMLQSSPLSLP--PDAAPKPPIPIQSKP---PVAP  457
              PP VP T   PP+   ++  +   +P + ++P ++P  P A P  P  + + P   P  P
Sbjct  12231  PPGVPATPSGPPTTPVKKPVSPGGKPAIPATPGAVPATPGATPASPGSVPTTPGGRPATP  12290

Query  458    -IKPPQLGAAKMSAAQQPP-----PHIPVQVVGT---RQPGTAQAQALGLAQLAAAVPTS  508
                +P   G    +   +P      P +P  V GT     PGT      G   +   VPT+
Sbjct  12291  GGRPATPGGRPATPGGRPATPGVKPAVPGTVPGTVPGTVPGTVPVTPGGKPAVPGTVPTT  12350

Query  509    RGMPGTVQSGQAHLASSPPSSQ  530
               G+   V +      + P  S+
Sbjct  12351  PGVKPAVPAKPVGPGTEPKPSK  12372


 Score = 35.8 bits (81),  Expect = 0.24, Method: Composition-based stats.
 Identities = 38/153 (25%), Positives = 47/153 (31%), Gaps = 26/153 (17%)

Query  439    PDAAPKPPIPIQSKPPVAPIKPPQLGAAKMSAAQQPPPHIPVQVVGTRQPGTAQAQALGL  498
              P   P  P    S PP  P+K P     K        P IP        PG   A     
Sbjct  12231  PPGVPATP----SGPPTTPVKKPVSPGGK--------PAIPAT------PGAVPATPGAT  12272

Query  499    AQLAAAVPTSRG----MPGTVQSGQAHLASSPPSSQAPGALQECPPTLAPGMTLAPVQGT  554
                   +VPT+ G     PG   +      ++P    A   ++   P   PG     V GT
Sbjct  12273  PASPGSVPTTPGGRPATPGGRPATPGGRPATPGGRPATPGVKPAVPGTVPGTVPGTVPGT  12332

Query  555    AHVVKGGATTSSPVVAQVPAAFYMQSVHLPGKP  587
                V  GG       V   P         +P KP
Sbjct  12333  VPVTPGGKPAVPGTVPTTPGV----KPAVPAKP  12361


> XP_028410472.1 uncharacterized protein LOC114533086 isoform X1 
[Dendronephthya gigantea]
Length=17260

 Score = 42.0 bits (97),  Expect = 0.003, Method: Composition-based stats.
 Identities = 36/142 (25%), Positives = 58/142 (41%), Gaps = 14/142 (10%)

Query  403    PPQVPPTQQVPPSQSQQQAQTLVVQPMLQSSPLSLP--PDAAPKPPIPIQSKP---PVAP  457
              PP VP T   PP+   ++  +   +P + ++P ++P  P A P  P  + + P   P  P
Sbjct  12388  PPGVPATPSGPPTTPVKKPVSPGGKPAIPATPGAVPATPGATPASPGSVPTTPGGRPATP  12447

Query  458    -IKPPQLGAAKMSAAQQPP-----PHIPVQVVGT---RQPGTAQAQALGLAQLAAAVPTS  508
                +P   G    +   +P      P +P  V GT     PGT      G   +   VPT+
Sbjct  12448  GGRPATPGGRPATPGGRPATPGVKPAVPGTVPGTVPGTVPGTVPVTPGGKPAVPGTVPTT  12507

Query  509    RGMPGTVQSGQAHLASSPPSSQ  530
               G+   V +      + P  S+
Sbjct  12508  PGVKPAVPAKPVGPGTEPKPSK  12529


 Score = 35.8 bits (81),  Expect = 0.24, Method: Composition-based stats.
 Identities = 38/153 (25%), Positives = 47/153 (31%), Gaps = 26/153 (17%)

Query  439    PDAAPKPPIPIQSKPPVAPIKPPQLGAAKMSAAQQPPPHIPVQVVGTRQPGTAQAQALGL  498
              P   P  P    S PP  P+K P     K        P IP        PG   A     
Sbjct  12388  PPGVPATP----SGPPTTPVKKPVSPGGK--------PAIPAT------PGAVPATPGAT  12429

Query  499    AQLAAAVPTSRG----MPGTVQSGQAHLASSPPSSQAPGALQECPPTLAPGMTLAPVQGT  554
                   +VPT+ G     PG   +      ++P    A   ++   P   PG     V GT
Sbjct  12430  PASPGSVPTTPGGRPATPGGRPATPGGRPATPGGRPATPGVKPAVPGTVPGTVPGTVPGT  12489

Query  555    AHVVKGGATTSSPVVAQVPAAFYMQSVHLPGKP  587
                V  GG       V   P         +P KP
Sbjct  12490  VPVTPGGKPAVPGTVPTTPGV----KPAVPAKP  12518


> XP_028410482.1 uncharacterized protein LOC114533086 isoform X3 
[Dendronephthya gigantea]
Length=17056

 Score = 42.0 bits (97),  Expect = 0.003, Method: Composition-based stats.
 Identities = 36/142 (25%), Positives = 58/142 (41%), Gaps = 14/142 (10%)

Query  403    PPQVPPTQQVPPSQSQQQAQTLVVQPMLQSSPLSLP--PDAAPKPPIPIQSKP---PVAP  457
              PP VP T   PP+   ++  +   +P + ++P ++P  P A P  P  + + P   P  P
Sbjct  12388  PPGVPATPSGPPTTPVKKPVSPGGKPAIPATPGAVPATPGATPASPGSVPTTPGGRPATP  12447

Query  458    -IKPPQLGAAKMSAAQQPP-----PHIPVQVVGT---RQPGTAQAQALGLAQLAAAVPTS  508
                +P   G    +   +P      P +P  V GT     PGT      G   +   VPT+
Sbjct  12448  GGRPATPGGRPATPGGRPATPGVKPAVPGTVPGTVPGTVPGTVPVTPGGKPAVPGTVPTT  12507

Query  509    RGMPGTVQSGQAHLASSPPSSQ  530
               G+   V +      + P  S+
Sbjct  12508  PGVKPAVPAKPVGPGTEPKPSK  12529


 Score = 35.8 bits (81),  Expect = 0.24, Method: Composition-based stats.
 Identities = 38/153 (25%), Positives = 47/153 (31%), Gaps = 26/153 (17%)

Query  439    PDAAPKPPIPIQSKPPVAPIKPPQLGAAKMSAAQQPPPHIPVQVVGTRQPGTAQAQALGL  498
              P   P  P    S PP  P+K P     K        P IP        PG   A     
Sbjct  12388  PPGVPATP----SGPPTTPVKKPVSPGGK--------PAIPAT------PGAVPATPGAT  12429

Query  499    AQLAAAVPTSRG----MPGTVQSGQAHLASSPPSSQAPGALQECPPTLAPGMTLAPVQGT  554
                   +VPT+ G     PG   +      ++P    A   ++   P   PG     V GT
Sbjct  12430  PASPGSVPTTPGGRPATPGGRPATPGGRPATPGGRPATPGVKPAVPGTVPGTVPGTVPGT  12489

Query  555    AHVVKGGATTSSPVVAQVPAAFYMQSVHLPGKP  587
                V  GG       V   P         +P KP
Sbjct  12490  VPVTPGGKPAVPGTVPTTPGV----KPAVPAKP  12518


> XP_028392389.1 uncharacterized protein LOC114516961 [Dendronephthya 
gigantea]
Length=321

 Score = 40.8 bits (94),  Expect = 0.004, Method: Compositional matrix adjust.
 Identities = 22/59 (37%), Positives = 38/59 (64%), Gaps = 2/59 (3%)

Query  893  WSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKICAKI  951
            +S EEV E++  L G +  +++F  +++DG+AL LL E  L S ++ K+GP +K+   I
Sbjct  9    YSCEEVSEWL-KLNGFETFSDKFIEEDVDGEALALLSENALESLVD-KVGPRMKLLGVI  65


> XP_028413638.1 uncharacterized protein LOC114536487 [Dendronephthya 
gigantea]
Length=401

 Score = 40.4 bits (93),  Expect = 0.006, Method: Compositional matrix adjust.
 Identities = 22/59 (37%), Positives = 38/59 (64%), Gaps = 2/59 (3%)

Query  893  WSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKICAKI  951
            +S EEV E++  L G +  +++F  +++DG+AL LL E  L S ++ K+GP +K+   I
Sbjct  9    YSCEEVSEWL-KLNGFETFSDKFIEEDVDGEALALLSENALESLVD-KVGPRMKLLGVI  65


> XP_028397165.1 SH3 and multiple ankyrin repeat domains protein 
3-like [Dendronephthya gigantea]
Length=1849

 Score = 40.8 bits (94),  Expect = 0.006, Method: Compositional matrix adjust.
 Identities = 24/70 (34%), Positives = 35/70 (50%), Gaps = 1/70 (1%)

Query  885   FLSSNPSRWSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPA  944
             FL +    WS   V +++AS+ G  E  + FR  +I G  LL L +E L      KLG  
Sbjct  1771  FLETPYENWSCAHVCDWLASI-GMAEHGDLFRENDIRGSNLLGLTKEDLRELGVKKLGHR  1829

Query  945   LKICAKINVL  954
             + I  ++N L
Sbjct  1830  MTITNEVNRL  1839


> XP_028414671.1 uncharacterized protein LOC114537777 [Dendronephthya 
gigantea]
Length=475

 Score = 40.0 bits (92),  Expect = 0.008, Method: Compositional matrix adjust.
 Identities = 22/59 (37%), Positives = 38/59 (64%), Gaps = 2/59 (3%)

Query  893  WSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKICAKI  951
            +S EEV E++  L G +  +++F  +++DG+AL LL E  L S ++ K+GP +K+   I
Sbjct  9    YSCEEVSEWL-KLNGFETFSDKFIEEDVDGEALALLSENALESLVD-KVGPRMKLLGVI  65


> XP_028397135.1 uncharacterized protein LOC114520968 [Dendronephthya 
gigantea]
Length=445

 Score = 40.0 bits (92),  Expect = 0.008, Method: Compositional matrix adjust.
 Identities = 21/55 (38%), Positives = 37/55 (67%), Gaps = 2/55 (4%)

Query  893  WSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKI  947
            +S EEV E++  L G +  +++F  +++DG+AL LL E  L S ++ K+GP +K+
Sbjct  9    YSCEEVSEWL-KLNGFETFSDKFIEEDVDGEALALLSENALESLVD-KVGPRMKL  61


> XP_028399973.1 uncharacterized protein LOC114523295 [Dendronephthya 
gigantea]
Length=476

 Score = 40.0 bits (92),  Expect = 0.009, Method: Compositional matrix adjust.
 Identities = 22/59 (37%), Positives = 38/59 (64%), Gaps = 2/59 (3%)

Query  893  WSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKICAKI  951
            +S EEV E++  L G +  +++F  +++DG+AL LL E  L S ++ K+GP +K+   I
Sbjct  9    YSCEEVSEWL-KLNGFETFSDKFIEEDVDGEALALLSENALESLVD-KVGPRMKLLGVI  65


> XP_028418060.1 uncharacterized protein LOC114542872 [Dendronephthya 
gigantea]
Length=515

 Score = 40.0 bits (92),  Expect = 0.010, Method: Compositional matrix adjust.
 Identities = 22/59 (37%), Positives = 38/59 (64%), Gaps = 2/59 (3%)

Query  893  WSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKICAKI  951
            +S EEV E++  L G +  +++F  +++DG+AL LL E  L S ++ K+GP +K+   I
Sbjct  9    YSCEEVSEWL-KLNGFETFSDKFIEEDVDGEALALLSENALESLVD-KVGPRMKLLGVI  65


> XP_028417239.1 uncharacterized protein LOC114541522 [Dendronephthya 
gigantea]
Length=515

 Score = 40.0 bits (92),  Expect = 0.010, Method: Compositional matrix adjust.
 Identities = 22/59 (37%), Positives = 38/59 (64%), Gaps = 2/59 (3%)

Query  893  WSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKICAKI  951
            +S EEV E++  L G +  +++F  +++DG+AL LL E  L S ++ K+GP +K+   I
Sbjct  9    YSCEEVSEWL-KLNGFETFSDKFIEEDVDGEALALLSENALESLVD-KVGPRMKLLGVI  65


> XP_028408983.1 LOW QUALITY PROTEIN: muscle M-line assembly protein 
unc-89-like [Dendronephthya gigantea]
Length=8367

 Score = 37.4 bits (85),  Expect = 0.085, Method: Composition-based stats.
 Identities = 74/326 (23%), Positives = 123/326 (38%), Gaps = 44/326 (13%)

Query  374   THLQLAQQQQQQQQQQQQQPQATTLTAPQPPQVPPTQQVPPSQSQQQAQTLVVQPMLQSS  433
             T L  A  ++ +   + ++P     T+P+  Q  P + V     ++ + T+    +L +S
Sbjct  3579  TPLPSAGDKKDKVPHENKKPHVAEKTSPRKEQDKPMEPV----GKEISFTIDSGSILMAS  3634

Query  434   PLSLPPDA------APKPPIPIQSKP------PVAPI-KPPQLGAAKMSAAQQPPPHIPV  480
             P SLP +         KP +  + KP      P  P+ K         +     P H+  
Sbjct  3635  PESLPEEKYRDKPKGRKPEVAEKKKPGKTQARPFEPVGKEINFTIESEAGLMASPDHLLD  3694

Query  481   QVVG----TRQPGTAQAQALGL-------AQLAAAVPTSRGMPGT--VQSGQAHLASSPP  527
             Q +      RQP  A+ + +G         ++ A + T    P    + S Q        
Sbjct  3695  QQISQQPNARQPEVAEKKKVGKDESKPFEKEIVANIDTESFSPAAFEISSEQEKKKPKAQ  3754

Query  528   SSQAPGALQECPP--TLAPGMTLAPVQGTAHVVKGGATTSSPVVAQVPAAFYMQS-VHLP  584
               +  G  +E  P  T+   ++L    GT  V     TT    ++  P A    S +  P
Sbjct  3755  KPKVAGKGEESRPFETVGKPVSLTIDDGTLVV-----TTPEEGISLKPEAKSAPSTMKKP  3809

Query  585   GKPQTLAVKRKADSEEERDDVSTLGSMLPAKASPVAESPKVMDEKSSLGEKAESVANVNA  644
              K +     R  DS+++R     L S   AK SP   +PK M +K+    + +  A    
Sbjct  3810  AKVKPQDKDRPVDSKKDRRQSIKLPSQKQAKDSPPKVAPKTMKDKTQRPVEKDQTAKPTK  3869

Query  645   NTPSSE---LVALTP---APSVPPPT  664
             +  S++    V   P   AP V P T
Sbjct  3870  DQQSADGIRPVGKLPKDEAPKVAPKT  3895


> XP_028391350.1 phospholipase DDHD2-like [Dendronephthya gigantea]
Length=912

 Score = 33.5 bits (75),  Expect = 1.0, Method: Compositional matrix adjust.
 Identities = 16/43 (37%), Positives = 26/43 (60%), Gaps = 1/43 (2%)

Query  907  GCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKICA  949
            G +     F+ ++IDG+AL+L  EE L   ++I +GP  KI +
Sbjct  579  GLENFILTFQMEQIDGEALMLCSEEDL-KELSIPMGPRKKIIS  620


> XP_028393858.1 uncharacterized protein LOC114518127 isoform X3 
[Dendronephthya gigantea]
Length=1012

 Score = 33.1 bits (74),  Expect = 1.5, Method: Compositional matrix adjust.
 Identities = 32/134 (24%), Positives = 48/134 (36%), Gaps = 22/134 (16%)

Query  269  PLPAAQTVTVSQGSQTEAESAAAKKAEADGSGQQNVGMNLTRTATPAPSQTLISSATYTQ  328
            P+P   T TV   SQTEA    ++  + D    ++VG   TR A             Y+ 
Sbjct  122  PMPEDFTTTVDDESQTEARDVCSETTQTDEKVDKDVGT--TRDA-------------YSM  166

Query  329  IQPHSLIQQQQQIHLQQKQVVIQQQIAIHHQQQFQH-------RQSQLLHTATHLQLAQQ  381
              P SL   + Q  + Q     Q        Q ++H       R+S L+ T+   Q    
Sbjct  167  TAPRSLSDSEVQTQIVQGSASTQTDYEDDDSQWYRHTSTVEEVRESSLIVTSERRQCCDS  226

Query  382  QQQQQQQQQQQPQA  395
            + Q   +     Q 
Sbjct  227  ETQTDDKVDDDEQV  240


> XP_028393856.1 uncharacterized protein LOC114518127 isoform X1 
[Dendronephthya gigantea]
Length=1028

 Score = 33.1 bits (74),  Expect = 1.5, Method: Compositional matrix adjust.
 Identities = 32/134 (24%), Positives = 48/134 (36%), Gaps = 22/134 (16%)

Query  269  PLPAAQTVTVSQGSQTEAESAAAKKAEADGSGQQNVGMNLTRTATPAPSQTLISSATYTQ  328
            P+P   T TV   SQTEA    ++  + D    ++VG   TR A             Y+ 
Sbjct  122  PMPEDFTTTVDDESQTEARDVCSETTQTDEKVDKDVGT--TRDA-------------YSM  166

Query  329  IQPHSLIQQQQQIHLQQKQVVIQQQIAIHHQQQFQH-------RQSQLLHTATHLQLAQQ  381
              P SL   + Q  + Q     Q        Q ++H       R+S L+ T+   Q    
Sbjct  167  TAPRSLSDSEVQTQIVQGSASTQTDYEDDDSQWYRHTSTVEEVRESSLIVTSERRQCCDS  226

Query  382  QQQQQQQQQQQPQA  395
            + Q   +     Q 
Sbjct  227  ETQTDDKVDDDEQV  240


> XP_028393857.1 uncharacterized protein LOC114518127 isoform X2 
[Dendronephthya gigantea]
Length=1027

 Score = 32.7 bits (73),  Expect = 1.7, Method: Compositional matrix adjust.
 Identities = 32/134 (24%), Positives = 48/134 (36%), Gaps = 22/134 (16%)

Query  269  PLPAAQTVTVSQGSQTEAESAAAKKAEADGSGQQNVGMNLTRTATPAPSQTLISSATYTQ  328
            P+P   T TV   SQTEA    ++  + D    ++VG   TR A             Y+ 
Sbjct  122  PMPEDFTTTVDDESQTEARDVCSETTQTDEKVDKDVGT--TRDA-------------YSM  166

Query  329  IQPHSLIQQQQQIHLQQKQVVIQQQIAIHHQQQFQH-------RQSQLLHTATHLQLAQQ  381
              P SL   + Q  + Q     Q        Q ++H       R+S L+ T+   Q    
Sbjct  167  TAPRSLSDSEVQTQIVQGSASTQTDYEDDDSQWYRHTSTVEEVRESSLIVTSERRQCCDS  226

Query  382  QQQQQQQQQQQPQA  395
            + Q   +     Q 
Sbjct  227  ETQTDDKVDDDEQV  240


> XP_028413398.1 microtubule-associated protein futsch-like [Dendronephthya 
gigantea]
Length=1248

 Score = 32.7 bits (73),  Expect = 1.9, Method: Compositional matrix adjust.
 Identities = 21/58 (36%), Positives = 31/58 (53%), Gaps = 8/58 (14%)

Query  887  SSNPSRWSVEEVYEFIASL----QGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIK  940
            S N   W+   V E+I  L    + C   AE F  +EI+G+ALL +  E+L   ++IK
Sbjct  3    SLNIENWTPSNVAEWIKGLGPQFESC---AESFVDEEINGKALLTINTEYL-GELDIK  56


> XP_028406223.1 suppression of tumorigenicity 18 protein-like 
isoform X2 [Dendronephthya gigantea]
Length=745

 Score = 31.6 bits (70),  Expect = 4.0, Method: Compositional matrix adjust.
 Identities = 15/43 (35%), Positives = 24/43 (56%), Gaps = 4/43 (9%)

Query  738  DSPSAELDKKANLLK--CEYCGKYAPAEQFR--GSKRFCSMTC  776
            DSP ++ D +   ++  CE CG+    EQF+  G + +CS  C
Sbjct  45   DSPGSDSDAELATVENNCEICGEIVDTEQFQLPGKRYYCSYEC  87


> XP_028406222.1 myelin transcription factor 1-like isoform X1 
[Dendronephthya gigantea]
Length=746

 Score = 31.6 bits (70),  Expect = 4.0, Method: Compositional matrix adjust.
 Identities = 15/43 (35%), Positives = 24/43 (56%), Gaps = 4/43 (9%)

Query  738  DSPSAELDKKANLLK--CEYCGKYAPAEQFR--GSKRFCSMTC  776
            DSP ++ D +   ++  CE CG+    EQF+  G + +CS  C
Sbjct  45   DSPGSDSDAELATVENNCEICGEIVDTEQFQLPGKRYYCSYEC  87


> XP_028410284.1 THAP domain-containing protein 1-like [Dendronephthya 
gigantea]
Length=286

 Score = 30.4 bits (67),  Expect = 7.1, Method: Composition-based stats.
 Identities = 16/58 (28%), Positives = 34/58 (59%), Gaps = 1/58 (2%)

Query  159  QVQNLAVRNQQASAQGPQMQGSTQKAIPPGASPVSSL-SQASSQALAVAQASSGATNQ  215
            QVQ LA+R ++++   P    +    + P  +P++ L + AS+++   A+ SS +T++
Sbjct  168  QVQTLAIRKKKSTPMTPAQAKNNSGIVTPPTTPITILEATASTESEQDAEYSSSSTDE  225



Lambda      K        H        a         alpha
   0.308    0.122    0.340    0.792     4.96 

Gapped
Lambda      K        H        a         alpha    sigma
   0.267   0.0410    0.140     1.90     42.6     43.6 

Effective search space used: 12251592470


  Database: Cnidaria/GCF_004324835.1_DenGig_1.0_protein.faa
    Posted date:  Jun 24, 2020  11:20 AM
  Number of letters in database: 17,717,918
  Number of sequences in database:  28,741



Matrix: BLOSUM62
Gap Penalties: Existence: 11, Extension: 1
Neighboring words threshold: 11
Window for multiple hits: 40
