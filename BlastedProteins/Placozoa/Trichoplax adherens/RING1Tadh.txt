BLASTP 2.6.0+


Reference: Stephen F. Altschul, Thomas L. Madden, Alejandro A.
Schaffer, Jinghui Zhang, Zheng Zhang, Webb Miller, and David J.
Lipman (1997), "Gapped BLAST and PSI-BLAST: a new generation of
protein database search programs", Nucleic Acids Res. 25:3389-3402.


Reference for composition-based statistics: Alejandro A. Schaffer,
L. Aravind, Thomas L. Madden, Sergei Shavirin, John L. Spouge, Yuri
I. Wolf, Eugene V. Koonin, and Stephen F. Altschul (2001),
"Improving the accuracy of PSI-BLAST protein database searches with
composition-based statistics and other refinements", Nucleic Acids
Res. 29:2994-3005.



Database: Placozoa/GCF_000150275.1_v1.0_protein.faa
           11,520 sequences; 5,220,296 total letters



Query= sp|Q06587|RING1_HUMAN E3 ubiquitin-protein ligase RING1 OS=Homo
sapiens OX=9606 GN=RING1 PE=1 SV=2

Length=406
                                                                      Score     E
Sequences producing significant alignments:                          (Bits)  Value

  XP_002108615.1 hypothetical protein TRIADDRAFT_19102, partial [...  236     5e-78
  XP_002117642.1 hypothetical protein TRIADDRAFT_7851, partial [T...  45.1    2e-06
  XP_002114733.1 hypothetical protein TRIADDRAFT_58523 [Trichopla...  44.7    1e-05
  XP_002114614.1 hypothetical protein TRIADDRAFT_28240 [Trichopla...  43.5    3e-05
  XP_002109891.1 hypothetical protein TRIADDRAFT_21397 [Trichopla...  42.0    2e-04
  XP_002111243.1 hypothetical protein TRIADDRAFT_54995 [Trichopla...  39.3    6e-04
  XP_002117834.1 predicted protein [Trichoplax adhaerens]             38.9    0.002
  XP_002114795.1 hypothetical protein TRIADDRAFT_28310, partial [...  38.1    0.002
  XP_002113932.1 hypothetical protein TRIADDRAFT_57864 [Trichopla...  37.0    0.008
  XP_002110348.1 hypothetical protein TRIADDRAFT_54253 [Trichopla...  36.2    0.010
  XP_002109608.1 hypothetical protein TRIADDRAFT_53804 [Trichopla...  35.8    0.023
  XP_002114236.1 hypothetical protein TRIADDRAFT_57833 [Trichopla...  34.7    0.032
  XP_002115187.1 hypothetical protein TRIADDRAFT_28618, partial [...  34.3    0.038
  XP_002113939.1 hypothetical protein TRIADDRAFT_57875 [Trichopla...  33.9    0.058
  XP_002108239.1 hypothetical protein TRIADDRAFT_52519 [Trichopla...  33.1    0.13 
  XP_002115875.1 hypothetical protein TRIADDRAFT_30014 [Trichopla...  30.4    0.19 
  XP_002109650.1 hypothetical protein TRIADDRAFT_20747, partial [...  31.6    0.37 
  XP_002116960.1 hypothetical protein TRIADDRAFT_61032 [Trichopla...  29.6    0.92 
  XP_002113584.1 hypothetical protein TRIADDRAFT_57151 [Trichopla...  29.3    1.4  
  XP_002117466.1 hypothetical protein TRIADDRAFT_61456 [Trichopla...  29.6    1.5  
  XP_002107666.1 hypothetical protein TRIADDRAFT_63462 [Trichopla...  29.3    1.5  
  XP_002113156.1 hypothetical protein TRIADDRAFT_57035 [Trichopla...  29.3    2.1  
  XP_002112826.1 hypothetical protein TRIADDRAFT_56365 [Trichopla...  28.1    5.1  
  XP_002113634.1 hypothetical protein TRIADDRAFT_27129 [Trichopla...  27.7    5.8  
  XP_002116583.1 hypothetical protein TRIADDRAFT_30995, partial [...  27.3    6.1  
  XP_002107658.1 hypothetical protein TRIADDRAFT_19665, partial [...  27.3    8.6  
  XP_002115433.1 hypothetical protein TRIADDRAFT_59362 [Trichopla...  27.3    9.2  


> XP_002108615.1 hypothetical protein TRIADDRAFT_19102, partial 
[Trichoplax adhaerens]
Length=152

 Score = 236 bits (603),  Expect = 5e-78, Method: Compositional matrix adjust.
 Identities = 108/146 (74%), Positives = 127/146 (87%), Gaps = 0/146 (0%)

Query  15   ELSLYELHRTPQEAIMDGTEIAVSPRSLHSELMCPICLDMLKNTMTTKECLHRFCSDCIV  74
            ELS+YELHR PQ AI D +E+ +S RSLHSELMCPICLD+LK+TMTTKECLHRFC++CI 
Sbjct  1    ELSVYELHRQPQAAIDDDSEVTISLRSLHSELMCPICLDILKSTMTTKECLHRFCAECIT  60

Query  75   TALRSGNKECPTCRKKLVSKRSLRPDPNFDALISKIYPSREEYEAHQDRVLIRLSRLHNQ  134
            TALRSGNKECPTCRKKLVS+RSLRPDPNFDALISKIYP+REEYEA Q++VL RL++ H+Q
Sbjct  61   TALRSGNKECPTCRKKLVSRRSLRPDPNFDALISKIYPNREEYEAQQEKVLARLNKHHSQ  120

Query  135  QALSSSIEEGLRMQAMHRAQRVRRPI  160
             AL SS EEG+RMQA +R+   R  +
Sbjct  121  HALQSSFEEGVRMQATNRSHSQRSSV  146


> XP_002117642.1 hypothetical protein TRIADDRAFT_7851, partial 
[Trichoplax adhaerens]
Length=98

 Score = 45.1 bits (105),  Expect = 2e-06, Method: Composition-based stats.
 Identities = 31/95 (33%), Positives = 47/95 (49%), Gaps = 6/95 (6%)

Query  21   LHRTPQEAIMD---GTEIAVSPRSLHSELMCPICLDMLKNTMTTKECLHRFCSDCIVTAL  77
            L R  Q   M+   G E++    +L S+  C IC   LK+ + T+ C HRFC +CI   +
Sbjct  3    LQRNMQNLRMELPKGYEVSFVGDNL-SKYECAICCCALKDPVQTR-CGHRFCKECIEVYI  60

Query  78   RSGNKECP-TCRKKLVSKRSLRPDPNFDALISKIY  111
            R G K CP  C+   +++ S+      + L   IY
Sbjct  61   RRGGKHCPNNCQPIAINELSVDNAAKREVLNLDIY  95


> XP_002114733.1 hypothetical protein TRIADDRAFT_58523 [Trichoplax 
adhaerens]
Length=177

 Score = 44.7 bits (104),  Expect = 1e-05, Method: Compositional matrix adjust.
 Identities = 24/54 (44%), Positives = 33/54 (61%), Gaps = 6/54 (11%)

Query  46   LMCPICLD---MLKNTMTTKE--CLHRFCSDCIVTALRSGNKECPTCRKKLVSK  94
            + CPIC +    +K T T +   C H FCS C+  AL+  N ECP CRKK+++K
Sbjct  118  IECPICKESYSQIKKTRTLQSTVCGHIFCSSCLKIALKRKN-ECPICRKKVLAK  170


> XP_002114614.1 hypothetical protein TRIADDRAFT_28240 [Trichoplax 
adhaerens]
Length=232

 Score = 43.5 bits (101),  Expect = 3e-05, Method: Compositional matrix adjust.
 Identities = 28/81 (35%), Positives = 35/81 (43%), Gaps = 4/81 (5%)

Query  46   LMCPICLDMLKNTMTTKECLHRFCSDCIVTALRSGNKECPTCRKKLVSKRSLR---PDPN  102
            L C +C   L N  T  ECLH FC  CIV  L   N  CP C   +     L+    D  
Sbjct  14   LTCSLCDGYLVNATTVVECLHTFCKSCIVKHLEDSN-NCPKCDNVVHQSHPLQYISYDRT  72

Query  103  FDALISKIYPSREEYEAHQDR  123
               L+ K+ P  +E E   +R
Sbjct  73   MQDLVYKLVPGLQERELETER  93


> XP_002109891.1 hypothetical protein TRIADDRAFT_21397 [Trichoplax 
adhaerens]
Length=659

 Score = 42.0 bits (97),  Expect = 2e-04, Method: Compositional matrix adjust.
 Identities = 27/88 (31%), Positives = 44/88 (50%), Gaps = 5/88 (6%)

Query  23   RTPQEAIMDGTEIAVSPRSLHSELMCPICLDMLKNTMTTKECLHRFCSDCIVTALRSGNK  82
            + P   I DG     + R+  ++ +CPIC  +++    TK C H FC +CI  +L   N 
Sbjct  25   KRPLTPIDDGISDTYNDRN--NDYICPICFGVIEEAYMTK-CGHSFCYECIRRSL-DENS  80

Query  83   ECPTCRKKLVSKRS-LRPDPNFDALISK  109
            +CP C  ++  K   + P+   + LI K
Sbjct  81   KCPKCNFQITDKVDPIFPNITLNELIIK  108


> XP_002111243.1 hypothetical protein TRIADDRAFT_54995 [Trichoplax 
adhaerens]
Length=169

 Score = 39.3 bits (90),  Expect = 6e-04, Method: Compositional matrix adjust.
 Identities = 22/83 (27%), Positives = 38/83 (46%), Gaps = 4/83 (5%)

Query  33   TEIAVSPRSLHSELMCPICLDMLKNTMTTKECLHRFCSDCIVTALRSGNKECPTCRKKLV  92
            +E  V  + ++  ++C +C   L +  T  ECLH FC  CIV      +  CP C  ++ 
Sbjct  3    SEAQVKVQQINPHVICYLCQGYLIDATTITECLHSFCRSCIVKHF-DKSLLCPLCNLRVH  61

Query  93   SKR---SLRPDPNFDALISKIYP  112
              R   ++R D     ++  + P
Sbjct  62   ETRPHLNIRSDTTLQEIVYGLVP  84


> XP_002117834.1 predicted protein [Trichoplax adhaerens]
Length=376

 Score = 38.9 bits (89),  Expect = 0.002, Method: Compositional matrix adjust.
 Identities = 20/46 (43%), Positives = 24/46 (52%), Gaps = 2/46 (4%)

Query  45   ELMCPICLDMLKNTMTTKECLHRFCSDCIVTALRSGNKECPTCRKK  90
            E  C IC  +L        C H FC +CI T L S +  CPTCRK+
Sbjct  225  EFTCSICQSLLVAAHLLP-CSHSFCKECIYTWL-SNHSTCPTCRKR  268


> XP_002114795.1 hypothetical protein TRIADDRAFT_28310, partial 
[Trichoplax adhaerens]
Length=214

 Score = 38.1 bits (87),  Expect = 0.002, Method: Compositional matrix adjust.
 Identities = 22/82 (27%), Positives = 40/82 (49%), Gaps = 4/82 (5%)

Query  40   RSLHSELMCPICLDMLKNTMTTKECLHRFCSDCIVTALRSGNKECPTCRKKLVSKR---S  96
            + ++  + C +C   L +  T  ECLH +C  C+V  ++  N+ CPTC   +   +   +
Sbjct  6    KDINEHITCGLCKGYLVDASTITECLHTYCKSCLVRYVQDSNR-CPTCDIVIHETQPLYN  64

Query  97   LRPDPNFDALISKIYPSREEYE  118
            +R D     ++ K  P  +E E
Sbjct  65   IRMDRTMQDIVDKFVPWLKEAE  86


> XP_002113932.1 hypothetical protein TRIADDRAFT_57864 [Trichoplax 
adhaerens]
Length=726

 Score = 37.0 bits (84),  Expect = 0.008, Method: Compositional matrix adjust.
 Identities = 13/28 (46%), Positives = 17/28 (61%), Gaps = 0/28 (0%)

Query  64  CLHRFCSDCIVTALRSGNKECPTCRKKL  91
           CLH FC +CI   +  G+  CP C KK+
Sbjct  25  CLHTFCKNCIEAFMAEGSAFCPVCNKKI  52


> XP_002110348.1 hypothetical protein TRIADDRAFT_54253 [Trichoplax 
adhaerens]
Length=283

 Score = 36.2 bits (82),  Expect = 0.010, Method: Compositional matrix adjust.
 Identities = 22/59 (37%), Positives = 28/59 (47%), Gaps = 5/59 (8%)

Query  36   AVSPRSLHSELMCPICLDMLKNTMTTKE--CLHRFCSDCIVTAL-RSGNKECPTCRKKL  91
             ++   L +   CPIC +  K   T ++  C H F S CIV  L R G   CP CR  L
Sbjct  181  VITSEILETNSECPICKEEFKVKDTARKLPCQHYFHSQCIVQWLQRHGT--CPVCRLNL  237


> XP_002109608.1 hypothetical protein TRIADDRAFT_53804 [Trichoplax 
adhaerens]
Length=1383

 Score = 35.8 bits (81),  Expect = 0.023, Method: Composition-based stats.
 Identities = 19/58 (33%), Positives = 25/58 (43%), Gaps = 8/58 (14%)

Query  41    SLHSELMCPICLDMLKNTMTTKECLHRFCSDCI--------VTALRSGNKECPTCRKK  90
             S  +E  CPIC+  L    T   C H +C DC            +R  + +CP CR K
Sbjct  1186  SHENEDSCPICVRKLGKEWTVLGCGHCYCYDCTDVMIKKCAQNDMRQQSVKCPLCRIK  1243


> XP_002114236.1 hypothetical protein TRIADDRAFT_57833 [Trichoplax 
adhaerens]
Length=257

 Score = 34.7 bits (78),  Expect = 0.032, Method: Compositional matrix adjust.
 Identities = 17/48 (35%), Positives = 28/48 (58%), Gaps = 5/48 (10%)

Query  48   CPICLDMLKNTMTTKECLHRFCSDCIVTALRSGN----KECPTCRKKL  91
            CPIC+   +  + T  C H FC++CI+T  + G+      CP CR+++
Sbjct  87   CPICILDPRAPVETN-CGHIFCAECIITYWKHGSWLGPMNCPICRQEI  133


> XP_002115187.1 hypothetical protein TRIADDRAFT_28618, partial 
[Trichoplax adhaerens]
Length=236

 Score = 34.3 bits (77),  Expect = 0.038, Method: Compositional matrix adjust.
 Identities = 16/45 (36%), Positives = 25/45 (56%), Gaps = 2/45 (4%)

Query  47   MCPICLDMLKNTMTTKECLHRFCSDCIVTALRSGNKECPTCRKKL  91
            +CPIC + +K  +   +C H FC DCI +      + CP CR ++
Sbjct  176  LCPICQEEIKEPIKL-DCKHIFCDDCI-SLWFDRERSCPLCRARI  218


> XP_002113939.1 hypothetical protein TRIADDRAFT_57875 [Trichoplax 
adhaerens]
Length=348

 Score = 33.9 bits (76),  Expect = 0.058, Method: Compositional matrix adjust.
 Identities = 19/64 (30%), Positives = 31/64 (48%), Gaps = 7/64 (11%)

Query  34   EIAVSPRSLHSELMCPICLDMLKNTMTTKECLHRFCSDCIVTALRSG------NKECPTC  87
            EIA + +++   ++CP+CLD+         C H FC  CI    ++       N  CP C
Sbjct  235  EIASTGQNIPENMICPVCLDIYYRPYRC-NCGHIFCDFCIRLLAKNKLDNSDPNVLCPLC  293

Query  88   RKKL  91
            R+ +
Sbjct  294  RQSI  297


> XP_002108239.1 hypothetical protein TRIADDRAFT_52519 [Trichoplax 
adhaerens]
Length=597

 Score = 33.1 bits (74),  Expect = 0.13, Method: Compositional matrix adjust.
 Identities = 26/102 (25%), Positives = 38/102 (37%), Gaps = 23/102 (23%)

Query  48   CPICLDML--KNTMTTKECLHRFCSDCIVTALRSGNKE-------CPTCRKKLVSK----  94
            C IC D       +    C H FC DC+   + S  KE       CP C+   +S     
Sbjct  280  CVICFDTFPANEVLNFSVCRHEFCKDCLSKFIESTVKENSILKLNCPVCQMPDLSMASGE  339

Query  95   ---------RSLRPDPNFDALISKIYPSR-EEYEAHQDRVLI  126
                     R+L  D      I ++Y  +  +Y  HQD + +
Sbjct  340  YFSVVDQMVRNLVKDGYLAKEIYELYDMKLRDYNLHQDPLFL  381


> XP_002115875.1 hypothetical protein TRIADDRAFT_30014 [Trichoplax 
adhaerens]
Length=86

 Score = 30.4 bits (67),  Expect = 0.19, Method: Composition-based stats.
 Identities = 18/46 (39%), Positives = 23/46 (50%), Gaps = 4/46 (9%)

Query  48  CPIC-LDMLKNTMTTKECLHRFCSDCIVTALRSGN---KECPTCRK  89
           CP C L     T+   EC H F   CI+  L+S N   + CP CR+
Sbjct  34  CPECKLPGDDCTIVWGECSHCFHVHCILKWLQSQNYQDQTCPMCRQ  79


> XP_002109650.1 hypothetical protein TRIADDRAFT_20747, partial 
[Trichoplax adhaerens]
Length=347

 Score = 31.6 bits (70),  Expect = 0.37, Method: Compositional matrix adjust.
 Identities = 21/56 (38%), Positives = 26/56 (46%), Gaps = 2/56 (4%)

Query  44   SELMCPICLDMLKNTM-TTKECLHRFCSDCIVTALR-SGNKECPTCRKKLVSKRSL  97
            S L CPIC +   N       C H F   C+ +  + SG K CP CRK    KR +
Sbjct  105  SSLPCPICQEHFGNQQQILLSCSHVFHRTCLESYEKFSGRKTCPMCRKVEYQKRVI  160


> XP_002116960.1 hypothetical protein TRIADDRAFT_61032 [Trichoplax 
adhaerens]
Length=197

 Score = 29.6 bits (65),  Expect = 0.92, Method: Compositional matrix adjust.
 Identities = 15/55 (27%), Positives = 24/55 (44%), Gaps = 3/55 (5%)

Query  47  MCPICLDMLK--NTMTTKECLHRFCSDCIVTALRSGNKECPTCRKKLVSKRSLRP  99
           +CP+CLD     + +    C H F   C+ + L    + CP C+   +    L P
Sbjct  27  LCPVCLDEFVAGDVLRILPCKHEFHKTCVDSWLE-NKQTCPLCKSNFLRTLGLIP  80


> XP_002113584.1 hypothetical protein TRIADDRAFT_57151 [Trichoplax 
adhaerens]
Length=188

 Score = 29.3 bits (64),  Expect = 1.4, Method: Compositional matrix adjust.
 Identities = 16/67 (24%), Positives = 30/67 (45%), Gaps = 15/67 (22%)

Query  65   LHRFCSDCIVTALRSGNKE--CPTC-------------RKKLVSKRSLRPDPNFDALISK  109
            L+++C+DC    LR  N +  C  C             ++KL ++ +L    N D   ++
Sbjct  23   LNKYCADCQTILLRDKNNQEICVNCHVIEDKPLKQEEKKQKLTTETNLSDSKNVDTTRNE  82

Query  110  IYPSREE  116
            +Y   E+
Sbjct  83   LYQHAEK  89


> XP_002117466.1 hypothetical protein TRIADDRAFT_61456 [Trichoplax 
adhaerens]
Length=618

 Score = 29.6 bits (65),  Expect = 1.5, Method: Compositional matrix adjust.
 Identities = 13/40 (33%), Positives = 19/40 (48%), Gaps = 1/40 (3%)

Query  48   CPICLDMLKNTMTTKECLHRFCSDCIVTALRSGNKECPTC  87
            C IC++     +T+  C H  C +C +  L    K CP C
Sbjct  566  CLICMEPYYIPLTSINCWHVHCEECWMRTL-GAKKLCPQC  604


> XP_002107666.1 hypothetical protein TRIADDRAFT_63462 [Trichoplax 
adhaerens]
Length=288

 Score = 29.3 bits (64),  Expect = 1.5, Method: Compositional matrix adjust.
 Identities = 14/47 (30%), Positives = 22/47 (47%), Gaps = 2/47 (4%)

Query  47   MCPICLDMLKN--TMTTKECLHRFCSDCIVTALRSGNKECPTCRKKL  91
            +C ICLD       +    C H +   CI   L    +ECP C++++
Sbjct  221  VCAICLDEYNEGEKLRILPCKHAYHCKCIDPWLTDNKRECPVCKRRV  267


> XP_002113156.1 hypothetical protein TRIADDRAFT_57035 [Trichoplax 
adhaerens]
Length=440

 Score = 29.3 bits (64),  Expect = 2.1, Method: Compositional matrix adjust.
 Identities = 14/47 (30%), Positives = 20/47 (43%), Gaps = 3/47 (6%)

Query  46  LMCPIC---LDMLKNTMTTKECLHRFCSDCIVTALRSGNKECPTCRK  89
           +MC  C     +LK     + C   FC +C+  +   GN  C  C K
Sbjct  1   MMCYKCAAKFGLLKRQNYCRRCNKLFCKNCLFKSTHDGNLICKDCLK  47


> XP_002112826.1 hypothetical protein TRIADDRAFT_56365 [Trichoplax 
adhaerens]
Length=2524

 Score = 28.1 bits (61),  Expect = 5.1, Method: Composition-based stats.
 Identities = 11/25 (44%), Positives = 14/25 (56%), Gaps = 0/25 (0%)

Query  310  QQEAGEPGGPGGGASDTGGPDGCGG  334
            ++E GE G      + T  PDGCGG
Sbjct  877  KREFGEKGATTNSVAGTTKPDGCGG  901


> XP_002113634.1 hypothetical protein TRIADDRAFT_27129 [Trichoplax 
adhaerens]
Length=424

 Score = 27.7 bits (60),  Expect = 5.8, Method: Compositional matrix adjust.
 Identities = 16/40 (40%), Positives = 22/40 (55%), Gaps = 1/40 (3%)

Query  48  CPICLDMLKNTMTTKECLHRFCSDCIVTALRS-GNKECPT  86
           C +C ++L   +  K C HR CS C+   LRS  N +CP 
Sbjct  22  CIVCGNVLCYPLQFKTCGHRTCSGCLKEVLRSKSNPKCPI  61


> XP_002116583.1 hypothetical protein TRIADDRAFT_30995, partial 
[Trichoplax adhaerens]
Length=201

 Score = 27.3 bits (59),  Expect = 6.1, Method: Compositional matrix adjust.
 Identities = 11/34 (32%), Positives = 16/34 (47%), Gaps = 3/34 (9%)

Query  58  TMTTKECLHRFCSDCIVTALRSGNK---ECPTCR  88
            +   +C H F  +CI   ++S      ECP CR
Sbjct  25  VVMLSQCKHYFHDECIFACVQSKTSEFLECPKCR  58


> XP_002107658.1 hypothetical protein TRIADDRAFT_19665, partial 
[Trichoplax adhaerens]
Length=556

 Score = 27.3 bits (59),  Expect = 8.6, Method: Compositional matrix adjust.
 Identities = 15/41 (37%), Positives = 23/41 (56%), Gaps = 2/41 (5%)

Query  46  LMCPICLDMLKNTMTTKECLHRFCSDCIVTALRSGNKECPT  86
           L C +C  ++K  +    C H FC  CI T++  GN+ CP+
Sbjct  10  LRCLLCQRIMKEPVVA-SCGHTFCKRCIETSVL-GNERCPS  48


> XP_002115433.1 hypothetical protein TRIADDRAFT_59362 [Trichoplax 
adhaerens]
Length=463

 Score = 27.3 bits (59),  Expect = 9.2, Method: Compositional matrix adjust.
 Identities = 14/52 (27%), Positives = 22/52 (42%), Gaps = 7/52 (13%)

Query  48  CPICLDMLKNTMTTKECLH------RFCSDCIVTALRSGNKECPTCRKKLVS  93
           CPICL   +N    ++C         FC  CI+       + CP C+ +  +
Sbjct  25  CPICLCPFENKSYLEKCFRILNEIDTFCFYCIL-QWSEVVQTCPLCKSEFTT  75



Lambda      K        H        a         alpha
   0.311    0.134    0.406    0.792     4.96 

Gapped
Lambda      K        H        a         alpha    sigma
   0.267   0.0410    0.140     1.90     42.6     43.6 

Effective search space used: 1275456560


  Database: Placozoa/GCF_000150275.1_v1.0_protein.faa
    Posted date:  Jun 24, 2020  10:44 AM
  Number of letters in database: 5,220,296
  Number of sequences in database:  11,520



Matrix: BLOSUM62
Gap Penalties: Existence: 11, Extension: 1
Neighboring words threshold: 11
Window for multiple hits: 40
