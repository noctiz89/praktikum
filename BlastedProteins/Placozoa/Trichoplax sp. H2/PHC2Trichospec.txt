BLASTP 2.6.0+


Reference: Stephen F. Altschul, Thomas L. Madden, Alejandro A.
Schaffer, Jinghui Zhang, Zheng Zhang, Webb Miller, and David J.
Lipman (1997), "Gapped BLAST and PSI-BLAST: a new generation of
protein database search programs", Nucleic Acids Res. 25:3389-3402.


Reference for composition-based statistics: Alejandro A. Schaffer,
L. Aravind, Thomas L. Madden, Sergei Shavirin, John L. Spouge, Yuri
I. Wolf, Eugene V. Koonin, and Stephen F. Altschul (2001),
"Improving the accuracy of PSI-BLAST protein database searches with
composition-based statistics and other refinements", Nucleic Acids
Res. 29:2994-3005.



Database: GCA_003344405.1_TrispH2_1.0_protein.faa
           12,174 sequences; 6,595,317 total letters



Query= AAI44122.1 PHC2 protein [Homo sapiens]

Length=830
                                                                      Score     E
Sequences producing significant alignments:                          (Bits)  Value

  RDD42395.1 Polyhomeotic-like protein 1 [Trichoplax sp. H2]          75.5    2e-15
  RDD44261.1 Sterile alpha motif domain-containing protein 11 [Tr...  77.0    5e-15
  RDD47832.1 Histone acetyltransferase KAT6A [Trichoplax sp. H2]      62.8    3e-10
  RDD44572.1 Sex comb on midleg-like protein 2 [Trichoplax sp. H2]    60.1    3e-09
  RDD47741.1 Lethal(3)malignant brain tumor-like protein 4 [Trich...  53.1    3e-07
  RDD47860.1 hypothetical protein TrispH2_000557 [Trichoplax sp. H2]  35.8    0.070
  RDD37907.1 Sphingomyelin synthase-related protein 1 [Trichoplax...  32.3    0.71 
  RDD42855.1 Eukaryotic translation initiation factor 4H [Trichop...  30.8    1.5  
  RDD40348.1 Ankyrin-3 [Trichoplax sp. H2]                            30.8    2.2  
  RDD37197.1 Tyrosine-protein kinase CSK [Trichoplax sp. H2]          30.0    3.8  
  RDD46091.1 Ankyrin repeat and SAM domain-containing protein 3 [...  29.6    4.6  
  RDD41975.1 hypothetical protein TrispH2_005140 [Trichoplax sp. H2]  28.5    9.2  
  RDD46663.1 Liprin-beta-1 [Trichoplax sp. H2]                        28.9    9.7  


> RDD42395.1 Polyhomeotic-like protein 1 [Trichoplax sp. H2]
Length=230

 Score = 75.5 bits (184),  Expect = 2e-15, Method: Composition-based stats.
 Identities = 33/71 (46%), Positives = 51/71 (72%), Gaps = 0/71 (0%)

Query  758  FLPSEPTKWNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKEDHLMSAMNIKLGPA  817
            +L S P  WN + V  F+ ++ GC+   + F  +++DG+ALLLL+E+HL+S +N+KLGPA
Sbjct  139  WLSSSPYSWNSQQVAYFVCTIDGCERYGQLFLNEQVDGEALLLLREEHLISILNLKLGPA  198

Query  818  LKIYARISMLK  828
            LKI A++  LK
Sbjct  199  LKILAKVKKLK  209


 Score = 32.7 bits (73),  Expect = 0.32, Method: Composition-based stats.
 Identities = 25/86 (29%), Positives = 41/86 (48%), Gaps = 6/86 (7%)

Query  538  PQILTHVIEGFVIQEGAEPF--PVGRSSLLVGNLKKKYAQGF--LPEKLPQQDHTTTTDS  593
            P I+TH+I+GF+I+E  +PF  P   +    GN      +    + +K P     +T + 
Sbjct  13   PSIITHLIDGFIIEESDQPFIKPSTTTYSTEGNDLTANTENVKNVSKKYPIHSKASTVN-  71

Query  594  EMEEPYLQESKEEGAPLKLKCELCGR  619
            +M   Y + +   G  LK   ELC +
Sbjct  72   QMSYKY-RRNTSNGKLLKTSRELCTK  96


> RDD44261.1 Sterile alpha motif domain-containing protein 11 [Trichoplax 
sp. H2]
Length=426

 Score = 77.0 bits (188),  Expect = 5e-15, Method: Compositional matrix adjust.
 Identities = 34/64 (53%), Positives = 45/64 (70%), Gaps = 1/64 (2%)

Query  766  WNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKEDHLMSAMNIKLGPALKIYARI-  824
            W+V+DVY F+  +    EIA  FR  EI G+AL LL E+H++S MNIKLGPALKI + + 
Sbjct  353  WDVDDVYNFVSQILSSDEIANIFREHEICGEALSLLNENHMLSTMNIKLGPALKIRSHVM  412

Query  825  SMLK  828
             ML+
Sbjct  413  KMLR  416


> RDD47832.1 Histone acetyltransferase KAT6A [Trichoplax sp. H2]
Length=557

 Score = 62.8 bits (151),  Expect = 3e-10, Method: Compositional matrix adjust.
 Identities = 31/68 (46%), Positives = 43/68 (63%), Gaps = 1/68 (1%)

Query  763  PTKWNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKEDHLMSAMNIKLGPALKIYA  822
            P +W V DV    + L G  E +  F  QEIDG+AL LL    ++SA+ +KLGPALKI++
Sbjct  482  PLEWKVSDVALVFQKL-GFAEFSHVFLVQEIDGKALFLLNRSDVISALGLKLGPALKIFS  540

Query  823  RISMLKDS  830
             I +L+ S
Sbjct  541  YIDILQSS  548


> RDD44572.1 Sex comb on midleg-like protein 2 [Trichoplax sp. 
H2]
Length=932

 Score = 60.1 bits (144),  Expect = 3e-09, Method: Compositional matrix adjust.
 Identities = 31/65 (48%), Positives = 41/65 (63%), Gaps = 1/65 (2%)

Query  766  WNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKEDHLMSAMNIKLGPALKIYARIS  825
            W+V+DV  FI       + A  F   EIDG+ALLLL+ + LM+ M +KLGPALK+Y  I 
Sbjct  867  WSVQDVANFIIQTEML-DYARLFSDNEIDGKALLLLEREILMNYMGLKLGPALKLYHIIK  925

Query  826  MLKDS  830
             +K S
Sbjct  926  FIKSS  930


> RDD47741.1 Lethal(3)malignant brain tumor-like protein 4 [Trichoplax 
sp. H2]
Length=743

 Score = 53.1 bits (126),  Expect = 3e-07, Method: Compositional matrix adjust.
 Identities = 25/67 (37%), Positives = 40/67 (60%), Gaps = 1/67 (1%)

Query  762  EPTKWNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKEDHLMSAMNIKLGPALKIY  821
            E   W V D+ ++I ++ GC E A  F  + IDG++ LL+ +  +   +  KLGPALK++
Sbjct  676  EVLNWGVTDLMKYIIAI-GCGEEATIFARELIDGESFLLMTQQDMTEHLQFKLGPALKLF  734

Query  822  ARISMLK  828
              IS +K
Sbjct  735  YHISSIK  741


 Score = 29.6 bits (65),  Expect = 5.4, Method: Compositional matrix adjust.
 Identities = 13/30 (43%), Positives = 15/30 (50%), Gaps = 0/30 (0%)

Query  612  LKCELCGRVDFAYKFKRSKRFCSMACAKRY  641
            L C  CG+   +  F  S RFCS  CA  Y
Sbjct  127  LCCINCGKYGVSSNFTASGRFCSRQCAGMY  156


> RDD47860.1 hypothetical protein TrispH2_000557 [Trichoplax sp. 
H2]
Length=804

 Score = 35.8 bits (81),  Expect = 0.070, Method: Compositional matrix adjust.
 Identities = 18/60 (30%), Positives = 29/60 (48%), Gaps = 0/60 (0%)

Query  753  GMGHHFLPSEPTKWNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKEDHLMSAMNI  812
            G+   +  ++  KW+V+DV E++ SL   +     F    IDG  L  L +D L+    I
Sbjct  360  GLSDSYKVTKLLKWSVDDVQEWLTSLQIHERYMMTFEEYRIDGYLLSCLTDDDLIQQFGI  419


> RDD37907.1 Sphingomyelin synthase-related protein 1 [Trichoplax 
sp. H2]
Length=428

 Score = 32.3 bits (72),  Expect = 0.71, Method: Compositional matrix adjust.
 Identities = 20/65 (31%), Positives = 35/65 (54%), Gaps = 3/65 (5%)

Query  766  WNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKED--HLMSAMNIKLGPALKIYAR  823
            W   +V ++++    C+ I + F+  ++DG+ALL LKE    L+  +   LG   +I+  
Sbjct  16   WTPHEVADWLQDRGHCEYI-DTFKKNKMDGRALLALKEKDLRLLDGLATCLGDLKRIWLD  74

Query  824  ISMLK  828
            I  LK
Sbjct  75   IQELK  79


> RDD42855.1 Eukaryotic translation initiation factor 4H [Trichoplax 
sp. H2]
Length=260

 Score = 30.8 bits (68),  Expect = 1.5, Method: Composition-based stats.
 Identities = 20/54 (37%), Positives = 25/54 (46%), Gaps = 13/54 (24%)

Query  332  PPPQQSRPVLQAEPHPQLASVSPSVALQPSSEAHAMPLGPVTPALPLQCPTANL  385
            PPP++ R     EP P+ AS  P + LQP S           PAL +   T NL
Sbjct  202  PPPEEFR-----EPSPETASRRPKLNLQPRSVG--------APALSVASDTRNL  242


> RDD40348.1 Ankyrin-3 [Trichoplax sp. H2]
Length=1100

 Score = 30.8 bits (68),  Expect = 2.2, Method: Compositional matrix adjust.
 Identities = 17/47 (36%), Positives = 27/47 (57%), Gaps = 6/47 (13%)

Query  755   GHHFLPSEPTKWNVEDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLL  801
             G +FL     +W+ +DV  +++ +   Q   E+FRA +IDG AL  L
Sbjct  1015  GLNFL-----QWSCDDVVNWLQEITLAQ-YDEKFRAADIDGIALFEL  1055


> RDD37197.1 Tyrosine-protein kinase CSK [Trichoplax sp. H2]
Length=492

 Score = 30.0 bits (66),  Expect = 3.8, Method: Compositional matrix adjust.
 Identities = 31/95 (33%), Positives = 46/95 (48%), Gaps = 9/95 (9%)

Query  25   SSSGCNNSSSGGSGRPTGPQISVYSGIPDRQTVQVIQQALHRQPSTAAQYLQQMYAAQ--  82
            SS G     S GS R  G +IS+ SG PD     +IQ+AL RQ ST   +   ++A+   
Sbjct  216  SSLGRFGEVSSGSYR--GREISIKSGRPDS---IIIQEALLRQASTLT-FGDNIFASHFN  269

Query  83   -QQHLMLQTAALQQQHLSSAQLQSLAAVQQASLVS  116
             ++ L L    L  +++      +L   Q  SLV+
Sbjct  270  PKKDLTLCMEHLHHKNIIEFIGITLDDQQDISLVT  304


> RDD46091.1 Ankyrin repeat and SAM domain-containing protein 3 
[Trichoplax sp. H2]
Length=661

 Score = 29.6 bits (65),  Expect = 4.6, Method: Compositional matrix adjust.
 Identities = 22/53 (42%), Positives = 30/53 (57%), Gaps = 3/53 (6%)

Query  769  EDVYEFIRSLPGCQEIAEEFRAQEIDGQALLLLKEDHLMSAMNIK-LGPALKI  820
            E V +F+  L   + ++  F  QEID + LL L E  L+S + IK LGP  KI
Sbjct  426  EGVVQFLEQLKLSKYLST-FEEQEIDMETLLTLSEADLIS-IGIKLLGPRRKI  476


> RDD41975.1 hypothetical protein TrispH2_005140 [Trichoplax sp. 
H2]
Length=415

 Score = 28.5 bits (62),  Expect = 9.2, Method: Compositional matrix adjust.
 Identities = 16/55 (29%), Positives = 30/55 (55%), Gaps = 0/55 (0%)

Query  692  VPLSVTAALQLTHSQEDSSRCSDNSSYEEPLSPISASSSTSRRRQGQRDLELPDM  746
            V   V +   +   +E++ +CS N S+     PI +S S+ + RQG +D E+ ++
Sbjct  184  VDSDVGSLFGINDQEEETHQCSGNISWNGSPDPIDSSPSSRKYRQGPQDTEIINL  238


> RDD46663.1 Liprin-beta-1 [Trichoplax sp. H2]
Length=780

 Score = 28.9 bits (63),  Expect = 9.7, Method: Compositional matrix adjust.
 Identities = 32/117 (27%), Positives = 46/117 (39%), Gaps = 21/117 (18%)

Query  190  AQMVQNLTLRTQQTPAAAASGPTPTQPVLPSLALKPTPGGSQPLPTPAQSRNTAQASPAG  249
            A  V+  TL   QTP             LP+L L     GSQP     + ++    SP  
Sbjct  235  ANQVKESTLNRTQTPQ------------LPTLTL----SGSQPDVNSDEVKSPTSGSPRK  278

Query  250  AKPGIADSVMEPHKKGDGNSSVPGSMEGRAGLSRTVPAVAAHPLIAPAYAQLQPHQL  306
              P  A +        DGN+      + +A   +  P + AHP+   +Y Q+ PH L
Sbjct  279  TSPSPASTNSAKFFSEDGNAM---HADAKALSVKLYPDIKAHPI--NSYFQINPHLL  330



Lambda      K        H        a         alpha
   0.310    0.125    0.359    0.792     4.96 

Gapped
Lambda      K        H        a         alpha    sigma
   0.267   0.0410    0.140     1.90     42.6     43.6 

Effective search space used: 3869014446


  Database: GCA_003344405.1_TrispH2_1.0_protein.faa
    Posted date:  Jun 24, 2020  11:11 AM
  Number of letters in database: 6,595,317
  Number of sequences in database:  12,174



Matrix: BLOSUM62
Gap Penalties: Existence: 11, Extension: 1
Neighboring words threshold: 11
Window for multiple hits: 40
