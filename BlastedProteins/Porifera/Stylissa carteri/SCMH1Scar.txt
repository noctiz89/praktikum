BLASTP 2.6.0+


Reference: Stephen F. Altschul, Thomas L. Madden, Alejandro A.
Schaffer, Jinghui Zhang, Zheng Zhang, Webb Miller, and David J.
Lipman (1997), "Gapped BLAST and PSI-BLAST: a new generation of
protein database search programs", Nucleic Acids Res. 25:3389-3402.


Reference for composition-based statistics: Alejandro A. Schaffer,
L. Aravind, Thomas L. Madden, Sergei Shavirin, John L. Spouge, Yuri
I. Wolf, Eugene V. Koonin, and Stephen F. Altschul (2001),
"Improving the accuracy of PSI-BLAST protein database searches with
composition-based statistics and other refinements", Nucleic Acids
Res. 29:2994-3005.



Database: Porifera/SCAR_T-PEP_160304
           26,967 sequences; 7,945,552 total letters



Query= CAG33442.1 SCMH1 [Homo sapiens]

Length=599
                                                                      Score     E
Sequences producing significant alignments:                          (Bits)  Value

  maker-SC_scaffold10708-snap-gene-0.13-mRNA-1 protein AED:0.02 e...  66.6    2e-11
  maker-SC_scaffold113045-snap-gene-0.30-mRNA-1 protein AED:0.40 ...  57.8    2e-10
  snap_masked-SC_scaffold78-processed-gene-0.1-mRNA-1 protein AED...  55.1    9e-10
  maker-SC_scaffold5347-augustus-gene-0.153-mRNA-1 protein AED:0....  58.5    4e-09
  maker-SC_scaffold6310-augustus-gene-0.10-mRNA-1 protein AED:0.1...  54.3    7e-08
  maker-SC_scaffold105997-snap-gene-0.3-mRNA-1 protein AED:0.06 e...  38.9    0.001
  maker-SC_scaffold143925-augustus-gene-0.13-mRNA-1 protein AED:0...  34.3    0.048
  snap_masked-SC_scaffold1910-processed-gene-0.23-mRNA-1 protein ...  32.3    0.11 
  snap_masked-SC_scaffold138952-processed-gene-0.0-mRNA-1 protein...  30.4    0.25 
  maker-SC_scaffold6656-snap-gene-0.10-mRNA-1 protein AED:0.22 eA...  32.7    0.30 
  snap_masked-SC_scaffold10238-processed-gene-0.1-mRNA-1 protein ...  30.0    0.34 
  maker-SC_scaffold4279-snap-gene-0.21-mRNA-1 protein AED:0.31 eA...  30.0    0.88 
  maker-SC_scaffold32495-augustus-gene-0.27-mRNA-1 protein AED:0....  31.6    1.0  
  augustus_masked-SC_scaffold22394-processed-gene-0.0-mRNA-1 prot...  30.0    1.7  
  maker-SC_scaffold6789-augustus-gene-0.45-mRNA-1 protein AED:0.3...  30.8    1.9  
  maker-SC_scaffold37705-augustus-gene-0.7-mRNA-1 protein AED:0.0...  30.0    2.6  
  maker-SC_scaffold6933-snap-gene-0.6-mRNA-1 protein AED:0.06 eAE...  30.0    2.8  
  maker-SC_scaffold102997-augustus-gene-0.5-mRNA-1 protein AED:0....  29.3    5.4  
  maker-SC_scaffold6618-augustus-gene-0.662-mRNA-1 protein AED:0....  28.5    6.0  
  maker-SC_scaffold19753-augustus-gene-0.2-mRNA-1 protein AED:0.1...  28.5    6.9  
  maker-SC_scaffold93-augustus-gene-0.5-mRNA-1 protein AED:0.00 e...  28.1    7.1  


> maker-SC_scaffold10708-snap-gene-0.13-mRNA-1 protein AED:0.02 
eAED:0.02 QI:93|0.87|0.44|1|0.62|0.55|9|0|670
Length=670

 Score = 66.6 bits (161),  Expect = 2e-11, Method: Compositional matrix adjust.
 Identities = 45/150 (30%), Positives = 74/150 (49%), Gaps = 13/150 (9%)

Query  448  SMDSASNPTNLVSTSQRHRPLLSSCGLPPSTASA-VRRLCSRGVLKGSNERRDMESFWKL  506
            S  +  +P + V       P LS    PPS +   + +L + G    S +R+  +   K 
Sbjct  527  STAATDSPVSTVQVEPLSAPPLSRTIPPPSISIVPLDQLSALGTALLSQQRKKQQQLAKQ  586

Query  507  NRSPGSDRYLESRDASRLSGRDPSSWTVEDVMQFVREADPQLGPHADLFRKHEIDGKALL  566
            +++ G+               DP SWTVE V+ +           A+LFR+ E+DG ALL
Sbjct  587  SKALGAGLPA-----------DPHSWTVEQVVHYFYSTS-DCRDFAELFREQEVDGTALL  634

Query  567  LLRSDMMMKYMGLKLGPALKLSYHIDRLKQ  596
            LL  + ++K +G+KLG ALK+  H++ L++
Sbjct  635  LLSHESLVKCLGIKLGRALKIMVHVEELRK  664


> maker-SC_scaffold113045-snap-gene-0.30-mRNA-1 protein AED:0.40 
eAED:0.62 QI:0|0|0|1|0|0.5|2|0|117
Length=117

 Score = 57.8 bits (138),  Expect = 2e-10, Method: Compositional matrix adjust.
 Identities = 31/74 (42%), Positives = 36/74 (49%), Gaps = 10/74 (14%)

Query  106  FKMGMKLEAVDRKNPHFICPATIGEVRGSEVLVTFDGWRGAFDYWCRFDSRDIFPVGWCS  165
            FK+GMKLEA  R  P  +C  TI  ++        DGW+     WCR DS DI P GWC 
Sbjct  54   FKVGMKLEAKSRLIPALVCVETISSIK--------DGWQSM--TWCRSDSTDIRPRGWCR  103

Query  166  LTGDNLQPPGTKVV  179
                 LQ P    V
Sbjct  104  SHNKELQAPSLSCV  117


> snap_masked-SC_scaffold78-processed-gene-0.1-mRNA-1 protein AED:0.00 
eAED:0.00 QI:81|1|1|1|0|0.33|3|86|88
Length=88

 Score = 55.1 bits (131),  Expect = 9e-10, Method: Composition-based stats.
 Identities = 27/65 (42%), Positives = 42/65 (65%), Gaps = 2/65 (3%)

Query  531  SWTVEDVMQFVREADPQLGPHADLFRKHEIDGKALLLLRSDMMMKYMGLKLGPALKLSYH  590
            SWTVEDV +F+     +   + D F +HEIDGKAL L++   ++  + L+LGP LK+  H
Sbjct  17   SWTVEDVGEFLTALGYE--AYVDKFLEHEIDGKALSLVQDHHLLMTLKLRLGPTLKIIEH  74

Query  591  IDRLK  595
            ++ +K
Sbjct  75   VNAIK  79


> maker-SC_scaffold5347-augustus-gene-0.153-mRNA-1 protein AED:0.43 
eAED:0.43 QI:0|0.33|0|0.5|1|1|4|0|513
Length=513

 Score = 58.5 bits (140),  Expect = 4e-09, Method: Compositional matrix adjust.
 Identities = 28/73 (38%), Positives = 44/73 (60%), Gaps = 8/73 (11%)

Query  527  RDPSSWTVEDVMQFVREADPQLGPHAD---LFRKHEIDGKALLLLRSDMMMKYMGLKLGP  583
            + P  W VE+V +F+        PH D   +F++HE+DG+++L L  + M+K M LK GP
Sbjct  439  KSPYEWGVEEVAEFINSI-----PHIDCTEVFKEHEVDGESMLSLNPERMVKLMDLKTGP  493

Query  584  ALKLSYHIDRLKQ  596
            AL++   I  LK+
Sbjct  494  ALRIYNKITALKE  506


> maker-SC_scaffold6310-augustus-gene-0.10-mRNA-1 protein AED:0.10 
eAED:0.12 QI:0|0|0.25|1|0.33|0.5|4|79|453
Length=453

 Score = 54.3 bits (129),  Expect = 7e-08, Method: Compositional matrix adjust.
 Identities = 27/68 (40%), Positives = 40/68 (59%), Gaps = 2/68 (3%)

Query  529  PSSWTVEDVMQFVREADPQLGPHADLFRKHEIDGKALLLLRSDMMMKYMGLKLGPALKLS  588
            P  W  E+V +F+R      G HA+ F++ EIDG +L LL    +++   +KLGPAL+L 
Sbjct  384  PREWDSEEVAEFLRLRG--FGDHAEAFQESEIDGHSLFLLGEQHLIERFSMKLGPALRLL  441

Query  589  YHIDRLKQ  596
              I RL+ 
Sbjct  442  DTISRLRH  449


> maker-SC_scaffold105997-snap-gene-0.3-mRNA-1 protein AED:0.06 
eAED:0.06 QI:47|1|0.5|1|0|0|2|0|120
Length=120

 Score = 38.9 bits (89),  Expect = 0.001, Method: Compositional matrix adjust.
 Identities = 19/58 (33%), Positives = 36/58 (62%), Gaps = 2/58 (3%)

Query  522  SRLSGRDPSSWTVEDVMQFVREADPQLGPHADLFRKHEIDGKALLLLRSDMMMKYMGL  579
            S+L+  + +SW V+DV Q++      LG   D FR++ +DG+ LL L ++++   +G+
Sbjct  53   SKLNPANLTSWGVDDVCQWLNSEG--LGRFMDTFRENAVDGECLLSLDNNLLKNDLGI  108


> maker-SC_scaffold143925-augustus-gene-0.13-mRNA-1 protein AED:0.01 
eAED:0.01 QI:19|1|0.66|1|1|1|3|0|146
Length=146

 Score = 34.3 bits (77),  Expect = 0.048, Method: Compositional matrix adjust.
 Identities = 21/62 (34%), Positives = 32/62 (52%), Gaps = 3/62 (5%)

Query  513  DRYLESRDASRLSGRDPSSWTVEDVMQFVREADPQLGPHADLFRKHEIDGKALL-LLRSD  571
            D  + + + ++  G++   W  EDV Q+  E    L  + +    HEI GK LL L RSD
Sbjct  87   DTTISAANIAKKIGKEVLQWNTEDVCQWFEELG--LEEYHESIHTHEITGKELLDLHRSD  144

Query  572  MM  573
            +M
Sbjct  145  LM  146


> snap_masked-SC_scaffold1910-processed-gene-0.23-mRNA-1 protein 
AED:0.60 eAED:0.60 QI:0|0.5|0.33|0.66|1|1|3|96|91
Length=91

 Score = 32.3 bits (72),  Expect = 0.11, Method: Composition-based stats.
 Identities = 26/67 (39%), Positives = 34/67 (51%), Gaps = 11/67 (16%)

Query  532  WTVEDVMQFVREADPQLGPHADLFRKHEIDGKALL-LLRSDMMMKYMGLKLGPALKLSYH  590
            W  EDV Q+  E    L  + +    HEI GK LL L RSD+M      +LG  +K   H
Sbjct  13   WNTEDVCQWFEELG--LEEYHESIHTHEITGKELLDLHRSDLM------ELG--IKKVGH  62

Query  591  IDRLKQG  597
            + RL+Q 
Sbjct  63   LTRLRQA  69


> snap_masked-SC_scaffold138952-processed-gene-0.0-mRNA-1 protein 
AED:0.04 eAED:0.09 QI:0|-1|0|1|-1|1|1|0|70
Length=70

 Score = 30.4 bits (67),  Expect = 0.25, Method: Composition-based stats.
 Identities = 18/44 (41%), Positives = 22/44 (50%), Gaps = 4/44 (9%)

Query  329  ASVVLQQAVQACIDCAYHQKTVFSFLKQGHGG----EVISAVFD  368
            ASV L+    AC+   Y  K VF +L  G G     EV+  VFD
Sbjct  15   ASVTLKDEQTACVKAIYEGKDVFLWLPSGFGKSMCYEVLPFVFD  58


> maker-SC_scaffold6656-snap-gene-0.10-mRNA-1 protein AED:0.22 
eAED:0.22 QI:0|0|0|1|0|0|2|0|226
Length=226

 Score = 32.7 bits (73),  Expect = 0.30, Method: Compositional matrix adjust.
 Identities = 44/182 (24%), Positives = 61/182 (34%), Gaps = 49/182 (27%)

Query  146  AFDYWCR--FDSRDIFPVGWCSLTGDNLQPPGTKVVIPKNPYPASDVNTEKPSIHSSTKT  203
            A D+W    FD      V W  LTG+N   P  ++V+  N       N     I    K 
Sbjct  64   AVDFWKNKAFDXXXXLDV-WPILTGENSSTPHHEIVLGYN------FNERGAIIVGDYKL  116

Query  204  VLEHQPGQRGRKPGKKRGRTPKTLISHPISAPSKTAEPLKFPKKRGPKPGSKRKPRTLLN  263
            ++           G   GR    + S           PL +P K GP  G    P  L N
Sbjct  117  IV-----------GPTVGRCDNNMYS-----------PLDYPCKDGPSGGKDCDPYCLFN  154

Query  264  ---PPPASPTTSTPEPDT-----------STVPQDAAT----IPSSAMQAPTVCIYLNKN  305
                P      S+ +PD            +  P+D        P S  Q P  C Y++++
Sbjct  155  IVEDPEERKDLSSSQPDMLKELAMRYEQYAKEPRDMQDQGYHTPESLPQDPNACRYMHEH  214

Query  306  GS  307
            G 
Sbjct  215  GE  216


> snap_masked-SC_scaffold10238-processed-gene-0.1-mRNA-1 protein 
AED:0.04 eAED:0.09 QI:0|-1|0|1|-1|1|1|0|70
Length=70

 Score = 30.0 bits (66),  Expect = 0.34, Method: Composition-based stats.
 Identities = 18/44 (41%), Positives = 22/44 (50%), Gaps = 4/44 (9%)

Query  329  ASVVLQQAVQACIDCAYHQKTVFSFLKQGHGG----EVISAVFD  368
            ASV L+    AC+   Y  K VF +L  G G     EV+  VFD
Sbjct  15   ASVTLKDERTACVKAIYEGKDVFLWLPTGFGKSMCYEVLPFVFD  58


> maker-SC_scaffold4279-snap-gene-0.21-mRNA-1 protein AED:0.31 
eAED:0.35 QI:0|0|0|1|0|0|2|0|110
Length=110

 Score = 30.0 bits (66),  Expect = 0.88, Method: Compositional matrix adjust.
 Identities = 12/39 (31%), Positives = 23/39 (59%), Gaps = 0/39 (0%)

Query  281  VPQDAATIPSSAMQAPTVCIYLNKNGSTGPHLDKKKVQQ  319
            + Q+ A    +A+  PT+C +L++NG +   L K  +Q+
Sbjct  60   IQQELAETTGTAVSVPTLCNFLHRNGLSWKKLSKMALQR  98


> maker-SC_scaffold32495-augustus-gene-0.27-mRNA-1 protein AED:0.29 
eAED:0.33 QI:18|0.23|0.16|0.80|0.66|0.74|31|215|1426
Length=1426

 Score = 31.6 bits (70),  Expect = 1.0, Method: Compositional matrix adjust.
 Identities = 41/174 (24%), Positives = 70/174 (40%), Gaps = 33/174 (19%)

Query  339  ACIDC-----AYHQKTVFSFLKQGHGGEVISAVFDREQHTLNLPAVNSITYVLRFLEKLC  393
            AC  C     AY  + +  ++     G V+S VFDRE +     +V++ T+         
Sbjct  724  ACYVCWSFARAYEPEELKPYVNAIASGLVVSVVFDREVNCRRAASVSACTH---------  774

Query  394  HNLRSDNLLGNQPFTQTHLSLTAIE-----------YSHSHDRYLPGETFVLGN--SLAR  440
            H  R   L+  Q  T T   +  +            + H  D     + F +G+  ++  
Sbjct  775  HRYRFSFLVLCQFVTSTWWCILQVAAFQENVGRQGTFPHGIDILTTADYFAVGSRMNVYL  834

Query  441  SLEPHSDSMDSASNPT--NLVSTSQRH--RPLLSSCGLPPSTASAVRRLCSRGV  490
            S+  +       + P   +LV     H  R LL+SC L P+  S++R L +R +
Sbjct  835  SVSVYIAQYSEYTRPLIDHLVECKLGHWDRLLLTSCVLCPT--SSLRELAARAL  886


> augustus_masked-SC_scaffold22394-processed-gene-0.0-mRNA-1 protein 
AED:0.23 eAED:0.23 QI:0|-1|0|1|-1|1|1|0|187
Length=187

 Score = 30.0 bits (66),  Expect = 1.7, Method: Compositional matrix adjust.
 Identities = 40/172 (23%), Positives = 59/172 (34%), Gaps = 50/172 (29%)

Query  154  DSRDIFPVGWCSLTGDNLQPPGTKVVIPKNPYPASDVNTEKPSIHSSTKTVLEHQPGQRG  213
            D  D++P+    LTG+N   P  ++V+  N       N     I    K ++        
Sbjct  38   DGLDVWPI----LTGENSSTPHHEIVLGYN------FNQRGAIIVGDYKLIV--------  79

Query  214  RKPGKKRGRTPKTLISHPISAPSKTAEPLKFPKKRGPKPGSKRKPRTLLN---PPPASPT  270
               G   GR    + S           PL +P K GP  G    P  L N    P     
Sbjct  80   ---GPTVGRCDNNMYS-----------PLDYPCKDGPSGGKDCDPYCLFNIVEDPEERND  125

Query  271  TSTPEPDT-----------STVPQD----AATIPSSAMQAPTVCIYLNKNGS  307
             S+ +PD            +  P+D        P S  Q P  C Y++++G 
Sbjct  126  LSSSQPDMLKELVMRYEQYAKEPRDMQDQGYHTPESLPQDPNACRYMHEHGE  177


> maker-SC_scaffold6789-augustus-gene-0.45-mRNA-1 protein AED:0.36 
eAED:0.39 QI:0|0.5|0.4|1|1|1|5|263|910
Length=910

 Score = 30.8 bits (68),  Expect = 1.9, Method: Compositional matrix adjust.
 Identities = 21/97 (22%), Positives = 45/97 (46%), Gaps = 15/97 (15%)

Query  376  LPAVNSITYVLRFLEKLCHNLRSDNLLGNQPFTQTHLSLTAIEYSHSHDRYLPGETFVLG  435
            +P +++I +VL+  EKLC     +       + +   S+ + +  HSH++ L        
Sbjct  200  IPNLSTILFVLKLSEKLCQYPVIE-------YYKEGKSVCSYQSPHSHEQIL--------  244

Query  436  NSLARSLEPHSDSMDSASNPTNLVSTSQRHRPLLSSC  472
                R+L+P + S++   + +  ++    H+ L S C
Sbjct  245  KHCIRALQPENRSIEGGRSSSPQIAVVGTHKDLESQC  281


> maker-SC_scaffold37705-augustus-gene-0.7-mRNA-1 protein AED:0.01 
eAED:0.01 QI:0|1|0.5|1|1|1|2|103|478
Length=478

 Score = 30.0 bits (66),  Expect = 2.6, Method: Compositional matrix adjust.
 Identities = 16/49 (33%), Positives = 28/49 (57%), Gaps = 4/49 (8%)

Query  532  WTVEDVMQFVREADPQLGPHADLFRKHEIDGKALLLLRSDMMMKYMGLK  580
            ++V +V +FV     QL    D+FR++ + G+ LL L    + K +GL+
Sbjct  408  FSVTEVCEFVS----QLEIQPDVFRQNSVSGEELLELSDADLQKELGLR  452


> maker-SC_scaffold6933-snap-gene-0.6-mRNA-1 protein AED:0.06 eAED:0.06 
QI:0|1|0.71|1|0.83|0.85|7|0|714
Length=714

 Score = 30.0 bits (66),  Expect = 2.8, Method: Compositional matrix adjust.
 Identities = 25/88 (28%), Positives = 38/88 (43%), Gaps = 15/88 (17%)

Query  331  VVLQQAVQACIDCAYHQK---TVFSFLKQ---------GHGGEVISAVFDREQHTLNLPA  378
            V L++ + AC    +H K    ++ F+ +         G  G    A      HT NL  
Sbjct  411  VTLKRTINACFASGWHDKLRRIIYEFVYRTGLIMLETDGPYGGAPCASETHAHHTDNL--  468

Query  379  VNSITYVLRFLEKLCHNLRSDNLLGNQP  406
             +SI +  +   KL  +LRS N+  NQP
Sbjct  469  -DSIFWQTKLQGKLYSDLRSLNVFINQP  495


> maker-SC_scaffold102997-augustus-gene-0.5-mRNA-1 protein AED:0.30 
eAED:0.30 QI:17|1|0.33|1|1|1|3|0|680
Length=680

 Score = 29.3 bits (64),  Expect = 5.4, Method: Compositional matrix adjust.
 Identities = 25/102 (25%), Positives = 45/102 (44%), Gaps = 25/102 (25%)

Query  376  LPAVNSITYVLRFLEKLCHN-----LRSDNLLGNQPFTQTHLSLTAIEYSHSHDRYLPGE  430
            +P +++I +VL+  EKL HN         NL+ + P              +SH++ L   
Sbjct  101  VPNLSAILFVLKLSEKLSHNPEAKFYEKGNLVCSYP------------SPYSHEQIL---  145

Query  431  TFVLGNSLARSLEPHSDSMDSASNPTNLVSTSQRHRPLLSSC  472
                     R+L+P   S+++ S+ +  ++    HR L  SC
Sbjct  146  -----KRCIRALQPEKQSIENRSSSSPYIAMVGTHRDLELSC  182


> maker-SC_scaffold6618-augustus-gene-0.662-mRNA-1 protein AED:0.00 
eAED:0.00 QI:0|1|0.5|1|1|0.5|2|118|203
Length=203

 Score = 28.5 bits (62),  Expect = 6.0, Method: Compositional matrix adjust.
 Identities = 16/53 (30%), Positives = 34/53 (64%), Gaps = 3/53 (6%)

Query  530  SSWTVEDVMQFVREADPQLGPHADLFRKHEIDGKALLLLRSDMMMKYMGLKLG  582
            S+ TV+D+ +F++    +L    +LF ++++DG +LLL  +D  ++ +G+  G
Sbjct  131  SALTVDDISEFLKGI--KLADLTELFHENDVDG-SLLLQLADKDLEDLGVANG  180


> maker-SC_scaffold19753-augustus-gene-0.2-mRNA-1 protein AED:0.12 
eAED:0.12 QI:0|1|0.66|1|0.8|0.83|6|0|323
Length=323

 Score = 28.5 bits (62),  Expect = 6.9, Method: Compositional matrix adjust.
 Identities = 15/47 (32%), Positives = 22/47 (47%), Gaps = 0/47 (0%)

Query  214  RKPGKKRGRTPKTLISHPISAPSKTAEPLKFPKKRGPKPGSKRKPRT  260
            R PG  RG+    L  H IS+ +  A PL   ++R  +     + RT
Sbjct  9    RSPGHSRGKRTTLLRKHSISSDTAIAAPLGIAEERMVQGSPLHRHRT  55


> maker-SC_scaffold93-augustus-gene-0.5-mRNA-1 protein AED:0.00 
eAED:0.00 QI:3|1|1|1|0.5|0.33|3|217|187
Length=187

 Score = 28.1 bits (61),  Expect = 7.1, Method: Composition-based stats.
 Identities = 13/26 (50%), Positives = 14/26 (54%), Gaps = 0/26 (0%)

Query  199  SSTKTVLEHQPGQRGRKPGKKRGRTP  224
            SS K   E Q GQR + PGK    TP
Sbjct  149  SSEKETRETQTGQRAQSPGKSGSPTP  174



Lambda      K        H        a         alpha
   0.317    0.134    0.409    0.792     4.96 

Gapped
Lambda      K        H        a         alpha    sigma
   0.267   0.0410    0.140     1.90     42.6     43.6 

Effective search space used: 2600498730


  Database: Porifera/SCAR_T-PEP_160304
    Posted date:  Jun 24, 2020  10:44 AM
  Number of letters in database: 7,945,552
  Number of sequences in database:  26,967



Matrix: BLOSUM62
Gap Penalties: Existence: 11, Extension: 1
Neighboring words threshold: 11
Window for multiple hits: 40
