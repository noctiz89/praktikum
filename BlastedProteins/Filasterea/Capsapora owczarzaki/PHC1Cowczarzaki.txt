BLASTP 2.6.0+


Reference: Stephen F. Altschul, Thomas L. Madden, Alejandro A.
Schaffer, Jinghui Zhang, Zheng Zhang, Webb Miller, and David J.
Lipman (1997), "Gapped BLAST and PSI-BLAST: a new generation of
protein database search programs", Nucleic Acids Res. 25:3389-3402.


Reference for composition-based statistics: Alejandro A. Schaffer,
L. Aravind, Thomas L. Madden, Sergei Shavirin, John L. Spouge, Yuri
I. Wolf, Eugene V. Koonin, and Stephen F. Altschul (2001),
"Improving the accuracy of PSI-BLAST protein database searches with
composition-based statistics and other refinements", Nucleic Acids
Res. 29:2994-3005.



Database: Filasterea/GCF_000151315.2_C_owczarzaki_V2_protein.faa
           8,792 sequences; 5,565,921 total letters



Query= AAH73964.1 PHC1 protein [Homo sapiens]

Length=957
                                                                      Score     E
Sequences producing significant alignments:                          (Bits)  Value

  XP_004349356.1 hypothetical protein CAOG_02606 [Capsaspora owcz...  35.8    0.045
  XP_004348442.1 hypothetical protein CAOG_03537 [Capsaspora owcz...  33.5    0.045
  XP_004346774.2 hypothetical protein CAOG_05089 [Capsaspora owcz...  35.8    0.067
  XP_004349116.1 RapGAP/RanGAP domain-containing protein [Capsasp...  33.5    0.37 
  XP_004342637.1 hypothetical protein CAOG_08036 [Capsaspora owcz...  31.6    1.4  
  XP_004363648.1 hypothetical protein CAOG_03920 [Capsaspora owcz...  29.6    4.7  
  XP_004346962.1 hypothetical protein CAOG_05277 [Capsaspora owcz...  29.6    5.3  
  XP_004349259.2 iron-responsive element-binding protein 1 [Capsa...  28.9    8.1  


> XP_004349356.1 hypothetical protein CAOG_02606 [Capsaspora owczarzaki 
ATCC 30864]
Length=339

 Score = 35.8 bits (81),  Expect = 0.045, Method: Compositional matrix adjust.
 Identities = 21/62 (34%), Positives = 31/62 (50%), Gaps = 1/62 (2%)

Query  893  WSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKICAKIN  952
            W+ E+V +++ +  G  E    F+  +IDG+ALL L  E L S      G  L+I   I 
Sbjct  255  WTAEDVAQWMGA-NGFAEYIPLFQENDIDGEALLALDHETLGSMSITSAGRRLRILRAIA  313

Query  953  VL  954
             L
Sbjct  314  TL  315


> XP_004348442.1 hypothetical protein CAOG_03537 [Capsaspora owczarzaki 
ATCC 30864]
Length=98

 Score = 33.5 bits (75),  Expect = 0.045, Method: Compositional matrix adjust.
 Identities = 18/52 (35%), Positives = 31/52 (60%), Gaps = 1/52 (2%)

Query  891  SRWSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLG  942
            S W+V +V E++ +L    +++E F    ++G ALL L   HL + M I++G
Sbjct  10   SDWTVNQVVEWVQALDLDVDVSELFLKHGMNGFALLRLSAAHL-TEMGIRVG  60


> XP_004346774.2 hypothetical protein CAOG_05089 [Capsaspora owczarzaki 
ATCC 30864]
Length=1119

 Score = 35.8 bits (81),  Expect = 0.067, Method: Compositional matrix adjust.
 Identities = 17/64 (27%), Positives = 32/64 (50%), Gaps = 1/64 (2%)

Query  893  WSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKICAKIN  952
            W+  EV +++ + +G  E    F    +DG+ L+ L+++HL       LG  LK+   + 
Sbjct  11   WTDAEVGQWLEA-EGFGEYVAIFADNNVDGEVLVSLQQDHLKDLGVTSLGKRLKLVKSLR  69

Query  953  VLKE  956
             L +
Sbjct  70   ALVD  73


> XP_004349116.1 RapGAP/RanGAP domain-containing protein [Capsaspora 
owczarzaki ATCC 30864]
Length=1051

 Score = 33.5 bits (75),  Expect = 0.37, Method: Compositional matrix adjust.
 Identities = 16/45 (36%), Positives = 26/45 (58%), Gaps = 3/45 (7%)

Query  780  YNVSCSHQFRL---KRKKMKEFQEANYARVRRRGPRRSSSDIARA  821
            +  +C+HQF L   KRKKM + +++   R+ +  PR  +  I RA
Sbjct  356  WTETCAHQFTLPMRKRKKMDDLRDSIIERMGQLCPRDCTRGIKRA  400


> XP_004342637.1 hypothetical protein CAOG_08036 [Capsaspora owczarzaki 
ATCC 30864]
Length=749

 Score = 31.6 bits (70),  Expect = 1.4, Method: Compositional matrix adjust.
 Identities = 24/64 (38%), Positives = 36/64 (56%), Gaps = 3/64 (5%)

Query  399  TAPQPPQVPPTQQVPPSQSQQQAQTLVVQPMLQSSPLSLPPDAAPKPPIPIQSKP--PVA  456
            TAP+P QV P +QV P++  ++    V+ P+ +  P + P   A  P  P+ S P  PVA
Sbjct  272  TAPEPEQV-PVRQVEPTRKPEEPAAKVILPIAKRLPAAPPVPVASAPVAPVVSAPVAPVA  330

Query  457  PIKP  460
            P+ P
Sbjct  331  PVTP  334


> XP_004363648.1 hypothetical protein CAOG_03920 [Capsaspora owczarzaki 
ATCC 30864]
Length=1138

 Score = 29.6 bits (65),  Expect = 4.7, Method: Compositional matrix adjust.
 Identities = 13/40 (33%), Positives = 27/40 (68%), Gaps = 0/40 (0%)

Query  354  IAIHHQQQFQHRQSQLLHTATHLQLAQQQQQQQQQQQQQP  393
            ++++HQ+  + ++ QL H A  L+  Q++QQ + Q++Q P
Sbjct  600  VSMNHQRISKQQRKQLRHEAHQLRKLQKKQQLEMQRRQSP  639


> XP_004346962.1 hypothetical protein CAOG_05277 [Capsaspora owczarzaki 
ATCC 30864]
Length=1255

 Score = 29.6 bits (65),  Expect = 5.3, Method: Compositional matrix adjust.
 Identities = 24/86 (28%), Positives = 34/86 (40%), Gaps = 0/86 (0%)

Query  517  SGQAHLASSPPSSQAPGALQECPPTLAPGMTLAPVQGTAHVVKGGATTSSPVVAQVPAAF  576
            SG+   ASS P+ Q   AL++    L P     P   T+++V     T     A  P   
Sbjct  600  SGEPRRASSKPTFQRRLALKQRMKQLPPSALWQPYSLTSNIVWASLATLHNAAALPPRTV  659

Query  577  YMQSVHLPGKPQTLAVKRKADSEEER  602
            +   VH P       ++R    EE R
Sbjct  660  FALDVHDPRTVHRSTMRRAVHVEERR  685


> XP_004349259.2 iron-responsive element-binding protein 1 [Capsaspora 
owczarzaki ATCC 30864]
Length=1103

 Score = 28.9 bits (63),  Expect = 8.1, Method: Compositional matrix adjust.
 Identities = 27/99 (27%), Positives = 43/99 (43%), Gaps = 11/99 (11%)

Query  416  QSQQQAQTLVVQPMLQSSPLSLPPDAAPKPPIPIQSKPPVAPIKPPQLGAA-KMSAAQQP  474
             +++  ++LV+ P  + S  SLPP A P  P+    +  VAP   P  G++  +S   Q 
Sbjct  286  DNRELPESLVIHPRAKPSLASLPPTATPSSPVRGSRRSLVAPTVTPTPGSSNNVSDRSQS  345

Query  475  PPHI----------PVQVVGTRQPGTAQAQALGLAQLAA  503
             P I           +  VGT     A +   G+  +AA
Sbjct  346  APRIQHVARDGGFLVMDTVGTSDSHAALSNGAGILTVAA  384



Lambda      K        H        a         alpha
   0.308    0.122    0.340    0.792     4.96 

Gapped
Lambda      K        H        a         alpha    sigma
   0.267   0.0410    0.140     1.90     42.6     43.6 

Effective search space used: 3967774709


  Database: Filasterea/GCF_000151315.2_C_owczarzaki_V2_protein.faa
    Posted date:  Jun 24, 2020  11:21 AM
  Number of letters in database: 5,565,921
  Number of sequences in database:  8,792



Matrix: BLOSUM62
Gap Penalties: Existence: 11, Extension: 1
Neighboring words threshold: 11
Window for multiple hits: 40
