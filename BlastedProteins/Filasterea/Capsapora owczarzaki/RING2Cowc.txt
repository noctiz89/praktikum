BLASTP 2.6.0+


Reference: Stephen F. Altschul, Thomas L. Madden, Alejandro A.
Schaffer, Jinghui Zhang, Zheng Zhang, Webb Miller, and David J.
Lipman (1997), "Gapped BLAST and PSI-BLAST: a new generation of
protein database search programs", Nucleic Acids Res. 25:3389-3402.


Reference for composition-based statistics: Alejandro A. Schaffer,
L. Aravind, Thomas L. Madden, Sergei Shavirin, John L. Spouge, Yuri
I. Wolf, Eugene V. Koonin, and Stephen F. Altschul (2001),
"Improving the accuracy of PSI-BLAST protein database searches with
composition-based statistics and other refinements", Nucleic Acids
Res. 29:2994-3005.



Database: Filasterea/GCF_000151315.2_C_owczarzaki_V2_protein.faa
           8,792 sequences; 5,565,921 total letters



Query= sp|Q99496|RING2_HUMAN E3 ubiquitin-protein ligase RING2 OS=Homo
sapiens OX=9606 GN=RNF2 PE=1 SV=1

Length=336
                                                                      Score     E
Sequences producing significant alignments:                          (Bits)  Value

  XP_004365242.2 hypothetical protein CAOG_00371 [Capsaspora owcz...  92.8    1e-21
  XP_004346855.2 hypothetical protein CAOG_05170 [Capsaspora owcz...  51.2    2e-07
  XP_011270517.1 hypothetical protein CAOG_08862 [Capsaspora owcz...  45.4    1e-05
  XP_004343252.2 ring finger protein 20 [Capsaspora owczarzaki AT...  44.7    2e-05
  XP_004365048.1 hypothetical protein CAOG_00177 [Capsaspora owcz...  44.7    3e-05
  XP_004345239.1 hypothetical protein CAOG_06490 [Capsaspora owcz...  43.1    8e-05
  XP_004347632.1 hypothetical protein CAOG_04881 [Capsaspora owcz...  42.0    2e-04
  XP_004342971.1 hypothetical protein CAOG_07886 [Capsaspora owcz...  41.6    3e-04
  XP_004364034.1 hypothetical protein CAOG_03195 [Capsaspora owcz...  40.4    5e-04
  XP_004346849.1 hypothetical protein CAOG_05164 [Capsaspora owcz...  40.4    5e-04
  XP_004366013.1 hypothetical protein CAOG_01142 [Capsaspora owcz...  40.8    5e-04
  XP_004348113.2 hypothetical protein CAOG_04288 [Capsaspora owcz...  40.4    5e-04
  XP_004348969.2 hypothetical protein CAOG_02219 [Capsaspora owcz...  40.4    6e-04
  XP_004347015.1 hypothetical protein CAOG_05330 [Capsaspora owcz...  39.3    0.001
  XP_004347627.1 SNF2 superfamily RAD5 protein [Capsaspora owczar...  39.3    0.001
  XP_004363799.2 hypothetical protein CAOG_02960 [Capsaspora owcz...  38.9    0.002
  XP_004365921.2 ariadne ubiquitin-conjugating enzyme E2 binding ...  36.2    0.012
  XP_004365262.1 hypothetical protein CAOG_00391 [Capsaspora owcz...  35.8    0.017
  XP_004345525.2 zinc finger family protein [Capsaspora owczarzak...  35.8    0.017
  XP_004349071.2 hypothetical protein CAOG_02321 [Capsaspora owcz...  35.4    0.020
  XP_004365749.2 hypothetical protein CAOG_00878 [Capsaspora owcz...  35.4    0.026
  XP_004365564.1 hypothetical protein CAOG_00693 [Capsaspora owcz...  33.9    0.058
  XP_004345653.2 hypothetical protein CAOG_06063 [Capsaspora owcz...  33.9    0.063
  XP_004365935.1 hypothetical protein CAOG_01064 [Capsaspora owcz...  33.5    0.069
  XP_004365112.1 RFWD3 protein [Capsaspora owczarzaki ATCC 30864]     32.7    0.16 
  XP_011270793.1 hypothetical protein CAOG_09060 [Capsaspora owcz...  32.3    0.21 
  XP_004346926.1 hypothetical protein CAOG_05241 [Capsaspora owcz...  32.0    0.30 
  XP_004364071.2 E3 ubiquitin-protein ligase CBL-B-A [Capsaspora ...  31.6    0.32 
  XP_004347494.2 hypothetical protein CAOG_04743 [Capsaspora owcz...  30.4    0.51 
  XP_004365695.1 hypothetical protein CAOG_00824 [Capsaspora owcz...  30.8    0.55 
  XP_004342884.2 hypothetical protein CAOG_07811 [Capsaspora owcz...  30.8    0.59 
  XP_004343779.2 hypothetical protein CAOG_07055 [Capsaspora owcz...  30.4    0.72 
  XP_004365669.1 hypothetical protein CAOG_00798 [Capsaspora owcz...  30.0    0.84 
  XP_004343242.1 hypothetical protein CAOG_07383 [Capsaspora owcz...  30.0    0.88 
  XP_004348782.1 hypothetical protein CAOG_02032 [Capsaspora owcz...  30.0    1.0  
  XP_004349809.2 DNA repair protein RAD16 [Capsaspora owczarzaki ...  29.3    1.9  
  XP_004349450.2 SPRY domain-containing protein [Capsaspora owcza...  28.5    2.8  
  XP_004345207.1 hypothetical protein CAOG_06458 [Capsaspora owcz...  28.1    4.2  
  XP_004346316.1 hypothetical protein CAOG_05643 [Capsaspora owcz...  28.1    4.5  
  XP_004364201.2 CHY zinc finger family protein [Capsaspora owcza...  27.7    5.1  
  XP_011270555.1 hypothetical protein CAOG_08893, partial [Capsas...  27.7    5.4  
  XP_004345686.2 hypothetical protein CAOG_06096 [Capsaspora owcz...  27.7    5.6  
  XP_004343087.2 hypothetical protein CAOG_07228 [Capsaspora owcz...  27.7    5.7  
  XP_004364183.1 hypothetical protein CAOG_03344 [Capsaspora owcz...  27.3    6.7  
  XP_004348260.1 hypothetical protein CAOG_04432 [Capsaspora owcz...  27.3    8.3  
  XP_004348080.1 hypothetical protein CAOG_04255 [Capsaspora owcz...  26.9    9.2  


> XP_004365242.2 hypothetical protein CAOG_00371 [Capsaspora owczarzaki 
ATCC 30864]
Length=341

 Score = 92.8 bits (229),  Expect = 1e-21, Method: Compositional matrix adjust.
 Identities = 52/151 (34%), Positives = 79/151 (52%), Gaps = 8/151 (5%)

Query  13   LSKTWELSLYELQRT-PQEAITDGLEIVVSPRSLHSELMCPICLDMLKNTMTTKECLHRF  71
            +  +  L  YE QR+ P   I D     + P +  + L CP+CL +++N     ECLHRF
Sbjct  19   IPDSMRLDAYEEQRSVPIPTIADNRRATI-PLTF-APLQCPVCLCLIQNAHGNIECLHRF  76

Query  72   CADCIITALRSGNKECPTCRKKLVSKRSLRPDPNFDALISKIYPSRDEYEAHQERVLARI  131
            C +CI +A+R G K+CP+CR  L ++RSLR D N D L++ +Y   +++E  Q    A  
Sbjct  77   CEECITSAVRMGPKQCPSCRGSLPTRRSLRHDSNLDQLVAAMYGDHEQFEKQQNEYSAEQ  136

Query  132  NKHNNQQALSHSIEEGLKIQAMNRLQRGKKQ  162
             +    Q L   +    K     R+ RG +Q
Sbjct  137  IRRKQAQTLVLKLPRREK-----RVARGDRQ  162


> XP_004346855.2 hypothetical protein CAOG_05170 [Capsaspora owczarzaki 
ATCC 30864]
Length=428

 Score = 51.2 bits (121),  Expect = 2e-07, Method: Compositional matrix adjust.
 Identities = 31/91 (34%), Positives = 42/91 (46%), Gaps = 2/91 (2%)

Query  39   VVSPRSLHSELMCPICLDMLKNTMTTKECLHRFCADCIITALRSGNKECPTCRKKLVSKR  98
            ++S   L     CPIC  +L NT  T  C HR+C  CI   L      CP C  +L    
Sbjct  11   LISSTMLLEHFTCPICCSLLSNTTITIPCGHRYCKSCIDECLNR-KSVCPCCNTRLNGGV  69

Query  99   SLRPDPNFDALISKIYPSRDEYE-AHQERVL  128
             L+ D  +DAL+  I   R   + AH  R++
Sbjct  70   VLQRDHQYDALLEMIQKERKAADAAHLVRLV  100


> XP_011270517.1 hypothetical protein CAOG_08862 [Capsaspora owczarzaki 
ATCC 30864]
Length=343

 Score = 45.4 bits (106),  Expect = 1e-05, Method: Compositional matrix adjust.
 Identities = 21/53 (40%), Positives = 28/53 (53%), Gaps = 4/53 (8%)

Query  47   SELMCPICLDMLKNTMTTKECLHRFCADCIITALRS---GNKECPTCRKKLVS  96
            S L CPIC+ + + + T   C H FCA CI T L+      + CP CR  + S
Sbjct  149  SHLECPICMSLFR-SATDLSCGHSFCASCISTQLQQVEPNQRRCPICRTPVTS  200


> XP_004343252.2 ring finger protein 20 [Capsaspora owczarzaki 
ATCC 30864]
Length=323

 Score = 44.7 bits (104),  Expect = 2e-05, Method: Compositional matrix adjust.
 Identities = 16/46 (35%), Positives = 26/46 (57%), Gaps = 1/46 (2%)

Query  49   LMCPICLDMLKNTMTTKECLHRFCADCIITALRSGNKECPTCRKKL  94
            + CPIC   +K+T+  + C H FC +C+     +  + CPTC K+ 
Sbjct  269  IFCPICRTNVKDTVMLR-CFHSFCNECVQKRYDTRQRACPTCAKQF  313


> XP_004365048.1 hypothetical protein CAOG_00177 [Capsaspora owczarzaki 
ATCC 30864]
Length=1169

 Score = 44.7 bits (104),  Expect = 3e-05, Method: Compositional matrix adjust.
 Identities = 32/103 (31%), Positives = 45/103 (44%), Gaps = 13/103 (13%)

Query  28   PQEAITDGLEIVVSPRSLHSELM--CPICLDML--KNTMTTKECLHRFCADCIITALRSG  83
            P     D L   V  R L   +   CPIC+D+   +N      C H FC  C++  L +G
Sbjct  815  PATGGVDSLPADVRKRVLEGNIQTECPICMDVATGENEGVISRCGHVFCRACLVAYLENG  874

Query  84   NKE-------CPTCRKKLVSKRSLRPDPNFDALISKIYPSRDE  119
             +        CPTCR+  + +R + P       +SK  PS DE
Sbjct  875  LRRNVNDKATCPTCRQP-IDQRQVVPLAAIQKSMSK-KPSADE  915


> XP_004345239.1 hypothetical protein CAOG_06490 [Capsaspora owczarzaki 
ATCC 30864]
Length=532

 Score = 43.1 bits (100),  Expect = 8e-05, Method: Compositional matrix adjust.
 Identities = 20/57 (35%), Positives = 31/57 (54%), Gaps = 3/57 (5%)

Query  42   PRS--LHSELMCPICLDMLKNTMTTKECLHRFCADCIITALRSGNKECPTCRKKLVS  96
            PR+  +   L C IC+++L + ++   CLH FC  C    +     +CPTCR K+ S
Sbjct  166  PRTDDMEQNLQCGICMEILHDCVSVVPCLHDFCGACYSDWMEK-KSDCPTCRAKVTS  221


> XP_004347632.1 hypothetical protein CAOG_04881 [Capsaspora owczarzaki 
ATCC 30864]
Length=2080

 Score = 42.0 bits (97),  Expect = 2e-04, Method: Compositional matrix adjust.
 Identities = 18/47 (38%), Positives = 27/47 (57%), Gaps = 2/47 (4%)

Query  50    MCPICLDMLKNTMTTKECLHRFCADCIITALRSGNKECPTCRKKLVS  96
             +CPIC++     +    C H FCA CI   +R  ++ CPTCR ++ S
Sbjct  1805  VCPICIET-STELCMTPCGHVFCAPCIADWMRH-HRICPTCRSRIQS  1849


> XP_004342971.1 hypothetical protein CAOG_07886 [Capsaspora owczarzaki 
ATCC 30864]
Length=712

 Score = 41.6 bits (96),  Expect = 3e-04, Method: Compositional matrix adjust.
 Identities = 23/70 (33%), Positives = 35/70 (50%), Gaps = 2/70 (3%)

Query  34   DGLEIVVSPRSLHSELMCPICLDMLKNTMTTKECLHRFCADCIITALRSGNKECPTCRKK  93
            +G ++ V  R+L   L+C ICL          EC+H FC  CI+  +R+        RKK
Sbjct  61   EGGKVSVRLRTLTPWLVCRICLGYCVTPTVIVECMHPFCKSCIVKHIRTTRWNA--TRKK  118

Query  94   LVSKRSLRPD  103
            L + + L P+
Sbjct  119  LPNIQVLCPE  128


 Score = 30.8 bits (68),  Expect = 0.58, Method: Compositional matrix adjust.
 Identities = 13/28 (46%), Positives = 18/28 (64%), Gaps = 0/28 (0%)

Query  242  SAQTRYIKTSGNATVDHLSKYLAVRLAL  269
            SA+   +  S NATVDH+ KY+  R +L
Sbjct  622  SARPSIVGVSSNATVDHVRKYIITRYSL  649


> XP_004364034.1 hypothetical protein CAOG_03195 [Capsaspora owczarzaki 
ATCC 30864]
Length=365

 Score = 40.4 bits (93),  Expect = 5e-04, Method: Compositional matrix adjust.
 Identities = 21/54 (39%), Positives = 27/54 (50%), Gaps = 3/54 (6%)

Query  45   LHSELMCPICLDMLKNTMTTKECLHRF-CADCIITALRSGNKECPTCRKKLVSK  97
            L  E+MC  C D  +N +T   C H   C DC    LR+GN  CP CR  +  +
Sbjct  308  LEGEIMCIACNDQPRN-VTYGSCRHHVVCTDCDTKILRNGNA-CPVCRAAITQR  359


> XP_004346849.1 hypothetical protein CAOG_05164 [Capsaspora owczarzaki 
ATCC 30864]
Length=422

 Score = 40.4 bits (93),  Expect = 5e-04, Method: Compositional matrix adjust.
 Identities = 25/75 (33%), Positives = 36/75 (48%), Gaps = 2/75 (3%)

Query  55   LDMLKNTMTTKECLHRFCADCIITALRSGNKECPTCRKKLVSKRSLRPDPNFDALISKIY  114
            + +L NT  T  C HR+C  CI   L +    CP C  +L     L+ D  +DAL+  I 
Sbjct  18   ISLLSNTTITIPCGHRYCKSCIDECL-NRKPVCPCCNTRLSGGVVLQRDHQYDALLEMIQ  76

Query  115  PSRDEYE-AHQERVL  128
              R   + AH  R++
Sbjct  77   KERKAADAAHLVRLV  91


> XP_004366013.1 hypothetical protein CAOG_01142 [Capsaspora owczarzaki 
ATCC 30864]
Length=982

 Score = 40.8 bits (94),  Expect = 5e-04, Method: Compositional matrix adjust.
 Identities = 25/84 (30%), Positives = 38/84 (45%), Gaps = 17/84 (20%)

Query  45   LHSELMCPICLDMLKNTMTTKECLHRFCADCII-----------TALRS----GNKECPT  89
            L   + CPIC   L +  +T  C H+FC  CI+            A RS        CP 
Sbjct  22   LKKSIECPICCCSLVDPYSTP-CNHQFCKTCILEHEYRSTKDEQEAKRSKKAAAPARCPM  80

Query  90   CRKKLVSKRSLRPDPNFDALISKI  113
            C++   ++R+L+  P F AL+  +
Sbjct  81   CKQPF-TRRALKQSPRFAALVESV  103


> XP_004348113.2 hypothetical protein CAOG_04288 [Capsaspora owczarzaki 
ATCC 30864]
Length=601

 Score = 40.4 bits (93),  Expect = 5e-04, Method: Compositional matrix adjust.
 Identities = 18/44 (41%), Positives = 25/44 (57%), Gaps = 3/44 (7%)

Query  51   CPICL-DMLKNTMTTKECLHRFCADCIITALRSGNKECPTCRKK  93
            CP+CL D +  TM  + C H FC +C I    + N+ CP C+ K
Sbjct  84   CPVCLGDFVDKTML-ESCFHIFCYEC-IRRWSAVNRMCPLCKTK  125


> XP_004348969.2 hypothetical protein CAOG_02219 [Capsaspora owczarzaki 
ATCC 30864]
Length=689

 Score = 40.4 bits (93),  Expect = 6e-04, Method: Compositional matrix adjust.
 Identities = 15/50 (30%), Positives = 26/50 (52%), Gaps = 1/50 (2%)

Query  43  RSLHSELMCPICLDMLKNTMTTKECLHRFCADCIITALRSGNKECPTCRK  92
           R+L + + C +CL+     M    C H FC+ C+   + +  + CP+C K
Sbjct  50  RNLDALMRCAVCLEFFNTAMMVTHCSHTFCSLCVRRHVEADGR-CPSCLK  98


> XP_004347015.1 hypothetical protein CAOG_05330 [Capsaspora owczarzaki 
ATCC 30864]
Length=422

 Score = 39.3 bits (90),  Expect = 0.001, Method: Compositional matrix adjust.
 Identities = 20/50 (40%), Positives = 28/50 (56%), Gaps = 2/50 (4%)

Query  42  PRSLHSELMCPICLDMLKNTMTTKECLHRFCADCIITALRSGNKECPTCR  91
           P+ L + L C +CL  L   +TT  C H +C +C+ +AL    K CP CR
Sbjct  24  PQQLEAFLDCALCLKALFQPVTTT-CGHSYCRNCLASALEY-KKLCPLCR  71


> XP_004347627.1 SNF2 superfamily RAD5 protein [Capsaspora owczarzaki 
ATCC 30864]
Length=1372

 Score = 39.3 bits (90),  Expect = 0.001, Method: Compositional matrix adjust.
 Identities = 23/64 (36%), Positives = 32/64 (50%), Gaps = 11/64 (17%)

Query  51    CPICLDMLKNTMTTKECLHRFCADCI---ITALRSGNKECPTCRKKLVSKRSL---RPDP  104
             CPICLD  ++ + T  CLH  C  C+   +  L S    CP CRK +  ++ +   RP  
Sbjct  1043  CPICLDFPEDIVVTP-CLHTGCKGCMQHTVARLHS----CPVCRKPVEPQQLVQVARPPA  1097

Query  105   NFDA  108
             N  A
Sbjct  1098  NLAA  1101


> XP_004363799.2 hypothetical protein CAOG_02960 [Capsaspora owczarzaki 
ATCC 30864]
Length=609

 Score = 38.9 bits (89),  Expect = 0.002, Method: Compositional matrix adjust.
 Identities = 28/95 (29%), Positives = 43/95 (45%), Gaps = 2/95 (2%)

Query  40   VSPRSLHSELMCPICLDMLK--NTMTTKECLHRFCADCIITALRSGNKECPTCRKKLVSK  97
            VS  S   E  C ICLD  K  +T+    C H F  +C+   L + N+ CP CR +  ++
Sbjct  373  VSLASTVCEETCVICLDDFKEGDTLRCLPCSHDFHQNCVDQWLLTKNRACPLCRSQPFAE  432

Query  98   RSLRPDPNFDALISKIYPSRDEYEAHQERVLARIN  132
                     D L   + PS ++    Q R+ A ++
Sbjct  433  TLSDNSSQSDDLELVLSPSAEQSPERQTRIEASLD  467


> XP_004365921.2 ariadne ubiquitin-conjugating enzyme E2 binding 
protein [Capsaspora owczarzaki ATCC 30864]
Length=544

 Score = 36.2 bits (82),  Expect = 0.012, Method: Compositional matrix adjust.
 Identities = 16/38 (42%), Positives = 22/38 (58%), Gaps = 1/38 (3%)

Query  46   HSELMCPIC-LDMLKNTMTTKECLHRFCADCIITALRS  82
            H E+ C IC +D  +  MT   C HRFC  C ++ L+S
Sbjct  132  HKEMTCEICYVDYPRKDMTGLPCNHRFCRHCWVSYLQS  169


> XP_004365262.1 hypothetical protein CAOG_00391 [Capsaspora owczarzaki 
ATCC 30864]
Length=778

 Score = 35.8 bits (81),  Expect = 0.017, Method: Compositional matrix adjust.
 Identities = 19/61 (31%), Positives = 29/61 (48%), Gaps = 2/61 (3%)

Query  51   CPICLDMLKNTMTTKECLHRFCADCIITALRSGNKECPTCRKKLVSKRSLRPDPNFDALI  110
            C  C  ++KN  +T  C H FC  CI   +   +K CP C+     K  +R +P    ++
Sbjct  30   CSACNQIMKNVHSTFTCEHFFCEACIKDEINDHSK-CPECQSPAWIK-DIRANPQIQGIV  87

Query  111  S  111
            S
Sbjct  88   S  88


> XP_004345525.2 zinc finger family protein [Capsaspora owczarzaki 
ATCC 30864]
Length=633

 Score = 35.8 bits (81),  Expect = 0.017, Method: Compositional matrix adjust.
 Identities = 20/54 (37%), Positives = 27/54 (50%), Gaps = 3/54 (6%)

Query  51   CPICLDMLKNTMTTKECLHRFCADCIITALRSGNKECPTCRKK-LVSKRSLRPD  103
            CPIC +  K+ +    C H FC DC +T      + CP CR   L + R+L  D
Sbjct  572  CPICQEETKDPVALP-CNHIFCEDC-VTQWFERERTCPMCRTTILTAGRALWRD  623


> XP_004349071.2 hypothetical protein CAOG_02321 [Capsaspora owczarzaki 
ATCC 30864]
Length=824

 Score = 35.4 bits (80),  Expect = 0.020, Method: Compositional matrix adjust.
 Identities = 13/35 (37%), Positives = 18/35 (51%), Gaps = 0/35 (0%)

Query  42   PRSLHSELMCPICLDMLKNTMTTKECLHRFCADCI  76
            P  +  EL CPIC  ++ N ++   C   FC  CI
Sbjct  268  PDDVPEELRCPICTQIMDNAVSVDCCSTTFCDSCI  302


> XP_004365749.2 hypothetical protein CAOG_00878 [Capsaspora owczarzaki 
ATCC 30864]
Length=1907

 Score = 35.4 bits (80),  Expect = 0.026, Method: Composition-based stats.
 Identities = 23/86 (27%), Positives = 37/86 (43%), Gaps = 9/86 (10%)

Query  15    KTWELSLYELQRTPQEAITDGLEIVVSPRSLHSELM--CPICLDMLKN------TMTTKE  66
             + W L L     +    + DG+ +  S    H E +  CPIC  ++        ++  K 
Sbjct  1819  RKWLLQLTAFLASQNGRVVDGILLWKSNADKHFEGVDDCPICYSVIHAINYSLPSLRCKT  1878

Query  67    CLHRFCADCIITALRS-GNKECPTCR  91
             C ++F + C+     S GN  CP CR
Sbjct  1879  CKNKFHSACLYKWFNSSGNSTCPLCR  1904


> XP_004365564.1 hypothetical protein CAOG_00693 [Capsaspora owczarzaki 
ATCC 30864]
Length=464

 Score = 33.9 bits (76),  Expect = 0.058, Method: Compositional matrix adjust.
 Identities = 16/41 (39%), Positives = 17/41 (41%), Gaps = 1/41 (2%)

Query  51  CPICLDMLKNTMTTKECLHRFCADCIITALRSGNKECPTCR  91
           C ICL           C H FCA C I A    N  CP C+
Sbjct  38  CVICLSSFTERGRLPTCPHLFCAPC-IQAWADVNNACPMCK  77


> XP_004345653.2 hypothetical protein CAOG_06063 [Capsaspora owczarzaki 
ATCC 30864]
Length=416

 Score = 33.9 bits (76),  Expect = 0.063, Method: Compositional matrix adjust.
 Identities = 18/51 (35%), Positives = 27/51 (53%), Gaps = 2/51 (4%)

Query  51  CPICLDMLKNTMTTKE-CLHRFCADCIITALRSGNKECPTCRKKLVSKRSL  100
           C ICL  L++  +    C H F   CI+    + N  CPTCR  + S+++L
Sbjct  4   CSICLCALESEASVAVVCGHVFHRSCIVEWFGTSNS-CPTCRIAVRSQKAL  53


> XP_004365935.1 hypothetical protein CAOG_01064 [Capsaspora owczarzaki 
ATCC 30864]
Length=287

 Score = 33.5 bits (75),  Expect = 0.069, Method: Compositional matrix adjust.
 Identities = 19/51 (37%), Positives = 26/51 (51%), Gaps = 7/51 (14%)

Query  48   ELMCPICL-DMLKNTMTT-KECLHRFCADCIIT--ALRSGNKECPTCRKKL  94
            E +C ICL DM   +      CLH FC  C++T   +RS    CP C+ + 
Sbjct  120  ENVCAICLEDMFDESKAQLPPCLHEFCIRCVLTWSTVRSC---CPLCKTEF  167


> XP_004365112.1 RFWD3 protein [Capsaspora owczarzaki ATCC 30864]
Length=1019

 Score = 32.7 bits (73),  Expect = 0.16, Method: Compositional matrix adjust.
 Identities = 16/47 (34%), Positives = 23/47 (49%), Gaps = 5/47 (11%)

Query  51   CPICLDMLKNT----MTTKECLHRFCADCIITALRSGNKECPTCRKK  93
            C IC +   N+    +   +C H F  +CI T L+S  K CP C  +
Sbjct  380  CSICFESWSNSGLHRIVALKCGHLFGKNCIETWLKSAEK-CPECNGR  425


> XP_011270793.1 hypothetical protein CAOG_09060 [Capsaspora owczarzaki 
ATCC 30864]
Length=913

 Score = 32.3 bits (72),  Expect = 0.21, Method: Compositional matrix adjust.
 Identities = 15/44 (34%), Positives = 22/44 (50%), Gaps = 6/44 (14%)

Query  51   CPICL-DMLKNTMTTKECLHRFCADCIITALRSGN---KECPTC  90
            CPICL   +   M   +C H FC  C++  +  G    ++CP C
Sbjct  330  CPICLCHPIAAKMA--QCGHVFCWACVLQYISFGERPWRKCPIC  371


> XP_004346926.1 hypothetical protein CAOG_05241 [Capsaspora owczarzaki 
ATCC 30864]
Length=774

 Score = 32.0 bits (71),  Expect = 0.30, Method: Compositional matrix adjust.
 Identities = 16/46 (35%), Positives = 23/46 (50%), Gaps = 2/46 (4%)

Query  51   CPICLDMLKNTMTTKECLHR-FCADCIITALRSGNKECPTCRKKLV  95
            C +CL +     T  +C H   CA CI+ A + G   CP C++  V
Sbjct  723  CIVCL-VNPRQFTVLDCGHFCICAACILAAGQRGLTNCPMCQQPAV  767


> XP_004364071.2 E3 ubiquitin-protein ligase CBL-B-A [Capsaspora 
owczarzaki ATCC 30864]
Length=781

 Score = 31.6 bits (70),  Expect = 0.32, Method: Compositional matrix adjust.
 Identities = 14/45 (31%), Positives = 22/45 (49%), Gaps = 1/45 (2%)

Query  50   MCPICLDMLKNTMTTKECLHRFCADCIITALRSGNKECPTCRKKL  94
            +C IC    KN +    C H  C  C+     +G++ CP CR ++
Sbjct  399  LCKICSVNDKN-VRINPCGHLLCLACVTHWRSTGSQVCPFCRDQI  442


> XP_004347494.2 hypothetical protein CAOG_04743 [Capsaspora owczarzaki 
ATCC 30864]
Length=219

 Score = 30.4 bits (67),  Expect = 0.51, Method: Compositional matrix adjust.
 Identities = 18/55 (33%), Positives = 23/55 (42%), Gaps = 5/55 (9%)

Query  50  MCPIC---LDMLKNT-MTTKECLHRFCADCIITALRSGNKECPTCRKKLVSKRSL  100
            CP C   +  L+N  M    C H+FC  CI          C  C K+LV +  L
Sbjct  9   FCPFCKTDVYFLQNAEMLVTPCGHKFCTTCIRQHFTQLTMPCLVC-KRLVKRTEL  62


> XP_004365695.1 hypothetical protein CAOG_00824 [Capsaspora owczarzaki 
ATCC 30864]
Length=426

 Score = 30.8 bits (68),  Expect = 0.55, Method: Compositional matrix adjust.
 Identities = 13/40 (33%), Positives = 17/40 (43%), Gaps = 1/40 (3%)

Query  51   CPICLDMLKNTMTTKECLHRFCADCIITALRSGNKECPTC  90
            C IC++   N   +  C H  C  C +  L    K CP C
Sbjct  374  CLICMEPYTNPAVSINCWHVHCQQCWLATL-GAKKVCPQC  412


> XP_004342884.2 hypothetical protein CAOG_07811 [Capsaspora owczarzaki 
ATCC 30864]
Length=771

 Score = 30.8 bits (68),  Expect = 0.59, Method: Compositional matrix adjust.
 Identities = 19/62 (31%), Positives = 31/62 (50%), Gaps = 13/62 (21%)

Query  51   CPICLDMLKNTMTTK------ECLHRFCADCII-----TALRSGNKECPTCRKKLVSKRS  99
            CP+C DML    TT+      +C  R+C  C+      TA  S + + P+  K+L ++ +
Sbjct  658  CPVCPDMLTRDPTTQGVGCCAQCNKRWCWSCLRDMVAHTACTSTSYQSPS--KRLAARSA  715

Query  100  LR  101
             R
Sbjct  716  AR  717


> XP_004343779.2 hypothetical protein CAOG_07055 [Capsaspora owczarzaki 
ATCC 30864]
Length=804

 Score = 30.4 bits (67),  Expect = 0.72, Method: Compositional matrix adjust.
 Identities = 16/52 (31%), Positives = 24/52 (46%), Gaps = 4/52 (8%)

Query  51   CPICLDMLKNT---MTTKECLHRFCADCIITALRSGNKECPTCRKKLVSKRS  99
            C +C++ +  T        C H F  DC+   +     +CPTCR+ LV   S
Sbjct  747  CVVCMNEIPYTRRGYMITPCSHIFHTDCLQRWM-DVKMQCPTCRQHLVPDTS  797


> XP_004365669.1 hypothetical protein CAOG_00798 [Capsaspora owczarzaki 
ATCC 30864]
Length=423

 Score = 30.0 bits (66),  Expect = 0.84, Method: Compositional matrix adjust.
 Identities = 15/44 (34%), Positives = 21/44 (48%), Gaps = 2/44 (5%)

Query  48   ELMCPICLDML--KNTMTTKECLHRFCADCIITALRSGNKECPT  89
            E++C +CL     + T+ T  C H F  DCI   L   +  CP 
Sbjct  372  EVVCTVCLMPFDAQQTVRTLSCGHEFHRDCIDNWLSQSSLNCPV  415


> XP_004343242.1 hypothetical protein CAOG_07383 [Capsaspora owczarzaki 
ATCC 30864]
Length=512

 Score = 30.0 bits (66),  Expect = 0.88, Method: Compositional matrix adjust.
 Identities = 17/64 (27%), Positives = 33/64 (52%), Gaps = 0/64 (0%)

Query  105  NFDALISKIYPSRDEYEAHQERVLARINKHNNQQALSHSIEEGLKIQAMNRLQRGKKQQI  164
            NF  L++K   +R++    +ER   R      ++ L  S  + ++   M R+++ K +Q+
Sbjct  437  NFSGLVAKWTAAREQAVEDEEREEQRKWDEQQRRQLLESRPDHMQQVMMRRVEKWKNEQL  496

Query  165  ENGS  168
            E GS
Sbjct  497  ERGS  500


> XP_004348782.1 hypothetical protein CAOG_02032 [Capsaspora owczarzaki 
ATCC 30864]
Length=346

 Score = 30.0 bits (66),  Expect = 1.0, Method: Compositional matrix adjust.
 Identities = 13/51 (25%), Positives = 23/51 (45%), Gaps = 2/51 (4%)

Query  51   CPICLD--MLKNTMTTKECLHRFCADCIITALRSGNKECPTCRKKLVSKRS  99
            C +C++   +   +    C H F   CI+  L      CP C++ + + RS
Sbjct  234  CAVCIEEFAVGENLRVLPCNHLFHDACIVPWLTQQRSTCPICKRDVRTGRS  284


> XP_004349809.2 DNA repair protein RAD16 [Capsaspora owczarzaki 
ATCC 30864]
Length=769

 Score = 29.3 bits (64),  Expect = 1.9, Method: Compositional matrix adjust.
 Identities = 14/49 (29%), Positives = 23/49 (47%), Gaps = 3/49 (6%)

Query  49   LMCPICLDMLKNTMTTKECLHRFCADCIITALRSGN---KECPTCRKKL  94
            L+C IC +  ++ +    C H FC + +   L S      +CP C + L
Sbjct  599  LVCGICHEEAEDAIVAASCRHVFCREDMHLYLSSSGVDKPQCPVCFRPL  647


> XP_004349450.2 SPRY domain-containing protein [Capsaspora owczarzaki 
ATCC 30864]
Length=500

 Score = 28.5 bits (62),  Expect = 2.8, Method: Compositional matrix adjust.
 Identities = 18/54 (33%), Positives = 27/54 (50%), Gaps = 7/54 (13%)

Query  47   SELMCPICLDMLKNTMTTKECLHR-FCADCIITALRSGNKECPTCRKKLVSKRS  99
             EL C +C+D  + ++    C H  FC DC           CP CR K+V+++S
Sbjct  404  GELRCILCVDEPR-SIRLLPCNHEGFCPDC-----AQQCDLCPLCRVKVVARQS  451


> XP_004345207.1 hypothetical protein CAOG_06458 [Capsaspora owczarzaki 
ATCC 30864]
Length=1094

 Score = 28.1 bits (61),  Expect = 4.2, Method: Compositional matrix adjust.
 Identities = 26/102 (25%), Positives = 42/102 (41%), Gaps = 18/102 (18%)

Query  23   ELQRTPQEAITDGLEIVVS-----------PRSLHSELMCPICLDMLKNTMTTKECLHRF  71
            ++++ P+ A   G+E+ VS           P SL   L CP+ L +L+       C H  
Sbjct  442  KVEKRPRVA---GIELAVSLLSKTDDEIDVPSSLQVSLTCPLTLAVLRLPARGVSCKHVQ  498

Query  72   CADC---IITALRSGNKECPTCRKKLVSKRSLRPDPNFDALI  110
            C +    I    R     CP C +   + R LR D   + ++
Sbjct  499  CFELETYISVCSRQRTWICPICSQP-TAYRHLRIDDQLNTIL  539


> XP_004346316.1 hypothetical protein CAOG_05643 [Capsaspora owczarzaki 
ATCC 30864]
Length=1180

 Score = 28.1 bits (61),  Expect = 4.5, Method: Compositional matrix adjust.
 Identities = 15/52 (29%), Positives = 22/52 (42%), Gaps = 12/52 (23%)

Query  50   MCPICLDMLKNTMTTKECLHRFCADCIITALRSGN---------KECPTCRK  92
            +C  C+  +   +   EC H FC +CI    +S +         K CP C K
Sbjct  680  VCSCCVAAVPAKL---ECSHYFCVNCIQPRAKSASSTPSTFTAPKTCPMCDK  728


> XP_004364201.2 CHY zinc finger family protein [Capsaspora owczarzaki 
ATCC 30864]
Length=394

 Score = 27.7 bits (60),  Expect = 5.1, Method: Compositional matrix adjust.
 Identities = 17/60 (28%), Positives = 27/60 (45%), Gaps = 5/60 (8%)

Query  43   RSLHSELMCPICLDML---KNTMTTKECLHRFCADCIITALRSGNKECPTCRKKLVSKRS  99
            +SLH    CP+CL+ L   +  +    C H     C+   L+     CP C K +V  ++
Sbjct  262  KSLHQN--CPVCLEFLFTSREAVQMLSCGHPMHFKCLRRMLKQSKYTCPMCSKCVVDMKA  319


> XP_011270555.1 hypothetical protein CAOG_08893, partial [Capsaspora 
owczarzaki ATCC 30864]
Length=529

 Score = 27.7 bits (60),  Expect = 5.4, Method: Compositional matrix adjust.
 Identities = 17/47 (36%), Positives = 20/47 (43%), Gaps = 3/47 (6%)

Query  51   CPICLDMLKNTMTTKECLHR-FCADCIITALRSGNKECPTCRKKLVS  96
            C ICL     T     C H   C +C  T L   N+ CP CR  + S
Sbjct  470  CLICLSA-PTTAKLMPCRHACLCTECA-TTLMQRNERCPVCRGHIES  514


> XP_004345686.2 hypothetical protein CAOG_06096 [Capsaspora owczarzaki 
ATCC 30864]
Length=527

 Score = 27.7 bits (60),  Expect = 5.6, Method: Compositional matrix adjust.
 Identities = 17/53 (32%), Positives = 22/53 (42%), Gaps = 2/53 (4%)

Query  45   LHSELMCPICLDMLKNTMTTKECLHRFCADCIITALRSGNKECPTCRKKLVSK  97
            L  +  C +C+D          C H  C D    A    +K CP CR  LV+K
Sbjct  469  LRDQETCIVCMDAAIQ-FAFVPCGHYVCCDINGCASSLVDKPCPLCR-ALVTK  519


> XP_004343087.2 hypothetical protein CAOG_07228 [Capsaspora owczarzaki 
ATCC 30864]
Length=1437

 Score = 27.7 bits (60),  Expect = 5.7, Method: Compositional matrix adjust.
 Identities = 22/78 (28%), Positives = 35/78 (45%), Gaps = 1/78 (1%)

Query  9    GTQPLSKTWELSLYELQRTPQEAITDGLEIVVSPRSLHSELMCPICLDMLKNTMTTKECL  68
            GT PL+ +   S   +   P     + +  VVS     +E+  P+ LD ++      E  
Sbjct  298  GTTPLTLSPFGSTESIGIEPSVLYREMINAVVSSLGARTEIPQPLLLDTIRKAFDMTETA  357

Query  69   HR-FCADCIITALRSGNK  85
            H+   AD    AL+SG+K
Sbjct  358  HQAAVADATKRALQSGSK  375


> XP_004364183.1 hypothetical protein CAOG_03344 [Capsaspora owczarzaki 
ATCC 30864]
Length=1038

 Score = 27.3 bits (59),  Expect = 6.7, Method: Compositional matrix adjust.
 Identities = 14/35 (40%), Positives = 22/35 (63%), Gaps = 0/35 (0%)

Query  133  KHNNQQALSHSIEEGLKIQAMNRLQRGKKQQIENG  167
            K+  Q AL  +I+EGL   + N L++ K + +ENG
Sbjct  314  KYGEQLALFKAIQEGLVTISHNVLRQDKAKVLENG  348


> XP_004348260.1 hypothetical protein CAOG_04432 [Capsaspora owczarzaki 
ATCC 30864]
Length=625

 Score = 27.3 bits (59),  Expect = 8.3, Method: Compositional matrix adjust.
 Identities = 25/91 (27%), Positives = 44/91 (48%), Gaps = 5/91 (5%)

Query  209  NNNAAMAIDPVMDGASEIELVFRPHPTLMEKDDSAQTRYIKTSGNATVDHLSKYLAVRLA  268
            N   A+ +D V +  S+I  V R H  L E   +A  R ++ +  A+V HL+  L     
Sbjct  4    NRAGALLLDAVQE--SDIARV-RKH--LSEPGAAALVRTVQDATGASVLHLAAGLGSIEI  58

Query  269  LEELRSKGESNQMNLDTASEKQYTIYIATAS  299
            + EL  +G +  MN    + +   +++A A+
Sbjct  59   VYELLRRGAAASMNTAHPTTQAIPLHLAAAA  89


> XP_004348080.1 hypothetical protein CAOG_04255 [Capsaspora owczarzaki 
ATCC 30864]
Length=1193

 Score = 26.9 bits (58),  Expect = 9.2, Method: Compositional matrix adjust.
 Identities = 14/44 (32%), Positives = 23/44 (52%), Gaps = 2/44 (5%)

Query  53   ICLDMLKNTMTTKECLHRFCADCIITALRSG--NKECPTCRKKL  94
            + LDM+    +TKE   + C+  I T   SG   KE  + +++L
Sbjct  950  VVLDMINTAKSTKEQAQQLCSVAIPTKRTSGTIQKEMESTKQRL  993



Lambda      K        H        a         alpha
   0.312    0.126    0.359    0.792     4.96 

Gapped
Lambda      K        H        a         alpha    sigma
   0.267   0.0410    0.140     1.90     42.6     43.6 

Effective search space used: 1140094121


  Database: Filasterea/GCF_000151315.2_C_owczarzaki_V2_protein.faa
    Posted date:  Jun 24, 2020  10:44 AM
  Number of letters in database: 5,565,921
  Number of sequences in database:  8,792



Matrix: BLOSUM62
Gap Penalties: Existence: 11, Extension: 1
Neighboring words threshold: 11
Window for multiple hits: 40
