BLASTP 2.6.0+


Reference: Stephen F. Altschul, Thomas L. Madden, Alejandro A.
Schaffer, Jinghui Zhang, Zheng Zhang, Webb Miller, and David J.
Lipman (1997), "Gapped BLAST and PSI-BLAST: a new generation of
protein database search programs", Nucleic Acids Res. 25:3389-3402.


Reference for composition-based statistics: Alejandro A. Schaffer,
L. Aravind, Thomas L. Madden, Sergei Shavirin, John L. Spouge, Yuri
I. Wolf, Eugene V. Koonin, and Stephen F. Altschul (2001),
"Improving the accuracy of PSI-BLAST protein database searches with
composition-based statistics and other refinements", Nucleic Acids
Res. 29:2994-3005.



Database: Hofstenia_miamia.HmiaM1.pep.all.fa
           22,454 sequences; 8,919,954 total letters



Query= AAH73964.1 PHC1 protein [Homo sapiens]

Length=957
                                                                      Score     E
Sequences producing significant alignments:                          (Bits)  Value

  HMIM003077-PA pep scaffold:HmiaM1:SCFE01000142.1:1091370:111567...  111     4e-25
  HMIM021214-PA pep scaffold:HmiaM1:SCFE01000861.1:65182:73576:-1...  67.0    2e-11
  HMIM015308-PA pep scaffold:HmiaM1:SCFE01000050.1:776045:803165:...  58.9    1e-08
  HMIM012075-PA pep scaffold:HmiaM1:SCFE01000378.1:188268:222270:...  56.6    3e-08
  HMIM011915-PA pep scaffold:HmiaM1:SCFE01000037.1:1713684:186216...  52.4    7e-07
  HMIM008875-PA pep scaffold:HmiaM1:SCFE01000279.1:256602:364831:...  37.4    0.043
  HMIM021615-PA pep scaffold:HmiaM1:SCFE01000009.1:2625373:265074...  32.0    1.3  
  HMIM000607-PA pep scaffold:HmiaM1:SCFE01000104.1:1139698:116950...  31.2    1.7  
  HMIM004028-PA pep scaffold:HmiaM1:SCFE01000162.1:685093:685479:...  30.0    1.9  
  HMIM001541-PA pep scaffold:HmiaM1:SCFE01000116.1:905249:908066:...  31.6    2.0  
  HMIM008325-PA pep scaffold:HmiaM1:SCFE01000263.1:557759:599188:...  30.4    4.3  
  HMIM011251-PA pep scaffold:HmiaM1:SCFE01000349.1:19139:30788:-1...  29.6    5.5  
  HMIM017891-PA pep scaffold:HmiaM1:SCFE01000629.1:187492:300933:...  30.0    7.0  
  HMIM000527-PA pep scaffold:HmiaM1:SCFE01001026.1:160853:161975:...  28.9    9.8  


> HMIM003077-PA pep scaffold:HmiaM1:SCFE01000142.1:1091370:1115672:1 
gene:HMIM003077 transcript:HMIM003077-RA gene_biotype:protein_coding 
transcript_biotype:protein_coding
Length=688

 Score = 111 bits (277),  Expect = 4e-25, Method: Compositional matrix adjust.
 Identities = 79/227 (35%), Positives = 109/227 (48%), Gaps = 34/227 (15%)

Query  753  CEYCGKYAPAEQFRG-SKRFCSMTCAKRYNVSCSHQFRL-----------KRKKMKEFQE  800
            CEYCG       F G  KRFC   CAK+Y  +CS    +           K+KK +   +
Sbjct  474  CEYCGLQGNRNSFYGPMKRFCKQLCAKQYRQNCSRGTGVPRAVHSSSPVRKKKKFRSRTD  533

Query  801  ANYARVR---RRGPRRSSSDIARAKIQGKCHRGQEDSSRGSDN------SSYDEA-LSPT  850
            + Y       R   +R SSD   +   G   R   D  R +++      S  D A +S  
Sbjct  534  SFYIDTTPKVRTASQRHSSDSRISPHTGPLLRS--DFKRSTESAMSPGASVRDSAKVSNY  591

Query  851  SPGPLSVRAGHGERDLGNPNTAPPTPELHGINPVFLSSNPSRWSVEEVYEFIASLQGCQE  910
               P++  A     D+ +  +AP  P      P+        W+  +VY FI S+    E
Sbjct  592  LLQPVTSSAVFRTEDIPSATSAPEAPN----KPI------KEWTRNDVYNFIRSIPAGSE  641

Query  911  IAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKICAKINVLKET  957
             A+ F SQ+IDG ALLLLKE+HLM AM++KLGPALKIC+ +  LKE+
Sbjct  642  YAQSFLSQDIDGHALLLLKEDHLMQAMSMKLGPALKICSHVRKLKES  688


> HMIM021214-PA pep scaffold:HmiaM1:SCFE01000861.1:65182:73576:-1 
gene:HMIM021214 transcript:HMIM021214-RA gene_biotype:protein_coding 
transcript_biotype:protein_coding
Length=575

 Score = 67.0 bits (162),  Expect = 2e-11, Method: Compositional matrix adjust.
 Identities = 29/65 (45%), Positives = 46/65 (71%), Gaps = 0/65 (0%)

Query  891  SRWSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKICAK  950
            SRWS ++V +F+ SL GC+  ++ F  ++IDG+A LLL +  +   +N+KLGPALK+ + 
Sbjct  503  SRWSPQQVAQFVKSLPGCERQSQVFVEEQIDGEAFLLLSQVDIARVLNVKLGPALKVFSS  562

Query  951  INVLK  955
            I +LK
Sbjct  563  ILLLK  567


> HMIM015308-PA pep scaffold:HmiaM1:SCFE01000050.1:776045:803165:1 
gene:HMIM015308 transcript:HMIM015308-RA gene_biotype:protein_coding 
transcript_biotype:protein_coding
Length=1371

 Score = 58.9 bits (141),  Expect = 1e-08, Method: Compositional matrix adjust.
 Identities = 26/68 (38%), Positives = 41/68 (60%), Gaps = 1/68 (1%)

Query  888   SNPSRWSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKI  947
             SNP  W+ +E+ +++ +     E A+  ++++ DGQA LLL    +M  +  KLGPALK+
Sbjct  1292  SNPLNWTTDELVDYLNTTDA-SEFAKTIKAEQFDGQAFLLLSLPFIMEFLGAKLGPALKV  1350

Query  948   CAKINVLK  955
             C  I  LK
Sbjct  1351  CHVIEKLK  1358


> HMIM012075-PA pep scaffold:HmiaM1:SCFE01000378.1:188268:222270:-1 
gene:HMIM012075 transcript:HMIM012075-RA gene_biotype:protein_coding 
transcript_biotype:protein_coding
Length=381

 Score = 56.6 bits (135),  Expect = 3e-08, Method: Compositional matrix adjust.
 Identities = 27/55 (49%), Positives = 38/55 (69%), Gaps = 1/55 (2%)

Query  893  WSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKI  947
            WSVE+V  FI +    +E A  F  QEIDG++LLL+  + ++  +N+KLGPALKI
Sbjct  304  WSVEQVASFIKACGFAKE-ASIFAEQEIDGRSLLLISRQDILCGLNLKLGPALKI  357


> HMIM011915-PA pep scaffold:HmiaM1:SCFE01000037.1:1713684:1862166:-1 
gene:HMIM011915 transcript:HMIM011915-RA gene_biotype:protein_coding 
transcript_biotype:protein_coding
Length=564

 Score = 52.4 bits (124),  Expect = 7e-07, Method: Compositional matrix adjust.
 Identities = 36/122 (30%), Positives = 56/122 (46%), Gaps = 16/122 (13%)

Query  838  SDNSSYDEALSPTSPGPLSVRAGHGERDLG-NPNTAPPTPELHGINPVFLSSNPSRWSVE  896
            +D   Y   + P     L++   H E+ L  N    PP+P L              W+V 
Sbjct  453  NDGCKYLSEVDPKLIPYLNIIKSHYEKSLDVNKTPLPPSPAL--------------WTVN  498

Query  897  EVYEFIASLQ-GCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNIKLGPALKICAKINVLK  955
            +V ++++ +           +S EIDG+A LLL  + +M  M IKLGPALK+      L+
Sbjct  499  DVCKYLSEVDPKLIPYLNIIKSHEIDGKAFLLLNNDIVMKYMGIKLGPALKVANLAQKLR  558

Query  956  ET  957
            +T
Sbjct  559  QT  560


> HMIM008875-PA pep scaffold:HmiaM1:SCFE01000279.1:256602:364831:-1 
gene:HMIM008875 transcript:HMIM008875-RA gene_biotype:protein_coding 
transcript_biotype:protein_coding
Length=6012

 Score = 37.4 bits (85),  Expect = 0.043, Method: Composition-based stats.
 Identities = 61/304 (20%), Positives = 108/304 (36%), Gaps = 38/304 (13%)

Query  26    IAQMSLYERQAVQATIAASRQASSPNTSTTQQQTTTTQASINLATTSAAQLISRSQSVSS  85
             ++ +  + R      +  S    S    ++  QT+  ++SI   +  ++Q  +  +  S+
Sbjct  2495  LSTVEAHSRPTQATALPESESTKSSQAGSSIGQTSGPKSSIPAVSRPSSQPATAMEQTSA  2554

Query  86    PSATTLTQSVLLGNTTSPPLNQSQAQMYLRVNRTLGRNVPLASQLILMPNGAVAAVQQEV  145
             P  +    SV    TT      +  +       ++       SQ       AV+  Q   
Sbjct  2555  PKPSIAPTSVPGSQTTEGEETAASGKQTSAPKPSIAPTGVPGSQTTESEETAVSGKQTSA  2614

Query  146   --PSAQSPGV-HADADQVQNLAVRNQQASAQGPQMQGSTQKAIPPGASPVSSLSQASSQA  202
               PS    GV  +   + +  AV  +Q SA  P        +I P   P S  +++   A
Sbjct  2615  PKPSIAPTGVPGSQTTESEETAVSGKQTSAPKP--------SIAPTGVPGSQTTESEETA  2666

Query  203   LAVAQASSGATNQSLNLSQAGGGSGNSIPGSMGPGGGGQAHGGLGQLPSSGMGGGSCPRK  262
             ++  Q S  A   S+        +   +PGS    G   A  G                K
Sbjct  2667  VSGKQTS--APKPSI--------APTGVPGSQTTEGEETAASG----------------K  2700

Query  263   GTGVVQPLPAAQTVTVSQGS-QTEAESAAAKKAEADGSGQQNVGMNLTRTATPAPSQTLI  321
              T   +P  A   V  SQ + Q +  S+  + +E   +    +  +LT    P+ + TLI
Sbjct  2701  QTSAPKPSIAPTGVPGSQTTRQGQTSSSMQQASELTTTSLSGIPESLTSPLIPSSATTLI  2760

Query  322   SSAT  325
              S +
Sbjct  2761  QSTS  2764


 Score = 35.8 bits (81),  Expect = 0.13, Method: Composition-based stats.
 Identities = 36/113 (32%), Positives = 55/113 (49%), Gaps = 25/113 (22%)

Query  3     TESEQNSNSTNGSSSSGGSSRPQIAQMSLYERQAVQA--TIAASRQASSPNTS-------  53
             TESE+ + S   +S+     +P IA   +   Q  +   T A+ +Q S+P  S       
Sbjct  2660  TESEETAVSGKQTSAP----KPSIAPTGVPGSQTTEGEETAASGKQTSAPKPSIAPTGVP  2715

Query  54    ----TTQQQTTTT-QASINLATTSAAQLISRSQSVSSP----SATTLTQSVLL  97
                 T Q QT+++ Q +  L TTS + +    +S++SP    SATTL QS  L
Sbjct  2716  GSQTTRQGQTSSSMQQASELTTTSLSGI---PESLTSPLIPSSATTLIQSTSL  2765


> HMIM021615-PA pep scaffold:HmiaM1:SCFE01000009.1:2625373:2650746:-1 
gene:HMIM021615 transcript:HMIM021615-RA gene_biotype:protein_coding 
transcript_biotype:protein_coding
Length=554

 Score = 32.0 bits (71),  Expect = 1.3, Method: Compositional matrix adjust.
 Identities = 33/107 (31%), Positives = 43/107 (40%), Gaps = 13/107 (12%)

Query  401  PQPPQVPPTQQVPPSQSQQQAQTLVVQPMLQSSP------LSLPPDAAPKPPI-------  447
            PQP   PP  Q P S + Q     ++Q  L SSP          P A P P +       
Sbjct  185  PQPLDSPPINQFPGSHAAQSRSFRMLQQSLDSSPQQSDGGAERAPYARPAPALSTAIPQR  244

Query  448  PIQSKPPVAPIKPPQLGAAKMSAAQQPPPHIPVQVVGTRQPGTAQAQ  494
            P  + P   P+  PQ    +  +A Q PP+ PV     RQP   Q +
Sbjct  245  PSGNTPSFRPVDAPQQMGYRPPSAVQNPPYQPVGGPHARQPDYNQYR  291


> HMIM000607-PA pep scaffold:HmiaM1:SCFE01000104.1:1139698:1169500:-1 
gene:HMIM000607 transcript:HMIM000607-RA gene_biotype:protein_coding 
transcript_biotype:protein_coding
Length=252

 Score = 31.2 bits (69),  Expect = 1.7, Method: Composition-based stats.
 Identities = 26/119 (22%), Positives = 49/119 (41%), Gaps = 4/119 (3%)

Query  38   QATIAASRQASSPNTSTTQQQTTTTQASINLATTSAAQLISRSQSVSSPSATTLTQSVLL  97
            + TI  SR    PN  T Q+    T   +N       +     +S +SP+ + +   +  
Sbjct  126  ENTIPRSRMNIHPNVYTRQRNRKFTNEVLNFEIEDLLKRFDFEESKTSPTNSDVKVHITD  185

Query  98   GNTTSPPLNQSQAQMYLRVNRTLGRNVPLASQLILMPNGAVAAVQQEVPSAQSPGVHAD  156
            GN  S    +  +Q+ L+    +  N  L+  +I++ +       +E+P  + P   AD
Sbjct  186  GNGVS----KENSQLNLKQILYILENKSLSIIVIIIHHLQSCLYSKEIPILKKPKCKAD  240


> HMIM004028-PA pep scaffold:HmiaM1:SCFE01000162.1:685093:685479:-1 
gene:HMIM004028 transcript:HMIM004028-RA gene_biotype:protein_coding 
transcript_biotype:protein_coding
Length=128

 Score = 30.0 bits (66),  Expect = 1.9, Method: Composition-based stats.
 Identities = 12/31 (39%), Positives = 19/31 (61%), Gaps = 0/31 (0%)

Query  38   QATIAASRQASSPNTSTTQQQTTTTQASINL  68
            Q  +  +RQ +SP T+ +QQQ  +T   +NL
Sbjct  74   QYLMKLTRQTASPTTAISQQQIMSTLGRLNL  104


> HMIM001541-PA pep scaffold:HmiaM1:SCFE01000116.1:905249:908066:-1 
gene:HMIM001541 transcript:HMIM001541-RA gene_biotype:protein_coding 
transcript_biotype:protein_coding
Length=650

 Score = 31.6 bits (70),  Expect = 2.0, Method: Compositional matrix adjust.
 Identities = 23/84 (27%), Positives = 33/84 (39%), Gaps = 4/84 (5%)

Query  429  MLQSSPLSLPPDAAPKPPIPIQSKPPVAPIKPPQLGAAKMSAAQQPPPHIPVQVVGTRQP  488
            +L   PL+L  +        + S+  V P K  Q+     S AQ   P + +QV    + 
Sbjct  412  LLDVCPLTLGIETVGGVMTKLISRNTVVPTKKSQI----FSTAQDNQPTVTIQVFEGERA  467

Query  489  GTAQAQALGLAQLAAAVPTSRGMP  512
             T     LG   L    P  RG+P
Sbjct  468  MTKDNHLLGKFDLNGIPPAPRGVP  491


> HMIM008325-PA pep scaffold:HmiaM1:SCFE01000263.1:557759:599188:1 
gene:HMIM008325 transcript:HMIM008325-RA gene_biotype:protein_coding 
transcript_biotype:protein_coding
Length=769

 Score = 30.4 bits (67),  Expect = 4.3, Method: Compositional matrix adjust.
 Identities = 13/47 (28%), Positives = 29/47 (62%), Gaps = 1/47 (2%)

Query  893  WSVEEVYEFIASLQGCQEIAEEFRSQEIDGQALLLLKEEHLMSAMNI  939
            W+V +V  ++  ++  ++  E F+S  +DG+ L  L EE +++ ++I
Sbjct  258  WTVRDVSNWLRQIK-LEKYVETFKSNNVDGKVLKNLTEEKIINYLHI  303


> HMIM011251-PA pep scaffold:HmiaM1:SCFE01000349.1:19139:30788:-1 
gene:HMIM011251 transcript:HMIM011251-RA gene_biotype:protein_coding 
transcript_biotype:protein_coding
Length=286

 Score = 29.6 bits (65),  Expect = 5.5, Method: Composition-based stats.
 Identities = 23/87 (26%), Positives = 35/87 (40%), Gaps = 3/87 (3%)

Query  810  GPRRSSSDIARAKIQGKCHRGQEDSSRGSDNSSYDEALSPTSPGPLSVRAGHGERDLGNP  869
             PRR S+++A A+I   C      +S     S Y++ +  TS         H E     P
Sbjct  4    APRRFSAEVAFARITELCEDEDSSASESDIESDYEDDMPSTSAHNEDGSDSHDEIQQCQP  63

Query  870  NTAPPTPELHGINPVFLSSNPSRWSVE  896
             T P   E+    P +   + + WS E
Sbjct  64   -TLP--SEMETNQPPYQGRDGTMWSRE  87


> HMIM017891-PA pep scaffold:HmiaM1:SCFE01000629.1:187492:300933:-1 
gene:HMIM017891 transcript:HMIM017891-RA gene_biotype:protein_coding 
transcript_biotype:protein_coding
Length=1678

 Score = 30.0 bits (66),  Expect = 7.0, Method: Compositional matrix adjust.
 Identities = 16/53 (30%), Positives = 27/53 (51%), Gaps = 1/53 (2%)

Query  150  SPGVHADADQVQNLAVRNQQASAQGPQMQGSTQKAIPPGASPVSSLSQASSQA  202
            SP V  D D   +  VRN +A  + P  +G  +K +  G+  ++  ++ S QA
Sbjct  69   SPCVDIDCDLYSSCHVRNGKAKCECPTCEGVPEKFV-CGSDGITYPNECSLQA  120


> HMIM000527-PA pep scaffold:HmiaM1:SCFE01001026.1:160853:161975:-1 
gene:HMIM000527 transcript:HMIM000527-RA gene_biotype:protein_coding 
transcript_biotype:protein_coding
Length=250

 Score = 28.9 bits (63),  Expect = 9.8, Method: Composition-based stats.
 Identities = 18/53 (34%), Positives = 24/53 (45%), Gaps = 8/53 (15%)

Query  742  AELDKKANLLKCEYCGKYAPAEQFRGSK---RFCSMTCAKRYNVSCSHQFRLK  791
            A L+K  N ++C  C    P+ +   SK   RF   TC+     SCS  F  K
Sbjct  15   ARLEKNHNGVECPQCNNCIPSNRINESKLVLRFLCSTCS-----SCSRIFETK  62



Lambda      K        H        a         alpha
   0.308    0.122    0.340    0.792     4.96 

Gapped
Lambda      K        H        a         alpha    sigma
   0.267   0.0410    0.140     1.90     42.6     43.6 

Effective search space used: 5565395330


  Database: Hofstenia_miamia.HmiaM1.pep.all.fa
    Posted date:  Jun 24, 2020  10:20 AM
  Number of letters in database: 8,919,954
  Number of sequences in database:  22,454



Matrix: BLOSUM62
Gap Penalties: Existence: 11, Extension: 1
Neighboring words threshold: 11
Window for multiple hits: 40
