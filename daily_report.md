Bitte schreibt in diese Datei am Ende des Tages was ihr alles gemacht habt, einerseits für euch, damit damit ihr ein überblich habt eas ihr alles geschafft habt, andererseits für die Tutoren, damit wir wissen wo ihr jeweils grade steht.

Diese Datei soll also am Ende jeden Tages committed und gepushed werden.

In dem report sollten stehen: 
* ein Datum
* Erkenntnisse und was ihr gelernt habt, insbesondere über eure Proteine.
* Verweise auf Graphiken und Bäume die ihr erstellt habt.
* Offene Probleme sowie  Aufgaben und Ideen wie ihr damit weitermachen werdet.
* aus dem protoll darf gerne hervorgehen wer an was gearbeitet hat, insbesondere dann, wenn ihr das arbeitsergebnis verschiedener Personen  über einen account committed. 



**Tag 1: 22.06.2020**
- Erste Schritte mit Git gelernt (alle)
- Aminosäurensequenzen der Proteine der Gruppe in Datenbank gesucht + Blast Recherche (Jessica)
- Recherchieren über die einzelnen Proteine und Zusammenfassung (Nils, Jenny)
- Python skript schreiben um die Proteome geordnet runterzuladen und abzuspeichern (Iris)

**Tag 2: 23.06.2020**
- Runterladen der Proteomdaten mit dem Skript
- Einzelne Dateien mussten hier noch gesondert behandelt werden, da der Link falsch war oder die Datei nicht entpackt werden konnte etc. 
- Erstellen der DB mit Blast. Hierbei gab es Probleme hinsichtlich der Frage, ob es eine gesamte DB sein soll und wie dies anzustellen ist 
    oder ob für jeden File eine eigene DB entstehen soll. Das hat viel Zeit in Anspruch genommen.
    Antwort: die DB müssen einzeln für jede Spezies erstellt werden um den besten Hit je Spezies finden zu können.
- Außerdem gab es Verwirrungen über eines unserer Proteine. Da der Name nirgendwo zu finden war. Es handelt sich scheinbar aber um ein Synonym.
- Blasten der erstellten .fasta-files unserer Proteine gegen die von uns erstellte DB. Fehlermeldung "BLAST Database error: No alias or index file found for protein database xy.faa", noch nicht gelöst (Jessica)
- 
**Tag 3: 24.06.2020**
- Problem mit DB gelöst, Datenbank-Format vermutlich falsch; nun mit neuem Befehl neu  (Nils)
- Ausführen im "Proteom"-Ordner in dem die Proteome heruntergeladenen wurden:

$   for file in */*; do makeblastdb -in $file -dbtype prot; done

- oder einzeln einfach:

$   makeblastdb -in proteomfastadatei.faa -dbtype prot

- Erstellen von Fasta-Dateien aus den Aminosäurensequenzen (NCBI, .. ) der Proteine (Jessica)

$   echo "sequenz in fastaformat" >> sequenz.fasta
- **Blasten: ** (aufgeteilt unter Jessica, Iris und Nils)

- Nun mit richtiger Datenbank: 
$ blastp -db proteomfastadatei.faa -query proteinsequenz.fasta > proteinvsproteom.txt 

- Ordner erstellt "BlastedProteins" und Einordnung der Verschiedenen Blast-Textfiles zu den einelnen Taxa (Jessica)
- Einfügen der Blastfiles in die einzelnen Ordner (alle); Überlegungen zur Weiterverarbeitung der Blastfiles in tabellarischer Form; erneut Blasten um direkt tabellarische Form zu erhalten (Jenny)
- - Bedeutung der einzelnen Scores/Values recherchiert (nils)

- BigBlueButton leider seit ca. 16:45 down. Weiterarbeiten nicht möglich. (Jessica)

nächtlicher NACHTRAG:
Automatisiertes Blasten und Ausgabe der Blast-Datei in Tabellenform (Jenny)
    
$ dbquery.sh:
    
    #!/bin/sh

    while read protein
    	do 
    	while read db
    		do
    		blastp -db $db -query $protein -outfmt 6 > ./blast/${protein}_${db}.txt
    		done < ./dblist.txt
	
    	blastp -db $db -query $protein -outfmt 6 > ./blast/${protein}_${db}.txt
    	done < ./proteinlist.txt

- Vorbereitung: Umbennung der entzippten Proteom-Fastas entsprechend der vorgegebenen Abkürzung in proteomeTable.txt (ABCD, in Grossbuchstaben!, ohne ".fa, .faa, .fasta"), 
        dann Erstellung der zugehörigen DB files mit  $ for file in *; do makeblastdb -in $file -dbtype prot; done
- Erstellung der Liste der Proteinquery-Dateien (proteinlist.txt) und der DB-Liste (dblist.txt)
- Sammlung aller DB.fa und proteinquery.fa -Dateien sowie von proteinlist.txt und dblist.txt in einem Ordner; Erstellung eines Unterordners blast für die Ergebnisse
- aus diesem Sammelordner heraus: Blasten mit 
        $ bash dbquery.sh
    
- automatisiertes Blasten funktioniert, aber die tabellerischen Dateien wurden noch nicht in git gepusht, da aufgrund von zu niedriger Übertragungsgeschwindigkeit beim Download noch einzelne DBs fehlen (kamen nicht beim automatischen Download mit, manueller Download notwendig)
- Erstellung von Entwürfen zum Filtern und Sortieren der nicht-tabellarischen (ohne -outfmt 6) und der tabellarischen Blast.output Dateien basierend auf awk (Jenny)

**Tag 4: 25.06.2020**

- wir haben scheinbar etwas zu viel Zeit damit verbracht, die einzelnen Proteine gegen die einzelnen Datenbanken zu alignen und in einzelnen Ordner zu packen. Außerdem war die Ausgabe nicht so, wie wir wollten (in nützlicher Tabellenform). Das müssen wir heute nochmal schnell automatisiert nachholen. (Jessica)

- Proteomdaten in Fasta umbenennen, wie in Tag 3 angegeben. Erstellen der DB Files wie ebenfalls aus Tag 3 zu entnehmen. (alle)
- Anpassung des Skripts fuer das Blasten - Output einmal in Tabellenform (ohne Alignments), einmal nur das Alignment (in Arbeit)
    Gespräch mit den BetreuerInnen darüber, dass wir das Alignment nicht brauchen. Wir suchen lediglich nach einen oder mehreren guten Hits in der
    Spezies für jedes Protein. Damit wollen wir nachvollziehen in welchen Spezies und phylogenetischen Branches wir das jeweilige Protein finden können.
    
**Tag 5: 26.06.2020**

- Auswertung der erstellten Blasttabellen. Dafür wird ein Skript geschrieben, welches bereits die besten Hits auswählt und zusammen in ein File fügt.
    Damit sollen die Ergebnisse übersichtlicher dargestellt werden und leichter ausgewertet werden können. Es soll uns ersparen alle Datein einzeln
    anschauen zu müssen. (Jenny)
    
- Beschäftigung mit Splitstree um die Ergbenisse im Folgenden darstellen zu können.
    Aus den ausgewählten Hits beim Blast soll pro Protein ein Tree erstellt werden der zeigt in welchen Spezies das Protein gefunden wurde. 
    Erkenntnis, dass SplitsTree aus einer Multiplen Sequenzalignment im fasta Format einen Baum generieren kann. Vorher musste SplitsTree zum 
    Laufen gebracht werden. Die Version SplitsTree 4 ist auf Windows mit Bugs behaftet. Der Fehler konnte nicht behoben werden. Mit der Version 
    SplitsTree 5 taucht er jedoch nicht mehr auf. (Iris)

- Testweise händisches Auswerten der Blasts für das Protein SCMH1 mit den verschiedenen Spezies. Vorerst wurden die Hits mit den besten e-value
    recht grob aussortiert. Dabei wurde beachtet, dass die Alignments über mehr als 70 AS gehen und eine relativ gute (>30%) Coverage besitzen. 
    Anschließend wurden testweise die original AS Sequenz des SCMH1 mit den ersten 17 Proteinen aus verschiedenen Spezies, welche einen Hit generierten
    über Clustalo alignt. (Iris)

$   clustalo -i "SCMH1_Multifasta.fasta" > "SCMH1_MSA.fasta" 

- Beim Betrachten des Alignment mit AliView konnte eine konservierte Region ausgemacht werden. Testweise wurde das Alignment mit SplitsTree geöffnet. 
    Es konnte ein Baum erstellt werden, der auf den ersten Blick allerdings nicht besonders aussagekräftig scheint. 
    Problematisch ist die Bezeichnung der einzelnen Blätter im Baum. Diese tragen im Moment die komplette Sequenzbezeichnung aus dem Aligment für 
    jede Sequenz. Das muss zur besseren Übersicht geändert werden. 
    Das zum Testen erstellte Multifasta und MSA kann unte dem Order Auswertung.Blast.Test gefunden werden. (Iris)

NACHTRAG: (Jenny)
- die Skripts zum automatischen Filter und Sorten zur einfachereren Auswertung der BLAST-Ergebnisse wurde erfolgreich erstellt;
   aus technischen Gruenden koennen die finalen zusammengefassten Ergebnisdateien erst am Montag ins Git geladen werden; bis auf das letzte Skript zum Joinen sind die Skripte hochgeladen
- Folgende Schritte wurden vorgenommen (alles automatisiert) (Jenny):
   - Filter auf evalue < 0.00001 und sortieren nach evalue (aufsteigend)
   - die 5 Matches mit dem niedrigsten evalue fuer jede Query wurden in einer Datei zusammengefasst, d.h. pro query Protein gibt es ein File mit den Treffern gegen alle DBs
   - Joinen der Tabelle mit den query Ergebnissen mit den Familiennamen der Proteine; 
   - in der joint table sind die Daten erstrangig nach "family", bei Gleichheit dieser nach "ssequid" und bei Gleichheit dieser nach "evalue" sortiert
   - für Spezien bzw. DBs für die kein signifikanter Match mit dem entsprechenden Protein gefunden wurde steht NA; 
      auf dieser Weise lässt sich schnell sehen, ob für das Protein in dieser Spezies ueberhaupt ein signifikanter Match gefunden wurde
    - Auswertung der Tabellen erfolgt im Laufe des Wochenende bzw. am Montag

   - die Tabelle enthält folgende Spalten (mit NA, wenn kein Match mit evalue < 0.00001 vorliegt): 
     family, species_abbrevm, species_name, qseqid, sseqid, evalue, bitscore, pident, length, mismatch, gapopen, qstart, qend, sstart, send