import sys
import os
from os import mkdir


def function1(current_directory):
    if x.startswith("#Abk") or x.startswith("#ANM"):
        return current_directory
    else:
        current_directory = (sys.argv[1] + '/' + x[1:len(x)-1].strip())
        if not os.path.isdir(current_directory):
            mkdir(current_directory)
        return current_directory


def function2(current_directory):
    split_line = x.split("\t")
    split_filename = split_line[3].split("/")
    filename = split_filename[len(split_filename)-1]
    if split_line[3] != "---":
        if not os.path.isfile(current_directory + '/' + filename):
            os.system('wget -O "' + current_directory + '/' + filename + '" ' + split_line[3])


proteom = open(sys.argv[1] + '/proteomTable.txt', 'r')
singleproteom = proteom.readlines()
current_directory = None
for x in singleproteom:
    if x.startswith('#'):
        current_directory = function1(current_directory)
    else:
        if x != "\n":
            function2(current_directory)
