#!/bin/bash

for file in *_tab.txt
	 do
		nvar=$( echo $file | sed -e  's/.txt//g' )
 		if [[ $file == PHC1seq_* ]];  then  awk '$11 < 0.00001 {print}' $file > ./summary/${nvar}.f.txt  | sort -t$'\t' -k11g
		elif [[ $file == PHC2seq_* ]]; then  awk '$11 < 0.00001 {print}' $file > ./summary/${nvar}.f.txt  | sort -t$'\t' -k11g
		elif [[ $file == PHC3seq_* ]]; then  awk '$11 < 0.00001 {print}' $file > ./summary/${nvar}.f.txt  | sort -t$'\t' -k11g
		elif [[ $file == RING1seq_* ]]; then  awk '$11 < 0.00001 {print}' $file > ./summary/${nvar}f.txt  | sort -t$'\t' -k11g
		elif [[ $file == RING2seq_* ]]; then  awk '$11 < 0.00001 {print}' $file > ./summary/${nvar}.f.txt  | sort -t$'\t' -k11g
		elif [[ $file == RING2isoformseq_* ]]; then  awk '$11 < 0.00001 {print}' $file > ./summary/${nvar}.f.txt  | sort -t$'\t' -k11g
		elif [[ $file == SCMH1seq_* ]]; then  awk '$11 < 0.00001 {print}' $file > ./summary/${nvar}.f.txt  | sort -t$'\t' -k11g
		else  echo "blabla" > ./summary/blabla.txt
		fi
	done

#sort  -t$'\t' -k11g test2.txt

#do
# 	nvar=$(echo $file | sed -rE 's/(...)(...)/\1.text )'
#	if .... > $nvar

