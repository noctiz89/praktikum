#!/bin/bash

echo -e "species\tqseqid\tsseqid\tpident\tlength\tmismatch\tgapopen\tqstart\tqend\tsstart\tsend\tevalue\tbitscore" >> ./comb/PHC1.txt
echo -e "species\tqseqid\tsseqid\tpident\tlength\tmismatch\tgapopen\tqstart\tqend\tsstart\tsend\tevalue\tbitscore" >> ./comb/PHC2.txt
echo -e "species\tqseqid\tsseqid\tpident\tlength\tmismatch\tgapopen\tqstart\tqend\tsstart\tsend\tevalue\tbitscore" >> ./comb/PHC3.txt
echo -e "species\tqseqid\tsseqid\tpident\tlength\tmismatch\tgapopen\tqstart\tqend\tsstart\tsend\tevalue\tbitscore" >> ./comb/RING1.txt
echo -e "species\tqseqid\tsseqid\tpident\tlength\tmismatch\tgapopen\tqstart\tqend\tsstart\tsend\tevalue\tbitscore" >> ./comb/RING2.txt
echo -e "species\tqseqid\tsseqid\tpident\tlength\tmismatch\tgapopen\tqstart\tqend\tsstart\tsend\tevalue\tbitscore" >> ./comb/RING2iso.txt
echo -e "species\tqseqid\tsseqid\tpident\tlength\tmismatch\tgapopen\tqstart\tqend\tsstart\tsend\tevalue\tbitscore" >> ./comb/SCMH1.txt

for file in *_tab.f.txt 
          do
		dbx=$( echo $file | sed -e  's/_tab.f.txt//g' )
		db=$( echo $dbx | sed -e  's/.*_//g' )
		
		if [[ $file == PHC1seq_* ]];  then  awk  'NR <=5 {print dbname"\t"$0 }'  dbname="$db" $file >> ./comb/PHC1.txt
                elif [[ $file == PHC2seq_* ]];  then  awk  'NR <=5 {print dbname"\t"$0 }' dbname="$db" $file >> ./comb/PHC2.txt
                elif [[ $file == PHC3seq_* ]];  then  awk  'NR <=5 {print dbname"\t"$0 }'  dbname="$db" $file >> ./comb/PHC3.txt
                elif [[ $file == RING1seq_* ]];  then  awk  'NR <=5 {print dbname"\t"$0 }'  dbname="$db" $file >> ./comb/RING1.txt
                elif [[ $file == RING2seq_* ]];  then  awk  'NR <=5 {print dbname"\t"$0 }'  dbname="$db" $file >> ./comb/RING2.txt 
                elif [[ $file == RING2isoformseq_* ]];  then  awk  'NR <=5 {print dbname"\t"$0 }' dbname="$db" $file >> ./comb/RING2iso.txt  
                elif [[ $file == SCMH1seq_* ]];  then  awk  'NR <=5 {print dbname"\t"$0 }' dbname="$db" $file >> ./comb/SCMH1.txt 
		else :
#                else  echo "blabla" > ./test/bl
                fi
         done

#sed 's/^@\(.*\)/\1/'
#^ means beginning of the string
#@ your known char
#(.*) the rest, captured

#-F $'\t' 
