#!/bin/bash

for file in *.txt
	do
		nvar=$( echo $file | sed -e  's/.txt//g' )
		cat $file | tr "\\t" "," > ${nvar}.csv
	done
